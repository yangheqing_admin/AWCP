package cn.org.awcp.core.domain;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class SzcloudJdbcTemplate extends org.springframework.jdbc.core.JdbcTemplate {

	private PlatformTransactionManager platformTransactionManager;
	private DefaultTransactionDefinition transactionDefinition;
	private ThreadLocal<TransactionStatus> transcationStatus = new ThreadLocal<TransactionStatus>();

	public void beginTranstaion() {
		// TODO 改用双重锁模式
		if (transcationStatus.get() == null) {
			synchronized (this) {
				if (transcationStatus.get() == null) {
					transcationStatus.set(platformTransactionManager.getTransaction(transactionDefinition));
				}
			}
		}
	}

	public void commit() {
		TransactionStatus tmp = transcationStatus.get();
		if (tmp == null) {
			return;
		}
		platformTransactionManager.commit(tmp);
		transcationStatus.remove();
	}

	public void rollback() {
		TransactionStatus tmp = transcationStatus.get();
		if (tmp == null) {
			return;
		}
		platformTransactionManager.rollback(tmp);
		transcationStatus.remove();

	}

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		transactionDefinition = new DefaultTransactionDefinition();
		transactionDefinition.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
		transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		platformTransactionManager = new DataSourceTransactionManager(getDataSource());

	}

	public PlatformTransactionManager getPlatformTransactionManager() {
		return platformTransactionManager;
	}

	public DefaultTransactionDefinition getTransactionDefinition() {
		return transactionDefinition;
	}

	public ThreadLocal<TransactionStatus> getTranscationStatus() {
		return transcationStatus;
	}

	@Override
	public <T> T queryForObject(String sql, Class<T> requiredType, Object... args) throws DataAccessException {
		if(args != null){
			logger.debug("queryForObject的参数 ————>> " + Arrays.toString(args));
		}		
		T t = super.queryForObject(sql, args, getSingleColumnRowMapper(requiredType));
		logger.debug("queryForObject的结果 ————>> " + t);
		return t;
	}
	
	@Override
	public Map<String, Object> queryForMap(String sql, Object... args) throws DataAccessException {
		if(args != null){
			logger.debug("queryForMap的参数 ————>> " + Arrays.toString(args));
		}
		Map<String,Object> map = super.queryForObject(sql, args, getColumnMapRowMapper());
		logger.debug("queryForMap的结果 ————>> " + map);
		return map;
	}
	
	@Override
	public <T> List<T> queryForList(String sql, Class<T> elementType, Object... args) throws DataAccessException {
		if(args != null){
			logger.debug("queryForList的参数 ————>> " + Arrays.toString(args));
		}
		List<T> list = super.query(sql, args, getSingleColumnRowMapper(elementType));
		logger.debug("queryForList的结果 ————>> " + list);
		return list;
	}
	
	@Override
	public List<Map<String, Object>> queryForList(String sql, Object... args) throws DataAccessException {
		if(args != null){
			logger.debug("queryForList的参数 ————>> " + Arrays.toString(args));
		}
		List<Map<String,Object>> list = super.query(sql, args, getColumnMapRowMapper());
		logger.debug("queryForList的结果 ————>> " + list);
		return list;
	}
	
	@Override
	public int update(String sql, Object... args) throws DataAccessException {
		if(args != null){
			logger.debug("update的参数 ————>> " + Arrays.toString(args));
		}
		int count = super.update(sql, newArgPreparedStatementSetter(args));
		logger.debug("update执行成功数 ————>> " + count);
		return count;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <T> List<T> queryForEntityList(String sql, Class<T> elementType, Object... args) throws DataAccessException {
		if(args != null){
			logger.debug("update的参数 ————>> " + Arrays.toString(args));
		}
		List<T> list = query(sql, args, new EntityRowMapper(elementType));
		logger.debug("queryForEntityList的结果 ————>> " + list);
		return list;
	}
	
	class EntityRowMapper<T> implements RowMapper<T>{
		Class<T> elementType;
		
		EntityRowMapper(Class<T> elementType){
			this.elementType = elementType;
		}
		
		@SuppressWarnings("rawtypes")
		@Override
		public T mapRow(ResultSet rs, int rowNum) throws SQLException {
			try {
				T t = elementType.newInstance();
				Field[] fields = elementType.getDeclaredFields();
				Map<String,Field> map = new HashMap<String,Field>();
				for(Field field : fields){
					String fieldName = field.getName();
					map.put(fieldName, field);
				}
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
				for (int i = 1; i <= columnCount; i++) {
					String key = JdbcUtils.lookupColumnName(rsmd, i);
					if(map.containsKey(key)){
						String setMethod = "set" + key.substring(0, 1).toUpperCase() + key.substring(1);
						Class type = map.get(key).getType();
						Object obj = JdbcUtils.getResultSetValue(rs, i,type);
						elementType.getDeclaredMethod(setMethod,type).invoke(t, obj);
					}
				}
				return t;
			} catch (Exception e) {			
				e.printStackTrace();
				throw new RuntimeException("将数据库数据转换为实体类失败");
			} 
		}		
	}	
}
