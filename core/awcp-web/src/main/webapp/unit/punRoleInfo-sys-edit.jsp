<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <title>角色编辑页面</title>
    <%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body>
	<section class="content">
		<div class="opeBtnGrop">
	    	<button class="btn btn-primary" id="saveBtn"><i class="fa fa-save"></i> 保存</button>
		    <button class="btn btn-default" id="undoBtn"></i><i class="fa fa-reply"></i> 取消</button>
    	</div>	
	    <div class="row">
	        <div class="col-md-12">
	            <div class="box box-info">
	                <div class="box-body">
	                    <form class="form-horizontal" id="groupForm" method="post">
	                        <input type="hidden" name="roleId" value="${vo.roleId}"/>
	                        <div class="form-group">
	                        	<div class="customGroup">	
		                            <div class="col-md-6">
		                            	<label class="control-label required">角色名</label>
		                                <input name="roleName" class="form-control" id="roleName" type="text"
		                                       value="${vo.roleName}" />
		                            </div>
	                            </div>
	                            <div class="customGroup">	
		                            <div class="col-md-6">
		                            	<label class="control-label">描述</label>
		                                <input name="dictRemark" class="form-control" id="dictRemark" type="text"
		                                       value="${vo.dictRemark}" />
		                            </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
	<%@ include file="../resources/include/common_lte_js.jsp"%>
	<script type="text/javascript">
		$(function(){
			$("#undoBtn").on("click",function(){
				toListPage(basePath + "unit/listRolesInSys.do");
			});
			
			var $form = $("#groupForm");
        	$form.bootstrapValidator({
        		excluded: [":disabled"],
        	    fields:{
                    "roleName": {
                        validators: {
                        	notEmpty: {
                                message: "角色名不能为空"
                            }
                        }                    
        			}           
        	    }
        	});
        	
        	$("#saveBtn").on("click",function () {
        		if(!validateForm()){
	    			return false;
	    		}
        		var data = myFun.ajaxExecute("api/unit/saveRoleInSys",$("#groupForm").serialize());
        		if(data.status == "0"){
        			toListPage(basePath + "unit/listRolesInSys.do");
        		} else{
        			if(data.message){
        				Comm.alert(data.message);
        			} else{
        				Comm.alert("保存失败");
        			}
        		}
    	    });
		})	    
	</script>
</body>
</html>
