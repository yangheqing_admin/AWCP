<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body>
	<section class="content">		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<form method="get" action="<%=basePath%>punUserGroupController/getUsers.do" id="userList">
							<input type="hidden" id="groupId" name="groupId" value="${groupId}"/>
							<input type="hidden" id="currentPage" name="currentPage" value="${userList.getPaginator().getPage()}" />
							<input type="hidden" id="pageSize" name="pageSize" value="${userList.getPaginator().getLimit()}" />
							<input type="hidden" id="totalCount" name="totalCount" value="${userList.getPaginator().getTotalCount()}" />
							<div class="row form-group">								
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">名称</span>
										<input class="form-control" id="name" name="name" value="${name}" type="text"/>
										<span class="input-group-btn">
					                      	<button class="btn btn-primary" id="searchBtn"><i class="fa fa-search"></i> 搜索</button>
					                    </span>
									</div>
								</div>
								<div class="col-xs-8" >
									<button class="btn btn-info" id="moveBtn" style="float:right;">
										<i class="fa fa-exchange"></i> 移动到
									</button>
								</div>
							</div>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th class="hidden"></th>
										<th data-width="" data-field="" data-checkbox="true"></th>
										<th>姓名</th>
										<th>用户名</th>
										<th>部门</th>
										<th>岗位</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${userList}" var="vo">
										<tr>
											<td class="hidden">
												<input  type="hidden" value="${vo.userId}"> 
											</td>
											<td></td>
											<td>${vo.name}</td>
											<td>${vo.userIdCardNumber}</td>
											<td>
											<c:choose>
												<c:when test="${not empty vo.deptName}">
													${vo.deptName}
												</c:when>
												<c:otherwise>
													${pun.groupChName}
												</c:otherwise>
										    </c:choose>
										    </td>
											<td>
												<c:forEach items="${vo.positionList}" var="p" varStatus="sta">
													<c:choose>
														<c:when test="${0==sta.index}">
															${p.name}
														</c:when>
														<c:otherwise>
															;${p.name}
														</c:otherwise>
													</c:choose>
					            				</c:forEach>
					            			</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<%@ include file="../resources/include/common_lte_js.jsp"%>
	<script type="text/javascript">
		$(function(){
			var groupId = $("#groupId").val();
			var count=0;//默认选择行数为0
			$(".table").bootstrapTable({
				pageSize:parseInt($("#pageSize").val()),
		        pageNumber:parseInt($("#currentPage").val()),
		        totalRows:parseInt($("#totalCount").val()),
		        sidePagination:"server",
		        pagination:true,
		        onPageChange:function(number, size){
		        	$("#pageSize").val(size);
		        	$("#currentPage").val(number);
		        	$("#userList").attr("action",basePath + "punUserGroupController/getUsers.do").submit();
		        },
		        onClickRow:function(row,$element,field){
		        	var $checkbox=$element.find(":checkbox").eq(0);
        	  	  	if($checkbox.get(0).checked){
					  	$checkbox.get(0).checked=false;
					  	$element.find("input[type='hidden']").removeAttr("name","boxs");
        	  	  	}else{
					  	$checkbox.get(0).checked=true;
					  	$element.find("input[type='hidden']").attr("name","boxs");
        	  	  	}
				  	count = $("input[name='boxs']").length;
        	 	},
		       	onCheck: function(row,$element){
					$element.closest("tr").find("input[type='hidden']").attr("name","boxs");
					count = $("input[name='boxs']").length;
		        },
		        onUncheck:function(row,$element){
		        	$element.closest("tr").find("input[type='hidden']").removeAttr("name");
					count = $("input[name='boxs']").length;
		        }, 
		        onCheckAll: function (rows) {
		        	$.each(rows,function(i,e){
		        		$("input[value='"+$($.trim(e["0"])).attr("value")+"']").attr("name","boxs");
		        	})
					count = $("input[name='boxs']").length;
		   		},
		        onUncheckAll: function (rows) {
		           	$.each(rows,function(i,e){
		        		$("input[value='"+$($.trim(e["0"])).attr("value")+"']").removeAttr("name");
		        	})
					count = $("input[name='boxs']").length;
		      	}
		  	});
			  
			$("#moveBtn").on("click",function(){
				var groupId = $("#groupId").val();
				var value = []; 
				$('input[name="boxs"]').each(function(){
					value.push($(this).val()); 
				});
				if(value.length == 0){
				  	Comm.alert("请至少选择一项进行操作");
				  	return false;
				} 
				top.dialog({
					title: "选择要移动的部门",
					url: basePath + "unit/deptList_check.jsp",
					id : 'edit-dialog' + Math.ceil(Math.random() * 10000),
					data:{
						userIds:value,
						oldGroupId:groupId
					},
					height:500,
				    onclose: function () {
				    	console.log("123");
				    	$("#userList").attr("action",basePath + "punUserGroupController/getUsers.do").submit();
				    }
				}).width(600).showModal();
				return false;
			});
		});
	</script>
</body>
</html>
