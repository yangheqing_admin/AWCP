<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head lang="zh">
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>用户列表</title>
    <%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body>
    <!-- Main content -->
    <section class="content">
    	<div class="opeBtnGrop">
	    	<a href="#" class="btn btn-primary"  id="addBtn"><i class="fa fa-plus"></i> 新增</a>
	    	<a href="#" class="btn btn-success" id="activeBtn"><i class="fa fa-server"></i> 启用</a>
			<a href="#" class="btn btn-warning" id="disableBtn"><i class="fa fa-file"></i> 禁用</a>	
			<a href="#" class="btn btn-danger" id="deleteBtn"><i class="fa fa-trash"></i> 删除</a>
			<a href="#" class="btn btn-default" id="closeBtn"><i class="fa fa-close"></i> 关闭</a>
    	</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form method="post" id="groupForm" action="<%=basePath%>unit/punUserBaseInfoList.do">
							<input type="hidden" id="currentPage" name="currentPage" value="${vos.getPaginator().getPage()}">
							<input type="hidden" id="pageSize" name="pageSize" value="${vos.getPaginator().getLimit()}">
							<input type="hidden" id="totalCount" name="totalCount" value="${vos.getPaginator().getTotalCount()}">				
							<div class="row form-group" id="searchDiv">
								<div class="col-md-2">
									<div class="input-group">
										<span class="input-group-addon">姓名</span>
							         	<input name="name" type="text" id="name" class="form-control"  value="${vo.name}">
						            </div>
								</div>
								<div class="col-md-2">
									<div class="input-group">
						                <span class="input-group-addon">部门</span>
						                <input name="deptName" class="form-control" id="deptName" type="text" value="${vo.deptName}">
						            </div>
								</div>
								<div class="col-md-2">
									<div class="input-group">
						                <span class="input-group-addon">职务</span>
						                <input name="userTitle" class="form-control" id="userTitle" type="text" value="${vo.userTitle}">
						            </div>
								</div>
								<div class="col-md-2">
									<div class="input-group">
						                <span class="input-group-addon">手机</span>
						                <input name="mobile" class="form-control" id="mobile" type="text" value="${vo.mobile}">
						            </div>
								</div>
								<div class="col-md-2">
									<a href="#" id="searchBtn" class="btn btn-info"><i class="fa fa-search"></i> 搜索</a>
								</div>
							</div>
							<table class="table table-hover">  
							    <thead>
							        <tr>
							        	<th class="hidden"></th>
							        	<th data-checkbox="true"></th>
							            <th data-width="50">序号</th>
							            <th>姓名</th>
							            <th>部门</th>
							            <th>用户名</th>
							            <th>用户编号</th>
							            <th data-width="100">手机号</th>
						            	<th>角色</th>
							            <th>职务</th>
							            <th data-width="50">状态</th>
							        </tr>
							    </thead>
							    <tbody>
									<c:forEach items="${vos}" var="vo" varStatus="status">
							          <tr> 	
							            <td class="hidden formData">
							            	<input type="hidden" value="${vo.userId}">
							            </td>
							            <td></td>
							            <td>${currentPage*vos.getPaginator().getLimit()-vos.getPaginator().getLimit()+status.index +1}</td>
							            <td><a href="javascript:void(0)" data-id='${vo.userId}' class="userName">${vo.name}</a></td>
							            <td>${vo.deptName}</td>
							            <td>${vo.userName}</td>
							            <td>${vo.userIdCardNumber}</td>
							            <td>${vo.mobile}</td> 
						            	<td>${vo.userBirthPlace}</td>
							            <td>${vo.userTitle}</td>
							            <td>${vo.userStatus=="1"?"启用":"禁用"}</td>
							          </tr>
							       </c:forEach>
								</tbody>
							</table>  
						</form>
					</div>				
				</div>				
			</div>
		</div>
    </section>
    <%@ include file="../resources/include/common_lte_js.jsp"%>
	<script>
	  	$(function(){
			var count=0;//默认选择行数为0
			$(".table").bootstrapTable({
				pageSize:parseInt($("#pageSize").val()),
				pageNumber:parseInt($("#currentPage").val()),
				totalRows:parseInt($("#totalCount").val()),
				sidePagination:"server",
				pagination:true,
				onPageChange:function(number, size){
					$("#pageSize").val(size);
					$("#currentPage").val(number);
					customSearch();
				},
				onClickRow:function(row,$element,field){
					var $checkbox=$element.find(":checkbox").eq(0);
					if($checkbox.get(0).checked){
						$checkbox.get(0).checked=false;
						$element.find("input[type='hidden']").removeAttr("name","boxs");
					}else{
						$checkbox.get(0).checked=true;
						$element.find("input[type='hidden']").attr("name","boxs");
					}
					count = $("input[name='boxs']").length;
				},
				onClickCell:function(field,value,row,$element){
					var userId = parseInt($element.find("a").attr("data-id"));
					if(userId){
						$("#groupForm").attr("action",basePath + "unit/punUserBaseInfoGet.do?boxs="+userId).submit();
					}
				},
				onCheck: function(row,$element){
					$element.closest("tr").find("input[type='hidden']").attr("name","boxs");
					count = $("input[name='boxs']").length;
				},
				onUncheck:function(row,$element){
					$element.closest("tr").find("input[type='hidden']").removeAttr("name");
					count = $("input[name='boxs']").length;
				},
				onCheckAll: function (rows) {
					$.each(rows,function(i,e){
						$("input[value='"+$($.trim(e["0"])).attr("value")+"']").attr("name","boxs");
					});
					count = $("input[name='boxs']").length;
				},
				onUncheckAll: function (rows) {
					$.each(rows,function(i,e){
						$("input[value='"+$($.trim(e["0"])).attr("value")+"']").removeAttr("name");
					});
					count = $("input[name='boxs']").length;
				}
			});

			function customSearch(){
				var arr = ["pageSize=" + $("#pageSize").val(),"currentPage=" + $("#currentPage").val(),"name=" + $("#name").val(),
					"deptName=" + $("#deptName").val(),"userTitle=" + $("#userTitle").val(),"mobile=" + $("#mobile").val()]
	        	search(basePath + "unit/punUserBaseInfoList.do",arr);
	        }
			
			//搜索
			$("#searchBtn").on("click",function(){
				customSearch();
			});
			
			$(document).keyup(function(event) {
		    	if (event.keyCode == 13) {
		    		customSearch();
		    	}
	    	});
			
			//关闭
	        $("#closeBtn").on("click",function(){
	        	closePage();
	        });
			
			//新增
			$("#addBtn").click(function(){
				location.href = basePath + "unit/intoPunUserBaseInfo.do";
				return false;
			});
  		
			//删除
			$("#deleteBtn").click(function(){
				if(count < 1){
					Comm.alert("请至少选择一项进行操作");
					return false;
				}
				Comm.confirm("确定删除？",function(){
					var data = myFun.ajaxExecute("api/unit/punUserBaseInfoDelete",$("#groupForm").serialize());
					if(data.status == "0"){
	            		$("#groupForm").submit();
	        		} else{
	        			if(data.message){
	        				Comm.alert(data.message);
	        			} else{
	        				Comm.alert("删除失败");
	        			}
	        		}
				});
			});
			
			//启用
			$("#activeBtn").click(function(){
				if(count == 1){
					var userId = $("input[name='boxs']").val();
					var data = myFun.ajaxExecute("api/unit/updateUserStatus",{userId:userId,userStatus:"1"});
					if(data.status=="0" && data.data==1){
						$("#groupForm").submit();
					}
				}else{
					Comm.alert("请选择一个用户进行操作");
				}
				return false;
			});
			
			//禁用
			$("#disableBtn").click(function(){
				if(count == 1){
					var userId = $("input[name='boxs']").val();
					var data = myFun.ajaxExecute("api/unit/updateUserStatus",{userId:userId,userStatus:"2"});
					if(data.status=="0" && data.data==1){
						$("#groupForm").submit();
					}
				}else{
					Comm.alert("请选择一个用户进行操作");
				}
				return false;
			});
      	});
	</script>
</body>
</html>