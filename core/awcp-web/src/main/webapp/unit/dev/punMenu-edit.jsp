<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>新增根节点</title>
	<%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body id="main">
	<section class="content">
	    <div class="container-fluid">
	        <div class="opeBtnGrop">
	        	<button type="button" class="btn btn-success" id="saveBtn"><i class="fa fa-save"></i> 保存</button>
	            <button type="button" class="btn btn-default" id="closeBtn"><i class="fa fa-close"></i> 关闭</button>
	        </div>
	        <div class="row">
	            <div class="col-md-12">
	                <div class="box box-info">
	                    <div class="box-body">
	                    	<form class="form-horizontal" id="groupForm" method="post">
								<input type="hidden" name="menuId" value="${vo.menuId}" /> 
								<input type="hidden" name="sysId" value="${vo.sysId}" /> 
								<input type="hidden" name="parentMenuId" value="${vo.parentMenuId}" />
								<div class="form-group">
									<div class="customGroup">
										<div class="col-md-4">
											<label class="control-label required">菜单名称</label>
											<input name="menuName" class="form-control" id="menuName" 
												type="text" value="${vo.menuName}" />
										</div>	
									</div>
									<div class="customGroup">
										<div class="col-md-4">
											<label class="control-label">动态表单</label>
											<select class="form-control" name="dynamicPageId" id="dynamicPageId" 
												value="${vo.dynamicPageId}"></select>
										</div>
									</div>
									<div class="customGroup">
										<div class="col-md-4">
											<label class="control-label">菜单地址</label>
											<input name="menuAddress" class="form-control" id="menuAddress"
												type="text" value="${vo.menuAddress}" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="customGroup">
										<div class="col-md-4">
											<label class="control-label required">序列号码</label>
											<input type="text" class="form-control" name="menuSeq" id="menuSeq"
												value="${vo.menuSeq}" />
										</div>
									</div>
									<div class="customGroup">
										<div class="col-md-4">
											<label class="control-label">图标</label>
											<input type="text" class="form-control" id="menuIcon" name="menuIcon" 
													value="${vo.menuIcon}" />
										</div>
									</div>
									<div class="customGroup">
										<div class="col-md-4">
											<label class="control-label">是否为常用</label><br/>
											<label class="radio-inline">
												<input name="menuFlag"  type="radio" ${vo.menuFlag==1?"checked":""} value="1">是
											</label>
											<label class="radio-inline">
												<input name="menuFlag"  type="radio" ${vo.menuFlag==0?"checked":""} value="0">否
											</label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-4">
										<label class="control-label">类型</label>
										<select class="form-control" id="type" name="type" value="${vo.type}">
											<option value="0">web菜单</option>
											<option value="1">app头部菜单</option>
											<option value="2">app中间菜单</option>
											<option value="3">app底部菜单</option>
										</select>
									</div>
								</div>
							</form>
	                    </div>
	               	</div>
	           	</div>
	        </div>
	    </div>
	</section>
	<%@ include file="/resources/include/common_lte_js.jsp"%>
	<script>
    	$(function(){
        	var dialog;
           	try {
     			dialog = top.dialog.get(window);
     		} catch (e) {
     			return;
     		}

     		var $form = $("#groupForm");
        	$form.bootstrapValidator({
        		excluded: [":disabled"],
        	    fields:{
        	    	"menuName": {
                        validators: {
                            notEmpty: {
                                message: "菜单名称不能为空"
                            }
                        }
                    },
                    "menuSeq": {
                        validators: {
                        	notEmpty: {
                                message: "序列号码不能为空"
                            },
                            regexp: {
                                regexp: /^[1-9]\d*$/,
                                message: '请输入正整数'
                            }
                        }
                    }
        	    }
        	});
     		
     		$("#closeBtn").on("click",function(){
     			dialog.close();
				dialog.remove();
     		})
     		
     		var arr = Comm.getData("api/dev/dynamicPageAjaxQuery");
     		var options = "<option value=''></option>";
     		for(var i=0;i<arr.length;i++){
     			options += "<option value='" + arr[i].id + "'>" + arr[i].name + "</option>";
     		}
     		$("#dynamicPageId").html(options).select2({
				 width:"100%",
				 placeholder:'请选择',
				 allowClear:!0,
				 language:'zh-CN'
			});
     		var dynamicPageId = $("#dynamicPageId").attr("value");
     		if(dynamicPageId){
     			$("#dynamicPageId").val(dynamicPageId).trigger("change"); 
     		}
     		
          	$("#saveBtn").click(function(){   
          		if(!validateForm()){
	    			return false;
	    		}
         		var menuAddress = $("#menuAddress").val();
         	   	var dynamicpageId = $("#dynamicPageId").val();
         	   	if(menuAddress && dynamicpageId){
         		   	alert("地址和动态表只能选填一项");
         		   	return false;
         	   	}
         		$.ajax({
 					type : "POST",
 					url : "<%=basePath%>dev/punMenuAJAXSave.do",
					data : $("#groupForm").serialize(),
					success : function(data) {
						if (data.status == 1) {
							alert(data.message);
						} else {
							dialog.close(data.data);
							dialog.remove();
						}
					}
				})
				return false;
			});		
    	});
	</script>
</body>
</html>
