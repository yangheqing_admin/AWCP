<%@ page language="java" contentType="text/html; charset=UTF-8" isErrorPage="true" pageEncoding="UTF-8"%>
<%@ page import="java.io.*,java.util.*"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE HTML>
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>出现错误</title>
	<link rel="stylesheet" href="/template/AdminLTE/css/bootstrap.min.css">
	<link rel="stylesheet" href="/template/AdminLTE/css/font-awesome.min.css">
    <link rel="stylesheet" href="/template/AdminLTE/css/ionicons.min.css">
    <link rel="stylesheet" href="/template/AdminLTE/css/artDialog/dialog.css" />
    <link rel="stylesheet" href="/template/AdminLTE/css/AdminLTE.css">
    <link rel="stylesheet" href="/template/AdminLTE/css/_all-skins.min.css">
    <link rel="stylesheet" href="/template/AdminLTE/css/main.css">
	<script src="<%=basePath %>resources/JqEdition/jquery-2.2.3.min.js"></script>
	<script type="text/javascript">
		if(window!=top){
			top.location.href = location.href;
		}
	</script>
</head>
<body>
	<section class="content">
		<div class="callout callout-danger">
          	<h4>登录超时或无权访问。请点击下方按钮登录</h4>
        </div>
        <div class="buttons" style="text-align: center;">
			<a class="btn btn-info" href="javascript:;" data-url="<%=basePath%>login.html">重新登录</a>
		</div>
	</section>
	<script>
		$(function(){
			$("a.btn").on("click",function(){
				window.parent.location.href = $(this).data("url");
			})
		})
	</script>
</body>
</html>
