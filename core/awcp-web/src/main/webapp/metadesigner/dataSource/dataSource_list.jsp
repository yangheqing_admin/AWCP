<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html >
<html>
<head>
    <title>数据源管理</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body>
	<section class="content">
	    <div class="container-fluid">
	        <div class="opeBtnGrop">
	            <button type="button" class="btn btn-primary" id="addBtn"><i class="fa fa-plus"></i> 新增</button>
	            <button type="button" class="btn btn-danger" id="deleteBtn"><i class="fa fa-trash"></i> 删除</button>
	            <button type="button" class="btn btn-default" id="closeBtn"><i class="fa fa-close"></i> 关闭</button>
	        </div>
	        <div class="row">
	            <div class="col-md-12">
	                <div class="box box-info">
	                    <div class="box-body">
	                        <form method="post" id="dataSourceList">
	                            <input type="hidden" id="currentPage" name="currentPage" 
	                            	value="${list.getPaginator().getPage()}" />
	                            <input type="hidden" id="pageSize" name="pageSize" 
	                            	value="${list.getPaginator().getLimit()}" />
	                            <input type="hidden" id="totalCount" name="totalCount" 
	                            	value="${list.getPaginator().getTotalCount()}" />
	                            <div class="row form-group" id="searchDiv">
	                                <div class="col-md-4">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">数据源名称</span>
	                                        <input name="name" class="form-control" id="name" type="text"/>
	                                        <span class="input-group-btn">
						                      	<button type="button" class="btn btn-info btn-flat" id="searchBtn">搜索</button>
						                    </span>
	                                    </div>
	                                </div>
	                            </div>
	                            <table class="table table-hover">
	                                <thead>
		                                <tr>
		                                    <th class="hidden"></th>
		                                    <th data-width="" data-field="" data-checkbox="true"></th>
		                                    <th>数据源名称</th>
		                                    <th>初始化连接数(个)</th>
		                                    <th>最小连接数(个)</th>
		                                    <th>最大连接数(个)</th>
		                                </tr>
	                                </thead>
	                                <tbody>
		                                <c:forEach items="${list}" var="k">
		                                    <tr>
		                                        <td class="hidden formData"><input type="hidden" value="${k.id}"></td>
		                                        <td></td>
		                                        <td><a href="javascript:void(0);"
		                                        	data-url="<%=basePath%>/dataSourceManage/get.do?id=${k.id}">${k.name }</a></td>
		                                        <td>${k.prototypeCount}</td>
		                                        <td>${k.minimumConnectionCount}</td>
		                                        <td>${k.maximumConnectionCount }</td>
		                                    </tr>
		                                </c:forEach>
	                                </tbody>
	                            </table>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
    <%@ include file="/resources/include/common_lte_js.jsp"%>
    <script type="text/javascript">
        $(function () {
            var count = 0;//默认选择行数为0
            $(".table").bootstrapTable({
                pageSize:parseInt($("#pageSize").val()),
                pageNumber:parseInt($("#currentPage").val()),
                totalRows:parseInt($("#totalCount").val()),
                sidePagination:"server",
                pagination:true,
                onPageChange:function(number, size){
                    $("#pageSize").val(size);
                    $("#currentPage").val(number);
                    $("#dataSourceList").submit();
                },
                onClickRow:function(row,$element,field){
                    var $checkbox=$element.find(":checkbox").eq(0);
                    if($checkbox.get(0).checked){
                        $checkbox.get(0).checked=false;
                        $element.find("input[type='hidden']").removeAttr("name","id");
                    }else{
                        $checkbox.get(0).checked=true;
                        $element.find("input[type='hidden']").attr("name","id");
                    }
                    count = $("input[name='id']").length;
                },
                onCheck: function(row,$element){
                    $element.closest("tr").find("input[type='hidden']").attr("name","id");
                    count = $("input[name='id']").length;
                },
                onUncheck:function(row,$element){
                    $element.closest("tr").find("input[type='hidden']").removeAttr("name");
                    count = $("input[name='id']").length;
                },
                onCheckAll: function (rows) {
                    $.each(rows,function(i,e){
                        $("input[value='"+$($.trim(e["0"])).attr("value")+"']").attr("name","id");
                    });
                    count = $("input[name='id']").length;
                },
                onUncheckAll: function (rows) {
                    $.each(rows,function(i,e){
                        $("input[value='"+$($.trim(e["0"])).attr("value")+"']").removeAttr("name");
                    });
                    count = $("input[name='id']").length;
                }
            });

            function openDialog(url){
            	top.dialog({
                    id: 'add-dataSource' + Math.ceil(Math.random() * 10000),
                    url: url,
                    skin: "col-md-6",
                    title: "添加数据源",
                    onclose: function () {
                    	if(this.returnValue){
                        	location.href = "<%=basePath%>dataSourceManage/selectPage.do";
                    	}
                    }
                }).showModal();
            }
            
            //新增数据源
            $("#addBtn").click(function () {
                var url = "<%=basePath %>dataSourceManage/get.do";
                openDialog(url);
            });
           
			//编辑数据源
            $("#dataSourceList").find("a").on("click",function(){
            	var url = $(this).attr("data-url");
            	openDialog(url);
            });

            //删除数据源
            $("#deleteBtn").click(function () {
                if (count < 1) {
                    Comm.alert("请至少选择一项进行操作");
                    return false;
                }
                Comm.confirm("确认删除？",function(){
                    $("#dataSourceList").attr("action", "remove.do").submit();
                })
            });
            
            //搜索
            $("#searchBtn").click(function(){
                $("#dataSourceList").submit();
            });

            //关闭
            $("#closeBtn").on("click",function(){
            	closePage();
            });
        });
    </script>
</body>
</html>