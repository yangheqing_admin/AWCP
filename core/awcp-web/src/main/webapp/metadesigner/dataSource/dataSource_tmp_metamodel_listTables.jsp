<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>数据库表列表</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body id="main">
	<section class="content">
		<div class="opeBtnGrop">
	    	<button class="btn btn-success" id="saveBtn"><i class="fa fa-save"></i> 创建元数据</button>
		    <button class="btn btn-default" id="undoBtn"></i><i class="fa fa-reply"></i> 取消</button>
    	</div>	
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" id="groupForm">
							<div class="form-group">
								<div class="col-md-3">
									<label class="checkbox-inline"> 
										<input name="table" type="checkbox"  value="1" id="selectAll">全选
									</label>
								</div>
								<div class="col-md-3">
									<div class="input-group input-group-sm">
						                <input type="text" id="filterText" class="form-control">
						                <span class="input-group-btn">
						                	<button type="button" id="filter" class="btn btn-info btn-flat">过滤</button>
						                </span>
						            </div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<table id="tableContent" style="border-style:none;width:100%"></table>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<%@ include file="/resources/include/common_lte_js.jsp" %>	
	<script src="<%=basePath%>resources/scripts/jquery.serializejson.min.js"></script>		
	<script>
     	$(document).ready(function(){
     		$("#undoBtn").on("click",function(){
     			location.href = basePath + "metaModel/selectPage.do";
     		});
     		
     		$("#selectAll").click(function(){
				var v = $(this).prop("checked");
				$("input[name='tableNames']").prop("checked", v);
			});
     		
     		var items = '${tables}';
			items = items.substring(1,items.length-1);
			var itemArr = items.split(",");
			setTableContent(itemArr);
    			
			$("#filter").on("click",function(){
				var filterText = $("#filterText").val();
				if(filterText){
					var arr = [];
					for(var i=0;i<itemArr.length;i++){
						if(itemArr[i].indexOf(filterText) != -1){
							arr.push(itemArr[i]);
						}
					}
					setTableContent(arr);
				} else{
					setTableContent(itemArr);
				}
				$("#selectAll").prop("checked",false);
			});
			
			$("#saveBtn").click(function(){
				//如果选择了
				var selectTables = "";
				$("input[name='tableNames']:checked").each(function(i){
					selectTables += $(this).val() + ",";
				});
				if(selectTables.length > 0){
  					//删除最后一个","
  					var data = $("#groupForm").serializeJSON();
  					selectTables = selectTables.substr(0,selectTables.length-1);
  					data.tableNames = selectTables;
  					$.ajax({
  						type : "POST",
  						url : basePath + "metaModel/addByDb.do",
  						data : data,
  						async : false,
  						success : function(rtn) {	  							
  							if(rtn.result == "1"){
  								Comm.alert("生成成功！",function(){
  									location.href = basePath + "metaModel/selectPage.do";
  								});
  							} else {
  								Comm.alert(rtn.message);
  							}
  						}
  					});
				} else {
					Comm.alert("请选择需要需要转换的数据库表。");
				}
   			});    			
    	});
     	
     	function setTableContent(itemArr){
     		var html = "";
			var index = 0;
			var temp = itemArr.length%4;
			for(var i=0;i<temp;i++){
				itemArr.push("");
			}
			for(var i=0;i<itemArr.length;i++){
				var item = itemArr[i];
				var td = "";
				if(item){
					td = "<td style='width:25%;'><label class='checkbox-inline'>" +
						 "<input name='tableNames' type='checkbox' value='" + itemArr[i] +  "' /> " + itemArr[i] + 
						 "</label></td>";
				} else{
					td = "<td style='width:25%;'></td>";
				}
				if(i%4 == 0){
					html += "<tr>" + td;
				} else if(i%4 == 3){
					html += td + "</tr>";
				} else{
					html += td;
				}
			}
			$("#tableContent").html(html);
     	}
     </script>	
</body>
</html>