<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head lang="zh">
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body id="main">
	<section class="content">
    	<div class="opeBtnGrop">
	    	<div class="btn-group">
				<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-plus"></i> 新增  <span class="caret"></span> 
				</button>
	            <ul class="dropdown-menu" role="menu">
	                <li><a href="#" id="addBtn">新增</a></li>
	                <li><a href="#" id="addbydb">已有数据源创建</a></li>
	            </ul>
	        </div>		
			<button type="button" class="btn btn-success" id="updateBtn"><i class="fa fa-edit"></i> 修改</button>
			<button type="button" class="btn btn-success" id="relationBtn"><i class="fa fa-edit"></i> 关联属性</button>
			<button type="button" class="btn btn-info" id="synMetaBtn"><i class="fa fa-refresh"></i> 数据库同步到元数据</button>
			<button type="button" class="btn btn-warning" id="createTableBtn"><i class="fa fa-refresh"></i> 元数据同步到数据库</button>
			<button type="button" class="btn btn-warning" id="deleteOnly"><i class="fa fa-trash"></i> 仅删除元数据</button>
			<button type="button" class="btn btn-danger" id="deleteBtn"><i class="fa fa-trash"></i> 删除元数据及表</button>
			<button type="button" class="btn btn-default" id="closeBtn"><i class="fa fa-close"></i> 关闭</button>
    	</div>	
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form method="post" id="modelMetaList">
							<input type="hidden" id="currentPage" name="currentPage" value="${list.getPaginator().getPage()}">
							<input type="hidden" id="pageSize" name="pageSize" value="${list.getPaginator().getLimit()}">
							<input type="hidden" id="totalCount" name="totalCount" value="${list.getPaginator().getTotalCount()}">	
							<div class="row form-group">
								<div class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon">数据库表名</span>
										<input name="tableName" class="form-control" id="tableName" type="text"/>
									</div>								
								</div>
								<div class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon">模型名称(中)</span>
										<input name="modelName" class="form-control" id="modelName" type="text"/>
									</div>									
								</div>
								<div class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon">模型名称(英)</span>
										<input name="modelCode" class="form-control" id="modelCode" type="text"/>
									</div>
								</div>																							
								<div class="col-md-3 btn-group">
									<button class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>									
								</div>								
							</div>							
							<table class="table table-hover">  
							    <thead>
							        <tr>
							        	<th class="hidden"></th>
							        	<th data-width="" data-field="" data-checkbox="true"></th>
							            <th>模型名称</th>
							            <th>模型编号</th>							            
							            <th>数据表名</th>
							            <th>模型描述</th>
							        </tr>
							    </thead>
							    <tbody>
									<c:forEach items="${list}" var="vo" varStatus="status">
							          	<tr> 	
							            	<td class="hidden formData"><input type="hidden" value="${vo.id}"></td>
							            	<td></td>
											<td>${vo.modelName }</td>
											<td>${vo.modelCode }</td>										
											<td>${vo.tableName }</td>
											<td>${vo.modelDesc }</td>																				
							          	</tr>
							    	</c:forEach>
								</tbody>
							</table>  
						</form>
					</div>				
				</div>				
			</div>
		</div>
    </section>
	<%@ include file="/resources/include/common_lte_js.jsp"%>
	<script>
		$(document).ready(function(){
			var count = 0;//默认选择行数为0
			$(".table").bootstrapTable({
				pageSize:parseInt($("#pageSize").val()),
				pageNumber:parseInt($("#currentPage").val()),
				totalRows:parseInt($("#totalCount").val()),
				sidePagination:"server",
				pagination:true,
				onPageChange:function(number, size){
					$("#pageSize").val(size);
					$("#currentPage").val(number);
					$("#modelMetaList").submit();
				},
				onClickRow:function(row,$element,field){
					var $checkbox=$element.find(":checkbox").eq(0);
					if($checkbox.get(0).checked){
						$checkbox.get(0).checked=false;
						$element.find("input[type='hidden']").removeAttr("name","id");
					}else{
						$checkbox.get(0).checked=true;
						$element.find("input[type='hidden']").attr("name","id");
					}
					count = $("input[name='id']").length;
				},
				onClickCell:function(field,value,row,$element){

				},
				onCheck: function(row,$element){
					$element.closest("tr").find("input[type='hidden']").attr("name","id");
					count = $("input[name='id']").length;
				},
				onUncheck:function(row,$element){
					$element.closest("tr").find("input[type='hidden']").removeAttr("name");
					count = $("input[name='id']").length;
				},
				onCheckAll: function (rows) {
					$.each(rows,function(i,e){
						$("input[value='"+$($.trim(e["0"])).attr("value")+"']").attr("name","id");
					});
					count = $("input[name='id']").length;
				},
				onUncheckAll: function (rows) {
					$.each(rows,function(i,e){
						$("input[value='"+$($.trim(e["0"])).attr("value")+"']").removeAttr("name");
					});
					count = $("input[name='id']").length;
				}
			});
		  	
			//新增
      		$("#addBtn").click(function(){
      			var url = "toggle.do";
      			location.href = url;
      			return false;
      		});
          	
			//数据库新增
      		$("#addbydb").click(function(){
      			location.href = basePath + "dataSourceManage/listTables.do";
      			return false;
      		});
          	
      		//修改
      		$("#updateBtn").click(function(){
      			if(count == 1){
      				$("#modelMetaList").attr("action","get.do").submit();
      			}else{
      				Comm.alert("请选择一项进行操作");
      			}
      			return false;
      		});
      		
      		//关联属性
          	$("#relationBtn").click(function(){
          		if(count != 1){
          			Comm.alert("请选择一项进行操作");
          		} else{
	          		$("#modelMetaList").attr("action",basePath + "metaModelItems/queryResultByParams.do").submit();
          		}
      			return false;
          	});
      		
      		//只删除元数据
      		$("#deleteOnly").click(function(){
      			if(count < 1){
      				Comm.alert("请至少选择一项进行操作");
          			return false;
      			}
      			Comm.confirm("确定删除元数据？",function(){
      				$("#modelMetaList").attr("action",basePath + "metaModel/removeModel.do").submit();
      			});
      			return false;
      		});
      		
          	//删除元数据和表
          	$("#deleteBtn").click(function(){
          		if(count < 1){
          			Comm.alert("请至少选择一项进行操作");
          			return false;
          		}
          		Comm.confirm("确定删除元数据和表？",function(){
          			$("#modelMetaList").attr("action","remove.do").submit();
          		});
      			return false;
          	});
          	
          	//元数据同步到数据库
          	$("#createTableBtn").click(function(){
          		if(count != 1){
          			Comm.alert("请选择一项进行操作");
          			return false;
          		}
               	Comm.confirm("此操作会覆盖数据库表，并清除数据",function(){
            	   	$.post(basePath + "metaModel/createTable.do",{id:$("input[name='id']").val()},function(data){
                       	if(data.status==0){
                           Comm.alert("创建成功");
						}else{
                           Comm.alert(data.message);
						}
					});
               	});
                return false;
            });
          	
          	//数据库同步到元数据
            $("#synMetaBtn").click(function(){
                if(count != 1){
                    Comm.alert("请选择一项进行操作");
                } else{
                	$.post(basePath + "metaModel/synchronizedMeta.do",{id:$("input[name='id']").val()},function(data){
                       	if(data.status==0){
                           Comm.alert("同步成功");
						}else{
                           Comm.alert(data.message);
						}
					});
                }
                return false;
            });  
            
          	//关闭
            $("#closeBtn").on("click",function(){
            	closePage();
            });
    	});
	</script>
</body>
</html>
