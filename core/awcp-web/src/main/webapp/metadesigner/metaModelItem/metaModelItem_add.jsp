<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>元数据页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body id="main">
	<section class="content">
		<div class="opeBtnGrop">
	    	<button class="btn btn-primary" id="saveBtn"><i class="fa fa-save"></i> 保存</button>
		    <button class="btn btn-default" id="undoBtn"></i><i class="fa fa-reply"></i> 取消</button>
    	</div>	
    	<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" id="groupForm">
							<input type="hidden" id="type" value="${type }" /> 
									<input type="hidden" id="modelId" name="modelId" value="${modelId }" />
							<div class="form-group">
								<div class="customGroup">	
									<div class="col-md-4">
										<label class="control-label required">数据库字段名</label>
										<input name="itemCode" class="form-control" id="itemCode" type="text" />
									</div>
								</div>
								<div class="customGroup">	
									<div class="col-md-4">
										<label class="control-label">属性类型</label>
										<select name="itemType" class="form-control">
											<option value="int">int</option>
											<option value="bigint">bigint</option>
											<option value="float">float</option>
											<option value="double">double</option>
											<option value="varchar">varchar</option>
											<option value="char">char</option>
											<option value="text">text</option>
											<option value="date">date</option>
											<option value="datetime">datetime</option>
										</select>
									</div>
								</div>
								<div class="customGroup">	
									<div class="col-md-4">
										<label class="control-label">属性长度</label>
										<input name="itemLength" class="form-control" id="itemLength" type="text" />
									</div>
								</div>
							</div>
							<div class="form-group">
								
								<div class="customGroup">	
									<div class="col-md-4">
										<label class="control-label">是否主键</label>
										<select name="usePrimaryKey" id="usePrimaryKeys" class="form-control">
											<option value="0" selected="selected">NO</option>
											<option value="1">YES</option>
										</select>
									</div>
								</div>
								<div class="customGroup">	
									<div class="col-md-4">
										<label class="control-label">是否索引</label>
										<select data-placeholder="请选择是否索引..." name="useIndex"
											class="form-control" tabindex="2">
											<option value="0" selected="selected">无</option>
											<option value="-1">主键</option>
											<option value="1">唯一</option>
											<option value="2">普通</option>
										</select>
									</div>
								</div>
								<div class="customGroup">	
									<div class="col-md-4">
										<label class="control-label">是否为空</label>
										<select id="useNull" class="form-control" name="useNull">
											<option value="0" id="un1">NO</option>
											<option value="1" id="un2" selected="selected">YES</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="customGroup">	
									<div class="col-md-4">
										<label class="control-label">默认值</label>
										<input name="defaultValue" class="form-control" id="defaultValue" type="text" />
									</div>
								</div>
								<div class="customGroup">	
									<div class="col-md-8">
										<label class="control-label">备注</label>
										<input name="remark" class="form-control" id="remark" type="text" />
									</div>
								</div>
							</div>
						</form>
		       		</div>
		       	</div>
		    </div>
		</div>
    </section>
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script type="text/javascript">
    	$(document).ready(function(){
    		var $form = $("#groupForm");
        	$form.bootstrapValidator({
        		excluded: [":disabled"],
        	    fields:{
                    "itemCode": {
                        validators: {
                        	notEmpty: {
                                message: "字段名称不能为空"
                            }
                        }
                    },
                    "itemLength": {
                    	validators:{
                    		callback: {
                    			message: '该字段类型请输入长度',
                    			callback: function(value, validator) {
                    		    	var itemType = $("[name='itemType']").val();
                    		    	var itemLength = $("[name='itemLength']").val();
                    		    	if(itemType!="date" && itemType!="datetime" && itemType!="text"){
                    		    		if(!itemLength){
                    		    			return false;
                    		    		}
                    		    	}
                    		    	return true;
                    		 	}
               				}
                    	}
                    }
        	    }
        	});
        	
	    	$("#saveBtn").click(function(){   
	    		if(!validateForm()){
	    			return false;
	    		}
	    		var data = myFun.ajaxExecute("metaModelItems/save.do",$("#groupForm").serialize());
	    		if(data.status == "0"){
	    			Comm.alert("保存成功",function(){
	    				location.href = basePath + "metaModelItems/queryPageResult.do?modelId=${modelId}";
	    			})
	    		} else if(data.message){
	    			Comm.alert(data.message);
	    		} else{
	    			Comm.alert("保存失败");
	    		}
				return false;
			});
    		
       		if('${type}'=='yes'){
       			$("#usePrimaryKeys").attr("disabled","true");
       		}
       			
       		$("#usePrimaryKey").change(function(){
       			if($(this).val()==1){
       				$("#un1").attr("selected","selected");
           			$("#useNull").attr("disabled","false");
       			}else{
       				$("#useNull").removeAttr("disabled");
       			}
       		});
       			
       		$("#undoBtn").on("click",function(){
       			location.href = basePath + "metaModelItems/queryResultByParams.do?id=${modelId }";
       		});
       	});
	</script>	
</body>
</html>