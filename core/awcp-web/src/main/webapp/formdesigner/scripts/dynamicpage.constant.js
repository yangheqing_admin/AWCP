var pageConstant = {
	type : "",
	getComponentTypes : function(){
		if(this.type == null || this.type == '') {
			this.type = new Map();
			this.type.put("1001", "单行文本框");
			this.type.put("1002", "日期文本框");
			this.type.put("1003", "多选复选框");
			this.type.put("1004", "radio单选按钮");
			this.type.put("1005", "多行输入框");
			this.type.put("1006", "下拉选项框");
			this.type.put("1008", "列框");
			this.type.put("1009", "标签");
			this.type.put("1010", "隐藏框");
			this.type.put("1011", "文件上传框");
			this.type.put("1012", "包含组件框");
			this.type.put("1016", "图片上传框");
			this.type.put("1017", "表格组件");
			this.type.put("1019", "富文本框");
			this.type.put("1020", "用户选择框");
			this.type.put("1033", "级联下拉框");
            this.type.put("1036","搜索条件");
            this.type.put("1043","流程意见组件");
		}
		return this.type;
	}
}