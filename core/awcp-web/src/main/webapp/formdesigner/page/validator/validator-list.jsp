<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="renderer" content="webkit">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>校验库列表</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body>
	<section class="content">
    	<div class="opeBtnGrop">
    		<c:choose>
				<c:when test="${ isSelect }">
					<button type="button" class="btn btn-info" id="submitBtn"><i class="fa fa-cog"></i> 确定</button>
				</c:when>
				<c:otherwise>
					<button class="btn btn-primary" id="addBtn"><i class="fa fa-plus"></i> 新增 </button>
					<button class="btn btn-danger" id="deleteBtn"><i class="fa fa-trash"></i> 删除</button>			
					<button class="btn btn-info" id="searchBtn"><i class="fa fa-search"></i> 搜索</button>
					<button class="btn btn-default" id="closePage"><i class="fa fa-close"></i> 关闭</button>
				</c:otherwise>
			</c:choose>			
    	</div>	
    	<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form method="post" id="validatorList" action="<%=basePath %>fd/validator/list.do">
							<input type="hidden" id="currentPage" name="currentPage" value="${vos.getPaginator().getPage()}" />
							<input type="hidden" id="pageSize" name="pageSize" value="${vos.getPaginator().getLimit()}" />
							<input type="hidden" id="totalCount" name="totalCount" value="${vos.getPaginator().getTotalCount()}" />
							<input type="hidden" name="isSelect" value="${isSelect }" />	
							<div class="row form-group">	
								<div class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon">校验名称</span>
										<input name="name" class="form-control" id="name" type="text" value="${name }"/>
									</div>
								</div>
								<div class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon">校验描述</span>
										<input name="validatorDes" class="form-control" id="validatorDes" type="text" 
											value="${validatorDes }"/>
									</div>
								</div>									
							</div>	
							<table class="table table-hover">
								<thead>
									<tr>
										<th class="hidden"></th>
							        	<th data-checkbox="true"></th>
							            <th data-width="30%">名称</th>
										<th>描述</th>
									</tr>
								</thead>
								<tbody>	
								<c:forEach items="${vos}" var="vo">
									<tr>
										<td class="hidden formData"><input type="hidden" value="${vo.id}"></td>
							            <td></td>
										<td>
											<c:choose>
				 								<c:when test="${ isSelect }">${vo.name}</c:when>
				 								<c:otherwise>
													<a href="<%=basePath%>fd/validator/edit.do?_selects=${vo.id}">${vo.name}</a> 
												</c:otherwise>
											</c:choose>	
										</td>
										<td>
											<span class="text-ellipsis">${vo.description }</span>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>	
						</form>
					</div>				
				</div>				
			</div>
		</div>
    </section>
    <%@ include file="/resources/include/common_lte_js.jsp"%>	
	<script type="text/javascript">
		$(document).ready(function(){
			var count = 0;//默认选择行数为0
			$(".table").bootstrapTable({
				pageSize:parseInt($("#pageSize").val()),
				pageNumber:parseInt($("#currentPage").val()),
				totalRows:parseInt($("#totalCount").val()),
				sidePagination:"server",
				pagination:true,
				onPageChange:function(number, size){
					$("#pageSize").val(size);
					$("#currentPage").val(number);
					$("#validatorList").submit();
				},
				onClickRow:function(row,$element,field){
					var $checkbox=$element.find(":checkbox").eq(0);
					if($checkbox.get(0).checked){
						$checkbox.get(0).checked=false;
						$element.find("input[type='hidden']").removeAttr("name","_selects");
					}else{
						$checkbox.get(0).checked=true;
						$element.find("input[type='hidden']").attr("name","_selects");
					}
					count = $("input[name='_selects']").length;
				},
				onClickCell:function(field,value,row,$element){
	
				},
				onCheck: function(row,$element){
					$element.closest("tr").find("input[type='hidden']").attr("name","_selects");
					count = $("input[name='_selects']").length;
				},
				onUncheck:function(row,$element){
					$element.closest("tr").find("input[type='hidden']").removeAttr("name");
					count = $("input[name='_selects']").length;
				},
				onCheckAll: function (rows) {
					$.each(rows,function(i,e){
						$("input[value='"+$($.trim(e["0"])).attr("value")+"']").attr("name","_selects");
					});
					count = $("input[name='_selects']").length;
				},
				onUncheckAll: function (rows) {
					$.each(rows,function(i,e){
						$("input[value='"+$($.trim(e["0"])).attr("value")+"']").removeAttr("name");
					});
					count = $("input[name='_selects']").length;
				}
			});
		  	
	    	//新增
			$("#addBtn").click(function(){
				location.href = basePath + "fd/validator/edit.do";
				return false;
			});
			 
			//删除
	    	$("#deleteBtn").click(function(){
	    		if(count < 1){
	    			Comm.alert("请至少选择一项进行操作");
	    			return false;
	    		}
	    		Comm.confirm("确定删除？",function(){
	    			$("#validatorList").attr("action","<%=basePath%>fd/validator/delete.do").submit();
	    		});
				return false;
	    	});
	    	
			//搜索
			$("#searchBtn").on("click",function(){
				$("#validatorList").submit();
			});
			
	    	//确认选择
	    	$("#submitBtn").click(function(){
	    		try {
					var dialog = top.dialog.get(window);
				} catch (e) {
					
				}
				if(dialog != null){
					if(count != 1){
						Comm.alert("请选择一条记录进行操作");
						return false;
					}
					var data = $("#validatorList input[name='_selects']").val();
					dialog.close(data);
					dialog.remove();
				}
				return false;
	    	});
	    	
	    	//关闭
	    	$("#closePage").on("click",function(){
	    		closePage();
	    	});
	    	
	    	$(document).keyup(function(event) {
		    	if (event.keyCode == 13) {
		    		$("#validatorList").submit();
		    	}
		    });
		});
	</script>
</body>
</html>


