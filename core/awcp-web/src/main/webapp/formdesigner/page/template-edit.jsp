<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>模板编辑页面</title>
	<link rel="stylesheet" href="<%=basePath%>resources/plugins/highlight/styles/monokai.css">  
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body id="main">
	<section class="content">
		<div class="opeBtnGrop">
	    	<button class="btn btn-primary" id="saveBtn"><i class="fa fa-save"></i> 保存</button>
		    <button class="btn btn-default" id="undoBtn"></i><i class="fa fa-reply"></i> 取消</button>
    	</div>
		<div class="row" id="dataform">
			<c:if test="${result!=null&&''!=result}">
				<span style="color: red">(${result})</span>
			</c:if>
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" id="groupForm" action="<%=basePath%>pfmTemplateController/saveOrUpdate.do" method="post" enctype="multipart/form-data">
							<div class="C_addForm">
								<input type="hidden" name="id" value="${vo.id}" />
								<input type="hidden" name="fileLocation" value="${vo.fileLocation}" />
								<div class="form-group">
									<div class="col-md-4">
										<label class="control-label required">模板名称：</label>
										<input name="fileName" class="form-control" id="fileName"
													type="text" placeholder="" value="${vo.fileName}" />
									</div>
									<div class="col-md-8">
										<label class="control-label">模板描述：</label>
										<input name="description" class="form-control" id="description"
											type="text" placeholder="" value="${vo.description}" />
									</div>
								</div>
								<div class="form-group">	
									<div class="col-md-12">
										<a class="btn btn-success btn-xs edit"><i class="fa fa-edit"></i> 编辑</a>
										<a class="btn btn-info btn-xs view"><i class="fa fa-search"></i> 查看</a>
									</div>							
									<div class="col-md-12">
										<label class="control-label">模版内容：</label>
										<textarea style="display: none;" name='content' class='form-control' 
											onpropertychange= "this.style.posHeight=this.scrollHeight ">${vo.content }</textarea>
										<pre><code></code></pre>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>	 
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script src="<%=basePath%>resources/plugins/highlight/highlight.pack.js"></script>
	<script type="text/javascript">
		$(function(){
			setHighLight();
			
			$("#undoBtn").on("click",function(){
				location.href = "<%=basePath%>pfmTemplateController/pageList.do";
			});
			
			$("#saveBtn").on("click",function(){
				$("#groupForm").submit();
			});
			
			$(".edit").click(function(){
				if($("textarea").is(":hidden")){
					$("pre").hide();
					$("textarea").show();
					setHeight($("textarea"));
				}
			});
			
			$(".view").click(function(){
				if($("code").is(":hidden")){
					$("pre").show();
					$("textarea").hide();
					setHighLight();
				}
			});
		});
		
		function setHeight($element) {
		  	$element.css({'height':'auto','overflow-y':'hidden'}).height($element.get(0).scrollHeight);
		}
		
		function setHighLight(){
			var value=hljs.highlight("html",$("textarea").val(),true,false).value;
			$("code").html(value);
			$("pre code").each(function(i,e){
				hljs.highlightBlock(e);
			})
		}	
	</script>
</body>
</html>
