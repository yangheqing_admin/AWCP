<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()	+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="renderer" content="webkit">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>样式列表</title>
	<base href="<%=basePath%>">
	<%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body id="main">
	<section class="content">
	    <div class="container-fluid">
	        <div class="opeBtnGrop">
				<button type="button" class="btn btn-success" id="addBtn"><i class="fa fa-plus"></i> 新增</button>
				<button type="button" class="btn btn-danger" id="deleteBtn"><i class="fa fa-trash"></i> 删除</button>
				<button type="button" class="btn btn-default" id="closeBtn"><i class="fa fa-close"></i> 关闭</button>
			</div>
			<div class="row">
	            <div class="col-md-12">
	                <div class="box box-info">
	                    <div class="box-body">
	                        <form method="post" id="dataList">
	                            <input type="hidden" id="currentPage" name="currentPage" 
	                            	value="${vos.getPaginator().getPage()}" />
	                            <input type="hidden" id="pageSize" name="pageSize" 
	                            	value="${vos.getPaginator().getLimit()}" />
	                            <input type="hidden" id="totalCount" name="totalCount" 
	                            	value="${vos.getPaginator().getTotalCount()}" />
	                            <div class="row form-group" id="searchDiv">
	                            	<div class="col-md-3">
										<div class="input-group">
											<span class="input-group-addon">样式名称</span>
											<input name="name" class="form-control" id="name" type="text" value="${name }"/>
										</div>
									</div>
									<div class="col-md-3">
										<div class="input-group">
											<span class="input-group-addon">样式描述</span>
											<input name="styleDesc" class="form-control" id="styleDesc" type="text" value="${styleDesc }"/>
										</div>
									</div>
									<div class="col-md-3">
										<button type="button" class="btn btn-info" id="searchBtn">
											<i class="fa fa-search"></i> 搜索
										</button>
									</div>
	                            </div>
	                            <table class="table table-hover">
	                                <thead>
		                                <tr>
		                                    <th class="hidden"></th>
		                                    <th data-width="" data-field="" data-checkbox="true"></th>
											<th>名称</th>
											<th>描述</th>
											<th>内容</th>
		                                </tr>
	                                </thead>
	                                <tbody>
		                                <c:forEach items="${vos}" var="vo">
		                                    <tr>
		                                        <td class="hidden formData"><input type="hidden" value="${vo.id}"></td>
		                                        <td></td>
		                                        <td><a href="<%=basePath%>/fd/style/edit.do?id=${vo.id}" >${vo.name}</a></td>
												<td><span class="text-ellipsis">${vo.description }</span></td>
												<td class="text-ellipsis">${vo.content }</td>
		                                    </tr>
		                                </c:forEach>
	                                </tbody>
	                            </table>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>			
	<%@ include file="/resources/include/common_lte_js.jsp"%>
    <script type="text/javascript">
        $(function () {
            var count = 0;//默认选择行数为0
            $(".table").bootstrapTable({
                pageSize:parseInt($("#pageSize").val()),
                pageNumber:parseInt($("#currentPage").val()),
                totalRows:parseInt($("#totalCount").val()),
                sidePagination:"server",
                pagination:true,
                onPageChange:function(number, size){
                    $("#pageSize").val(size);
                    $("#currentPage").val(number);
                    $("#dataList").submit();
                },
                onClickRow:function(row,$element,field){
                    var $checkbox=$element.find(":checkbox").eq(0);
                    if($checkbox.get(0).checked){
                        $checkbox.get(0).checked=false;
                        $element.find("input[type='hidden']").removeAttr("name","id");
                    }else{
                        $checkbox.get(0).checked=true;
                        $element.find("input[type='hidden']").attr("name","id");
                    }
                    count = $("input[name='id']").length;
                },
                onCheck: function(row,$element){
                    $element.closest("tr").find("input[type='hidden']").attr("name","id");
                    count = $("input[name='id']").length;
                },
                onUncheck:function(row,$element){
                    $element.closest("tr").find("input[type='hidden']").removeAttr("name");
                    count = $("input[name='id']").length;
                },
                onCheckAll: function (rows) {
                    $.each(rows,function(i,e){
                        $("input[value='"+$($.trim(e["0"])).attr("value")+"']").attr("name","id");
                    });
                    count = $("input[name='id']").length;
                },
                onUncheckAll: function (rows) {
                    $.each(rows,function(i,e){
                        $("input[value='"+$($.trim(e["0"])).attr("value")+"']").removeAttr("name");
                    });
                    count = $("input[name='id']").length;
                }
            });
    	
         	//新增
			$("#addBtn").click(function(){
				location.href = basePath + "fd/style/edit.do";
				return false;
			})
		
	    	//删除
	    	$("#deleteBtn").click(function(){
	    		if(count < 1){
	    			Comm.alert("请至少选择一项进行操作");
	    			return false;
	    		}
	    		Comm.confirm("确定删除？",function(){
	    			$("#dataList").attr("action", basePath + "fd/style/delete.do").submit()
	    		});    		
				return false;
	    	});
            
            //搜索
            $("#searchBtn").click(function(){
                $("#dataList").submit();
            });
            	
            //关闭
	    	$("#closeBtn").on("click",function(){
	    		closePage();
	    	})
    	});
	</script>
</body>
</html>


