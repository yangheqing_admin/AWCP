<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html >
<html>
<head>
    <title>页面绑定管理</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body id="main">
	<section class="content">
	    <div class="container-fluid">
	        <div class="opeBtnGrop">
	        	<button type="button" class="btn btn-primary" id="addBtn"><i class="fa fa-plus"></i> 新增</button>
	            <button type="button" class="btn btn-success" id="updateBtn"><i class="fa fa-edit"></i>修改</button>
	            <button type="button" class="btn btn-danger" id="deleteBtn"><i class="fa fa-trash"></i> 删除</button>
	            <button type="button" class="btn btn-default" id="closeBtn"><i class="fa fa-close"></i> 关闭</button>
	        </div>
	        <div class="row">
	            <div class="col-md-12">
	                <div class="box box-info">
	                    <div class="box-body">
	                        <form method="post" id="dataList">
	                            <input type="hidden" id="currentPage" name="currentPage" 
	                            	value="${vos.getPaginator().getPage()}" />
	                            <input type="hidden" id="pageSize" name="pageSize" 
	                            	value="${vos.getPaginator().getLimit()}" />
	                            <input type="hidden" id="totalCount" name="totalCount" 
	                            	value="${vos.getPaginator().getTotalCount()}" />
	                            <div class="row form-group" id="searchDiv">
	                                <div class="col-md-4">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">动态页面名称</span>
											<input name="name" class="form-control" id="name" type="text" value="${name}" />
	                                        <span class="input-group-btn">
						                      	<button type="button" class="btn btn-info btn-flat" id="searchBtn">搜索</button>
						                    </span>
	                                    </div>
	                                </div>
	                            </div>
	                            <table class="table table-hover">
	                                <thead>
		                                <tr>
		                                    <th class="hidden"></th>
		                                    <th data-width="" data-field="" data-checkbox="true"></th>
											<th>PAGEID_PC</th>
											<th>PAGEID_PC_LIST</th>
											<th>PAGEID_APP</th>
											<th>PAGEID_APP_LIST</th>
		                                </tr>
	                                </thead>
	                                <tbody>
		                                <c:forEach items="${vos}" var="vo">
		                                    <tr>
		                                        <td class="hidden formData"><input type="hidden" value="${vo.id}"></td>
		                                        <td></td>
		                                        <td>${vo.PAGEID_PC}</td>
												<td>${vo.PAGEID_PC_LIST}</td>
												<td>${vo.PAGEID_APP}</td>
												<td>${vo.PAGEID_APP_LIST}</td>
		                                    </tr>
		                                </c:forEach>
	                                </tbody>
	                            </table>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>			
	<%@ include file="/resources/include/common_lte_js.jsp"%>
    <script type="text/javascript">
        $(function () {
            var count = 0;//默认选择行数为0
            $(".table").bootstrapTable({
                pageSize:parseInt($("#pageSize").val()),
                pageNumber:parseInt($("#currentPage").val()),
                totalRows:parseInt($("#totalCount").val()),
                sidePagination:"server",
                pagination:true,
                onPageChange:function(number, size){
                    $("#pageSize").val(size);
                    $("#currentPage").val(number);
                    $("#dataList").submit();
                },
                onClickRow:function(row,$element,field){
                    var $checkbox=$element.find(":checkbox").eq(0);
                    if($checkbox.get(0).checked){
                        $checkbox.get(0).checked=false;
                        $element.find("input[type='hidden']").removeAttr("name","id");
                    }else{
                        $checkbox.get(0).checked=true;
                        $element.find("input[type='hidden']").attr("name","id");
                    }
                    count = $("input[name='id']").length;
                },
                onCheck: function(row,$element){
                    $element.closest("tr").find("input[type='hidden']").attr("name","id");
                    count = $("input[name='id']").length;
                },
                onUncheck:function(row,$element){
                    $element.closest("tr").find("input[type='hidden']").removeAttr("name");
                    count = $("input[name='id']").length;
                },
                onCheckAll: function (rows) {
                    $.each(rows,function(i,e){
                        $("input[value='"+$($.trim(e["0"])).attr("value")+"']").attr("name","id");
                    });
                    count = $("input[name='id']").length;
                },
                onUncheckAll: function (rows) {
                    $.each(rows,function(i,e){
                        $("input[value='"+$($.trim(e["0"])).attr("value")+"']").removeAttr("name");
                    });
                    count = $("input[name='id']").length;
                }
            });
    	
            //搜索
            $("#searchBtn").click(function(){
                $("#dataList").submit();
            });
            
		  	//新增
			$("#addBtn").click(function(){
				location.href = basePath + "fd/list-bind.do";
				return false;
			});
		
			//修改
			$("#updateBtn").click(function(){
				if(count == 1){
					top.dialog({
						id: 'add-dialog' + Math.ceil(Math.random()*10000),
						title: '页面绑定',
						url: "formdesigner/page/dynamicPage-bind-update.jsp",
						data:{
							id:$("input[name='id']").val()
						},
						skin:"col-md-4",
						onclose: function () {
							if (this.returnValue) {
								Comm.alert("保存成功",function(){
									location.href = basePath + "common/user/list-bind.do";
								});
							}
						}
					}).showModal();
				}else{
					Comm.alert("请选择一项进行操作");
				}
				return false;
			});

	    	//删除
	    	$("#deleteBtn").click(function(){
	    		if(count < 1){
	    			Comm.alert("请至少选择一项进行操作");
	    			return false;
	    		}
	    		Comm.confirm("确定删除？",function(){
	    			$("#dataList").attr("action",basePath + "common/user/delete.do").submit()
	    		})
				return false;
	    	});
	    	
	    	$("#closeBtn").on("click",function(){
	    		closePage();
	    	})
    	});
	</script>
</body>
</html>


