/**
 * 动态页面配置中数据源操作js
 */
var dataSource = new Map();
var itemMap = new Map();
var sqlTableName = "";

//全选checkbox点击事件
$("#checkAllDatasource").click(function(){
	if($(this).prop("checked")){
		$(":checkbox[name='data']").prop("checked",true);
	}else{
		$(":checkbox[name='data']").prop("checked",false);
	}	
});

//初始化数据源表格
function initDataSource(){
	var model = $("#modelJsonArray").val();
	if(!empty(model)){
		var dataJson = JSON.parse(model);
		$.each(dataJson,function(idx,item){
			dataSource.put(item.id,item);
		});
	}
	freshDataTable();
}

//生成表格Html
function freshDataTable(){
	$("#datasourcebody").empty();
	dataSource.each(function(key,value,index){
		var str = "<tr id='" + value.id + "'><td><input type='checkbox' name='data' value='" + value.id + "'/></td>";
		str += "<td><a href='javascript:void(0);' onclick='editDatasource(\"" + value.id + "\")'>" + value.name + "</a></td>";
		str += "<td>" + value.description + "</td>";
		str += "</tr>";
		$("#datasourcebody").append(str);
	});
	return false;
}

//编辑数据源
function editDatasource(id){
	var to = null;
	if(!empty(id)){
		to = dataSource.get(id);
	}
	var pageId = $("#id").val();
	top.dialog({
		id: 'add-dataSource' + Math.ceil(Math.random()*10000),
		title: '数据源编辑',
		url: "fd/datadefine/edit.do",
		data:to,
		height : 600,
		width : 1000,
		onclose: function () {
			if (this.returnValue) {
				var ret= this.returnValue;
					dataSource.put(ret.id,ret);
					$("#modelJsonArray").val(dataSource.toJSON());
					$.ajax({
						url:"fd/updateData.do",
						async : false,
						type:"POST",
						data:{dataJson:dataSource.toJSON(),id:pageId},
						error : function(XMLHttpRequest, textStatus, errorThrown) {
							alert(errorThrown);
						},
						success:function(ret){
							if(ret=="1"){
								freshDataTable();
							}else{
								alert("出现异常！请重新操作！");
							}
							
						}
					});
				}
		}, 
	}).showModal(); 
	return false;
}

function freshSQL(){
	sqlTableName = models.get($("#modelCode").val());
	var sql="";
	if(!itemMap.isEmpty()){
		sql += "\"select "; 
		itemMap.each(function(key,value,index){
			if(value){
				sql += key + ",";
			}
		});
		if(","==sql.charAt(sql.length-1)){
			sql=sql.substring(0,sql.length-1);
		}
		sql += " from ";
		sql += sqlTableName;
		sql += "\";";
	}
	$("#sqlScript").val(sql);
}

//删除数据源
$("#deleteModel").click(function (){
	$(":checkbox[name='data']:checked").each(function(idx,item){
		var id = $(this).val();
		dataSource.remove(id);
	});
	var pageId = $("#id").val();
	$("#modelJsonArray").val(dataSource.toJSON());
	$.ajax({
		url:"fd/updateData.do",
		async : false,
		type:"POST",
		data:{dataJson:dataSource.toJSON(),id:pageId},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(errorThrown);
		},
		success:function(ret){
			if(ret=="1"){
				freshDataTable();
			}else{
				Comm.alert("出现异常！请重新操作");
			}
		}
	});
	return false;
});

//复制数据源
$("#copyModel").click(function (){
	var count = $(":checkbox[name='data']:checked").size();
	var _selects = new Array();
	$(":checkbox[name='data']:checked").each(function(){
		var value = $(this).val();
		_selects.push(value);
	});
	var pageId = $("#id").val();
	if(count < 1){
		Comm.alert("请选择数据源！");
		return false;
	} else {
		$(":checkbox[name='data']:checked").each(function(idx,item){
			var id = $(this).val();
			var temp = dataSource.get(id);
			var copy = {};
			for(var s in temp){
				copy[s] = temp[s];
			}
			copy.id = guidGenerator();
			copy.name = copy.name + new Date().getTime();
			dataSource.put(copy.id,copy);
		});
		$("#modelJsonArray").val(dataSource.toJSON());
		$.ajax({
			url:"fd/updateData.do",
			async : false,
			type:"POST",
			data:{dataJson:dataSource.toJSON(),id:pageId},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
			},
			success:function(ret){
				if(ret=="1"){
					freshDataTable();
				}else{
					Comm.alert("出现异常！请重新操作");
				}
			}
		});
	}
	return false;
});

//刷新数据源
$("#freshModel").click(function (){
	var _selects=new Array();
	var count = $(":checkbox[name='data']:checked").size();
	if(count<1){
		Comm.alert("请选择数据源！");
		return false;
	}
	$(":checkbox[name='data']:checked").each(function(){
		var value=$(this).val();
		_selects.push(value);
	});
	var pageId = $("#id").val();
	$.ajax({
		url:"fd/datadefine/freshModel.do",
		async : false,
		type:"POST",
		data:{_selects:_selects.join(","),pageId:pageId},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(errorThrown);
		},
		success:function(ret){
			$(":checkbox[name='data']").prop("checked",false);
			$("#checkAllDatasource").prop("checked",false);
			if(ret=="0"){
				Comm.alert("出现异常！请重新操作");
			}else{
				Comm.alert("刷新成功！");
				if(!empty(ret)){
					$.each(ret,function(idx,item){
						dataSource.put(item.id,item);
					});
					$("#modelJsonArray").val(dataSource.toJSON());
				}				
			}
		}
	});
	return false;
});


