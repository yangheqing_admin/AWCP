/**
 * 动作按钮操作js
 */
var actMap = new Map();
var actView = new Map();
actView.put("actType.2000", "动作");
actView.put("actType.2001", "保存");
actView.put("actType.2002", "返回");
actView.put("actType.2003", "删除");
actView.put("actType.2004", "导入");
actView.put("actType.2009", "新增");
actView.put("actType.2010", "更新");
actView.put("actType.3000", "流程办结");
actView.put("actType.3001", "流程撤销");
actView.put("actType.3002", "流程拒绝");
actView.put("actType.3003", "流程回撤");
actView.put("actType.3004", "流程退回");
actView.put("actType.3005", "流程发送");
actView.put("actType.3006", "流程跳转");
actView.put("actType.3007", "流程传阅");
actView.put("actType.3008", "加签");
actView.put("actType.3009", "流程保存");

function initPage(){
	try {
		var dialog = top.dialog.get(window);
	} catch (e) {
		return;
	}
	if (dialog) {
		$("#dynamicPageId").prop("disabled","disabled");
		$("#closeBtn").show();
		$("#closeBtn").on("click",function(){
			dialog.close();
            dialog.remove();
		});
	} else{
		$("#backBtn").show();
		$("#backBtn").on("click",function(){
			location.href = basePath + "fd/act/list.do";
		});
	}

	var $form = $("#groupForm");
	$form.bootstrapValidator({
		excluded: [":disabled"],
	    fields:{
	    	"order": {
                validators: {
                    notEmpty: {
                        message: "请输入按钮顺序"
                    },
                    regexp: {
                        regexp: /^[1-9]\d*$/,
                        message: '请输入正整数'
                    }
                }
            },
            "name": {
                validators: {
                	notEmpty: {
                        message: "请输入按钮名称"
                    }
                }
            }
	    }
	});
	
	//保存
	$("#saveBtn").click(function() {
		if(!validateForm()){
			return false;
		}
		if (dialog) {
            $("#dynamicPageId").removeAttr("disabled");
		}
		var data = $("#groupForm").serializeJSON();
        data.dynamicPageName = $("#dynamicPageId option:selected").text();
        var ret = myFun.ajaxExecute("fd/act/saveByAjax.do",data);
        if (ret.hasOwnProperty("status")) {
            Comm.alert(ret.message,function(){
            	if (ret.status == -4) {
                    location.href = basePath + "login.html";
                }
            });
        } else if (ret.hasOwnProperty("pageId")) {
        	if(dialog){
        		var json = eval(ret);
                dialog.close(json);
                dialog.remove(); 
        	} else{
        		Comm.alert("保存成功",function(){
        			location.href = basePath + "fd/act/list.do";
        		})
        	}
        } 
        return false;
	});
	
	//按钮样式设置
	var $btnSet = $("#js-btn-set"),					
		$btnSetIcon = $btnSet.find("#btnIcon"),
		$btnSetColor = $btnSet.find("#btnColor");	
	$btnSet.find(".icon-menu a").click(function(){//修改按钮的图标
		var icon = $(this).attr("class");
		$btnSetIcon.find("i").attr("class",icon);
		$btnSetIcon.find("input").val(icon);
	});	
	$btnSet.find(".color-menu span").click(function(){//修改按钮的颜色
		var common = "label label-dot";
		var color = $(this).data("color");
		$btnSetColor.find("span").attr("class",common + " label-" + color);
		$btnSetColor.find("input").val(color);
	});	
	
	//显示按钮的类型
	var actType = $("#actType").val();
	var actName = actView.get("actType." + actType);
	$("#selectActType").html("<option value='" + actType + "'>" + actName + "</option>");
}

function addAct(type){
	act_add(type);
}

// 弹出新增页面动作页面
function act_add(type) {
	top.dialog({
		id : 'act-add-dialog' + Comm.uuid(),
		title : '新增动作',
		url : basePath + "fd/act/edit.do?dialog=1&dynamicPageId=" + $("#id").val() + "&order=3&type=" + type,
		height : 540,
		width : 1000,
		onclose : function() {
			if (this.returnValue) {
				var ret = this.returnValue;
				actMap.put(ret.pageId, ret);
				freshActTable();
			}
		}
	}).showModal();
	return false;
}

// 编辑页面动作信息
function editAct(id) {
	top.dialog({
		id : 'act-edit-dialog' + Comm.uuid(),
		title : '动作编辑',
		url : basePath + 'fd/act/edit.do?_selects=' + id,
		height : 540,
		width : 1000,
		onclose : function() {
			if (this.returnValue) {
				var ret = this.returnValue;
				actMap.put(ret.pageId, ret);
				freshActTable();
				Comm.alert("更新成功！");
			}
		}
	}).showModal();
	return false;
}

// ajax to delete act and fresh table data
$("#deleteAct").click(function() {
	var _selects = [];
	$(":checkbox[name='act']:checked").each(function() {
		var value = $(this).val();
		_selects.push(value);
	});
    if(_selects.length <1){
        Comm.alert("请至少选择一条数据");
        return false;
    }
	$.ajax({
		url : basePath + "fd/act/deleteByAjax.do",
		type : "POST",
		async : false,
		data : {
			_selects : _selects.join(",")
		},
		success : function(ret) {
			if ("1" == ret) {
				$(":checkbox[name='act']:checked").each(function() {
					var value = $(this).val();
					actMap.remove(value);
				});
				freshActTable();
				Comm.alert("删除成功！");
			} else {
				Comm.alert("删除失败！");
			}

		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(errorThrown);
		}
	});
	return false;
});

// 刷新table
function freshActTable() {// refreshActTable act table
	$("#actdatabody").empty();
	actMap.each(function(key, value, index) {
		var str = "<tr id='" + value.pageId + "'><td><input type='checkbox' name='act' value='" + value.pageId + "'/></td>";
		str += "<td><a href='javascript:void(0);' onclick='editAct(\"" + value.pageId + "\")'>" + value.name + "</a></td>";
		str += "<td>" + actView.get("actType." + value.actType) + "</td>";
		str += "<td>" + value.order + "</td>";
		str += "</tr>";
		$("#actdatabody").append(str);
	});
}

// 全选
$("#checkAllAct").click(function() {
	if ($(this).prop("checked")) {
		$(":checkbox[name='act']").prop("checked", true);
	} else {
		$(":checkbox[name='act']").prop("checked", false);
	}
});

function initActMap() {
	var pageId = $("#id").val();
	if (!empty(pageId)) {
		$.ajax({
			url : basePath + "fd/act/getByPageId.do?dynamicPageId=" + pageId,
			type : "GET",
			async : false,
			success : function(ret) {
				var json = eval(ret);
				$.each(json, function(idx, item) {
					actMap.put(item.pageId, item);
				});
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(errorThrown);
			}
		});
	}
	return false;
}

function initAct() {
	initActMap();
	freshActTable();
}

$("#ajaxExcute").click(function(){
	var str = '';
	str+='var actId = $(this).attr("id");\n'		
	str+='$("#actId").val(actId.substring(actId.length-36));			//对应按钮ID\n'
	str+='$.ajax({\n'
	str+='	   type: "POST",\n'
	str+='	   url: basePath + "document/excuteOnly.do",	//调用后台.do\n'
		str+='	   data:$("#groupForm").serialize(),			//将整个表单参数传至后台，可自己定义传回去的参数\n'
			str+='	   async : false,\n'
				str+='	   success: function(data){\n'
					str+='		if(data=="1"){							//返回1表示执行成功，这里对应服务器脚本内容\n'
						str+='		alert("执行成功");\n'
						str+='		//var dialog = top.dialog.get(window);	//如果是对应弹窗的窗口，要求执行成功关闭窗口，则执行此处三行代码\n'
							str+='	//dialog.close(1);\n'
						str+='		//dialog.remove();\n'
						str+='	}else{\n'
							str+='	alert(data);			//不成功，则弹窗提示\n'					
					str+='		}\n'
				 str+='	   }\n'
				str+='	});\n'
	
	if($(this).prop("checked")){
		var clientScript = $("#clientScript").val();
		if(clientScript==''){
			$("#clientScript").val(str);
		}
	}else{
	
	}

});
	
$("#ajaxView").click(function(){
	var str = '';
	str+='var basePath = "${basePath}";\n'
	str+='var _selects = "";\n'
		str+='$("input[name=\'_selects\']").each(function(){\n'
			str+='_selects = _selects+$(this).val()+",";\n'
				str+='});\n'
					str+='if(_selects == null || _selects == ""){\n'
						str+=' alert("请先选择数据");\n'
						str+='return false;\n'
							str+='}\n'
								str+='var data = $("#createForm").serialize();\n'
									str+='data._selects = _selects;\n'
										str+='top.dialog({\n'
											str+='id : "edit-dialog" + Math.ceil(Math.random() * 10000),\n'
											str+='title : "载入中...",\n'
											str+='url : basePath + "document/view.do?dynamicPageId=1",	//弹出页面链接\n'
												str+='data : data,\n'
													str+='width : 600,\n'
														str+='onclose : function() {\n'
															str+='	if (this.returnValue) {					//弹框关闭后传回的值\n'
																str+='		var ret = this.returnValue;\n'
																	str+='	$("#groupForm").submit();			//刷新页面\n'
																		str+='	}\n'
																			str+='}\n'

																				str+='}).showModal();\n'
																					str+='return false;\n'
	
	if($(this).prop("checked")){
		var clientScript = $("#clientScript").val();
		if(clientScript==''){
			$("#clientScript").val(str);
		}
	}else{
	
	}
});