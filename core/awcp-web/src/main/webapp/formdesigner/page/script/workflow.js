$(function(){
	var pageId = $("#id").val();
	if(pageId){
		$.ajax({
			type:"post",
			url:basePath + "fd/workflow/getBindNodeList.do?pageId=" + pageId,
			async:false,
			dataType:"json",
			success:function(data){
				$("#noteList").empty();
				var html = "";
				$.each(data,function(index,item){
					html += "<tr data-id='" + item.id + "_" + item.workflowId + "'>" + 
							"<td></td>" + 
							"<td>" + item.name + "</td>" +
							"<td>" + item.workflowName + "</td></tr>";
				});
				$("#noteList").append(html);
				$("#nodetable").bootstrapTable({});
			}
		})
	}
	
	$("#addWorkflowBtn").click(function(){	
		//动态页面id
		var pageId = $("#id").val();
		if(pageId){
			top.dialog({ 	
				id: 'add-workflow-node-list-dialog' + Math.ceil(Math.random()*10000),
				title: '新增结点',
				url:"fd/workflow/notBindNodeList.do?pageId="+pageId,
				width:780,
				onclose: function () {
					if(this.returnValue){
						if(this.returnValue.result == '1'){
							reCreateTable(this.returnValue.msg);
						} else {
							alert(this.returnValue.msg);
						}
					}
				}
			}).showModal();
		} else {
			Comm.alert("请打开动态页面");
		}
		return false;
	});

	//delete
	$("#deleteWorkflowBtn").click(function(){
		var count = $("#nodetable").find("tr.selected").length;
		if(count<1){
			Comm.alert("请至少选择一项操作");
			return false;
		}
		Comm.confirm("确定删除？",function(){
			var arr = [];
			$("#nodetable").find("tr.selected").each(function(){
				arr.push($(this).attr("data-id"));
			})
			if(arr.length > 0){
	     		$.ajax({
	 				type : "POST",
					url : "fd/workflow/delNode.do",
	 				data : {"nodeIds":arr.join(),"pageId":pageId},
	 				success : function(data) { 					
	 					if(data.result == '1'){
							reCreateTable(data.msg);
						} else {
							Comm.alert(data.msg);
						}
	 				}
	 			});
			}
		});
		return false;
	});
	
	//config
	$("#configWorkflowBtn").click(function(){
		var count = $("#nodetable").find("tr.selected").length;
		if(count != 1){
			Comm.alert("请选择一个节点操作");
			return false;
		}		
		var nodeIds = $("#nodetable").find("tr.selected").eq(0).attr("data-id");
		var pageId = $("#id").val();
		top.dialog({ 	
			id: 'config-workflow-node-vars-dialog' + Math.ceil(Math.random()*10000),
			title: '配置结点参数',
			url:"fd/workflow/nodeVariableEdit.do?nodeId="+nodeIds + "&pageId=" + pageId,
			width:780,
			data : {"dataSource" : $("#modelJsonArray").val()},//数据源信息带上
			onclose: function () {
				if(this.returnValue){
					if(this.returnValue.result == '1'){
						reCreateTable(this.returnValue.msg);
					} else {
						Comm.alert(this.returnValue.msg);
					}
				}
			}
		}).showModal();
		return false;
	});
    	
    //配置权限组页面
	$("#authorityWorkflowBtn").click(function(){
		var count = $("#nodetable").find("tr.selected").length;
		if(count != 1){
			alert("请选择一组操作");
			return false;
		}
		var nodeIds = $("#nodetable").find("tr.selected").eq(0).attr("data-id");
		var pageId = $("#id").val();
		var urlStr = "formdesigner/page/workflow/workflow-authority-add.jsp";
	    var postData = {};
	    postData.pageId=pageId;
	    postData.nodeIds=nodeIds;
		top.dialog({
			id: 'add-dialog' + Math.ceil(Math.random()*10000),
			title: '载入中...',
			url: urlStr,
			data:postData,
			onclose: function () {
				if (this.returnValue) {
					var ret= this.returnValue;
					Comm.alert("保存成功");
				}
			}
		}).showModal();
		return false;
    });	
});
	
/**
 * 动态创建列表
 * @param temp
 */
function reCreateTable(temp){
	var nodeHtm = "";
	if(temp){
		temp = JSON.parse(temp);
		for(var i = 0; i < temp.length; i++){
			nodeHtm += '<tr data-id="' + temp[i].id + '_' + temp[i].workflowId + '">' + 
					   '<td></td>' +
					   '<td>' + temp[i].name + '</td>' + 
					   '<td>' + temp[i].workflowName + '</td>' +
					   '</tr>';
		}
	} 
	$("#nodetable").bootstrapTable(("destroy"));
	$("#nodetable tbody").html(nodeHtm);
	$("#nodetable").bootstrapTable({});
}