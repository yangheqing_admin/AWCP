<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html >
<html>
<head>
	<title>页面绑定管理</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body id="main">
	<section class="content">
	    <div class="container-fluid">
	        <div class="opeBtnGrop">
	        	<button type="button" class="btn btn-success" id="addBtn"><i class="fa fa-plus"></i> 绑定</button>
	            <button type="button" class="btn btn-default" id="backBtn"><i class="fa fa-reply"></i> 返回</button>
	        </div>
			<div class="row">
	            <div class="col-md-12">
	                <div class="box box-info">
	                    <div class="box-body">
	                        <form method="post" id="dataList">
	                            <div class="row form-group" id="searchDiv">
	                                <div class="col-md-4">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">动态页面名称</span>
											<input name="name" class="form-control" id="name" type="text" value="${name}" />
	                                        <span class="input-group-btn">
						                      	<button type="button" class="btn btn-info btn-flat" id="searchBtn">搜索</button>
						                    </span>
	                                    </div>
	                                </div>
	                            </div>
	                            <table class="table table-hover" id="dataTable">
	                                <thead>
		                                <tr>
		                                    <th class="hidden"></th>
		                                    <th data-width="" data-field="" data-checkbox="true"></th>
											<th>名称</th>
											<th>类型</th>
											<th>描述</th>
											<th>创建时间</th>
		                                </tr>
	                                </thead>
	                                <tbody>
		                                <c:forEach items="${vos}" var="vo">
		                                    <tr>
		                                        <td class="hidden formData"><input type="hidden" value="${vo.id}"></td>
		                                        <td></td>
		                                        <td>${vo.name}</td>
												<td>
													<c:choose>
														<c:when test="${vo.pageType == 1001 }">普通页面</c:when>
														<c:when test="${vo.pageType == 1002 }">表单页面</c:when>
														<c:when test="${vo.pageType == 1003 }">列表页面</c:when>
														<c:otherwise></c:otherwise>
													</c:choose>
												</td>
												<td>${vo.description }</td>
												<td><fmt:formatDate value="${vo.created }" pattern="yyyy-MM-dd"/></td>
		                                    </tr>
		                                </c:forEach>
	                                </tbody>
	                            </table>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>				
	<%@ include file="/resources/include/common_lte_js.jsp"%>
	<script type="text/javascript">
		$(function(){
			var count = 0;//默认选择行数为0
            $(".table").bootstrapTable({
                sidePagination:"server",
                pagination:false,
                onClickRow:function(row,$element,field){
                    var $checkbox=$element.find(":checkbox").eq(0);
                    if($checkbox.get(0).checked){
                        $checkbox.get(0).checked=false;
                        $element.find("input[type='hidden']").removeAttr("name","id");
                    }else{
                        $checkbox.get(0).checked=true;
                        $element.find("input[type='hidden']").attr("name","id");
                    }
                    count = $("input[name='id']").length;
                },
                onCheck: function(row,$element){
                    $element.closest("tr").find("input[type='hidden']").attr("name","id");
                    count = $("input[name='id']").length;
                },
                onUncheck:function(row,$element){
                    $element.closest("tr").find("input[type='hidden']").removeAttr("name");
                    count = $("input[name='id']").length;
                },
                onCheckAll: function (rows) {
                    $.each(rows,function(i,e){
                        $("input[value='"+$($.trim(e["0"])).attr("value")+"']").attr("name","id");
                    });
                    count = $("input[name='id']").length;
                },
                onUncheckAll: function (rows) {
                    $.each(rows,function(i,e){
                        $("input[value='"+$($.trim(e["0"])).attr("value")+"']").removeAttr("name");
                    });
                    count = $("input[name='id']").length;
                }
            });
			
			//隐藏全选按钮，防止将隐藏的记录选中
			$("[name='btSelectAll']").hide();
			
		 	//绑定
			$("#addBtn").click(function(){
				if(count<1){
	    			Comm.alert("请至少选择一项进行操作");
	    			return false;
	    		}
	    		if(count>4){
	    			Comm.alert("最多只能选择4项进行绑定");
	    			return false;
	    		}
	    		Comm.confirm("确定绑定？",function(){
	    			var data = Comm.getData("common/user/binding.do",$("#dataList").serialize());
	    			if(data == "1"){
	    				Comm.alert("绑定成功", function(){
	    					location.href = basePath + "common/user/list-bind.do";
	    				});
	    			} else{
	    				Comm.alert(data.message);
	    			}
	    		});
				return false;
			});
		 	
		 	$("#searchBtn").on("click",function(){
		 		searchTable();
		 	});
		 	
		 	function searchTable(){
		 		var name = $("#name").val();
		 		$("#dataTable").children("tbody").find("tr").show();
		 		$("#dataTable").children("tbody").find("tr").each(function(){
		 			var count = $(this).find("input[name='id']").length;
		 			if(count==0){
		 				var pageName = $.trim($(this).children("td").eq(2).text());
		 				if(pageName.indexOf(name) == -1){
		 					$(this).hide();
		 				}
		 			}
		 		})
		 	}
		 	
		 	$("#name").on("keydown",function(e){
		 		var theEvent = e || window.event;    
				var code = theEvent.keyCode || theEvent.which || theEvent.charCode;    
				if (code == 13) {    
					searchTable();
					return false;
				} 
		 	});
		 	
		 	$("#backBtn").on("click",function(){
		 		location.href = basePath + "common/user/list-bind.do";
		 	});
   	 	});
	</script>
</body>
</html>


