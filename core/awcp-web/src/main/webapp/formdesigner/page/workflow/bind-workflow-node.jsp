<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>绑定流程页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body id="main">
	<section class="content">
		<div class="opeBtnGrop">
	    	<button class="btn btn-primary" id="addWorkFlowBtn"><i class="fa fa-save"></i> 保存</button>
		    <button class="btn btn-default" id="closeBtn"></i><i class="fa fa-close"></i> 关闭</button>
    	</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<div class="row" id="searchform">
							<form action="<%=basePath%>fd/workflow/notBindNodeList.do" id="createForm" class="clearfix" method="post">
								<div class="col-sm-4">
									<input type="hidden" id="pageId" name="pageId" value="${pageId}"/>
									<!-- 弹出框选择流程 -->
									<div class="input-group">
										<span class="input-group-addon">选择流程</span>
										<input name="workflowId" class="form-control" id="workflowId" type="hidden" value="${workflowId}"/>
										<input name="workflowName" readonly="readonly" class="form-control" id="workflowName" type="text" value="${workflowName}"/>
										<div class="input-group-btn"> <button class="btn btn-default" type="button" id="workflowSelect">选择</button> </div>
									</div>
								</div>
								<div class="col-sm-4 btn-group">
									<button class="btn btn-success" type="submit"><i class="fa fa-server"></i> 提交</button>
								</div>
							</form>
						</div>
						<form class="form-horizontal" id="groupForm">	
							<input type="hidden" id="pageId" name="pageId" value="${pageId}"/></th>
							<table class="table table-hover">
								<thead>
									<tr>
										<th class="hidden"></th>
							        	<th data-width="" data-field="" data-checkbox="true"></th>
										<th>结点名</th>
										<th>流程名</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${vos}" var="vo" varStatus="status">
										<tr>
											<td class="hidden formData">
												<input id="nodeIds" type="hidden" value="${vo.NODEID}"/>
												<input id="worknodeName" type="hidden" value="${vo.NAME}"/>
												<input id="workFlowId" type="hidden" value="${workflowId}"/>
												<input id="workflowName" type="hidden" value="${workflowName}"/>
												<input id="priority" type="hidden" value="${status.index + 1}"/>
											</td>
								            <td></td>
											<td>${vo.NAME}</td>
											<td>${workflowName}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<%@ include file="/resources/include/common_lte_js.jsp" %>		
	<script src="<%=basePath%>resources/scripts/jquery.serializejson.min.js"></script>
	<script src="<%=basePath%>resources/scripts/json2.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){ 
			$("#closeBtn").on("click",function(){
				top.dialog({id:window.name}).close();
			});
			
			$(".table").bootstrapTable({});
			
        	//点击，弹出流程列表页面
        	$("#workflowSelect").click(function(){
        		top.dialog({ 
					id: 'select-workflow-dialog' + Comm.uuid(),
					title: '选择流程',
					height:500,
					width:800,
					url: basePath + "fd/workflow/templateList.do",
					onclose : function() {
						if (this.returnValue) {
							$("#workflowName").val(this.returnValue.name);
							$("#workflowId").val(this.returnValue.id);
						}
					}
				}).width(800).height(500).show();
        	});
        	
       	 	var dialog;
    	 	try {
				dialog = top.dialog.get(window);
			} catch (e) {
				return;
			}			 
        	 //保存按钮
			$("#addWorkFlowBtn").click(function() {
				if($("tr.selected").length < 1){
					Comm.alert("请至少选择一个结点");
					return false;
				} else {
					selectTrOperate();
					$.ajax({
						type : "POST",
						url : basePath + "fd/workflow/relate.do",
						data : $("#groupForm").serialize(),
						success : function(data) {
							dialog.close(data);
							dialog.remove();
						}
					});
				}
				return false;
			});
			 
			function selectTrOperate(){
				$("#groupForm").find("tbody .formData").find("input").removeAttr("name");
				$("tr.selected").each(function(i){
					$(this).find("input#nodeIds").attr("name","nodes["+i+"].id");
					$(this).find("input#worknodeName").attr("name","nodes["+i+"].name");
					$(this).find("input#workFlowId").attr("name","nodes["+i+"].workflowId");
					$(this).find("input#workflowName").attr("name","nodes["+i+"].workflowName");
					$(this).find("input#priority").attr("name","nodes["+i+"].priority");
				})			
			};
		});
			
		</script>
	</body>
</html>
