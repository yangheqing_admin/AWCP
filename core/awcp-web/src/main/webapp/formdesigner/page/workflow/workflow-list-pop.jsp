<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>流程列表页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body id="main">
	<section class="content">
		<div class="opeBtnGrop">
	    	<button class="btn btn-primary" id="selectBtn"><i class="fa fa-save"></i> 保存</button>
		    <button class="btn btn-default" id="closeBtn"></i><i class="fa fa-close"></i> 关闭</button>
    	</div>
    	<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" id="groupForm">	
							<div class="row" style="margin-bottom:10px;">
								<input type="hidden" id="currentPage" name="currentPage" 
	                            	value="${vos.getPaginator().getPage()}" />
		                       	<input type="hidden" id="pageSize" name="pageSize" 
		                            	value="${vos.getPaginator().getLimit()}" />
		                        <input type="hidden" id="totalCount" name="totalCount" 
		                            	value="${vos.getPaginator().getTotalCount()}" />
								<div class="col-sm-5">
									<div class="input-group">
										<span class="input-group-addon">流程名</span>
										<input type="text" class="form-control" name="workflowName" value="${workflowName }"/>
									</div>
								</div>
								<div class="col-sm-5">
									<div class="input-group">
										<span class="input-group-addon">流程类型</span>
										<select name="type" class="form-control" id="type" value="${type }">
											<option value="0">请选择流程类型</option>
											<c:forEach items="${categories }" var="c">
												<option value="${c.NO }">${c.NAME }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-sm-2 btn-group">
									<button class="btn btn-info" id="searchBtn"><i class="fa fa-search"></i> 搜索</button>
								</div>
							</div>
							
							<table class="table table-hover">
								<thead>
									<tr>
										<th class="hidden"></th>
							        	<th data-width="" data-field="" data-checkbox="true"></th>
										<th>流程名称</th>
										<th>流程类型</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${vos}" var="vo" varStatus="status">
										<tr>
											<td class="hidden formData">
												<input id="templateID" type="hidden" value="${vo.NO}">
												<input id="templateName" type="hidden" value="${vo.NAME}">
											</td>
											<td></td>
											<td>${vo.NAME}</td>
											<td>${vo.FLOWSORT}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<%@ include file="/resources/include/common_lte_js.jsp" %>		
	<script type="text/javascript">
		$(document).ready(function(){
			$("#type option[value='" + $("#type").attr("value") + "']").attr("selected",true);
			
			$("#closeBtn").on("click",function(){
				top.dialog({id:window.name}).close();
			});
			
			$(".table").bootstrapTable({
				pageSize:parseInt($("#pageSize").val()),
				pageNumber:parseInt($("#currentPage").val()),
				totalRows:parseInt($("#totalCount").val()),
				sidePagination:"server",
				pagination:true,
				onPageChange:function(number, size){
					$("#pageSize").val(size);
					$("#currentPage").val(number);
					$("#groupForm").submit();
				}
			});
			
			$("#searchBtn").on("click",function(){
				$("#groupForm").submit();
			});
			
			var dialog;
			try {
				dialog = top.dialog.get(window);
				var data = dialog.data; // 获取对话框传递过来的数据
			} catch (e) {
				alert(e);
				return;
			}
			
			//保存
			$("#selectBtn").click(function(){
				var count = $("tr.selected").length;
				if(count == 1){
					var id = $("tr.selected").find("#templateID").val();
					var name = $("tr.selected").find("#templateName").val();	
					dialog.close({'id':id,'name':name}); // 关闭（隐藏）对话框
					dialog.remove();
				}else{
					Comm.alert("请选择一个流程进行操作");
				}
				return false;
			});
		});
		</script>
	</body>
</html>
