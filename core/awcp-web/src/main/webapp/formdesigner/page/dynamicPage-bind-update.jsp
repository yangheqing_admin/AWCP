<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<title>页面绑定管理</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body id="main">
	<section class="content">
	    <div class="container-fluid">
	        <div class="opeBtnGrop">
	        	<button type="button" class="btn btn-primary" id="save"><i class="fa fa-save"></i> 保存</button>
	            <button type="button" class="btn btn-default" id="closeBtn"><i class="fa fa-close"></i> 关闭</button>
	        </div>
	   </div>
	   <div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" id="groupForm">	
							<input type="hidden" id="pageId" />	
							<input type="hidden" id="PC" />
							<input type="hidden" id="PC_LIST" />	
							<input type="hidden" id="APP" />	
							<input type="hidden" id="APP_LIST" />					
							<div class="form-group">
								<div class="customGroup">	
									<div class="col-md-4">
										<label class="control-label">PAGEID_PC</label>
										<input id="PC_Name" type="text" class="form-control" readonly="readonly" />
									</div>
								</div>
								<div class="customGroup">	
									<div class="col-md-4">
										<label class="control-label">PAGEID_LIST</label>
										<input id="PC_LIST_Name" type="text" class="form-control" readonly="readonly" />
									</div>
								</div>
								<div class="customGroup">	
									<div class="col-md-4">
										<label class="control-label">PAGEID_APP</label>
										<input id="APP_Name" type="text" class="form-control" readonly="readonly" />
									</div>
								</div>
								<div class="customGroup">	
									<div class="col-md-4">
										<label class="control-label">PAGEID_APP_LIST</label>
										<input id="APP_LIST_Name" type="text" class="form-control" readonly="readonly" />
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<%@ include file="/resources/include/common_lte_js.jsp"%>
	<script type="text/javascript">	
		$('document').ready(function() {
			try {
				var dialog = top.dialog.get(window);
			} catch (e) {
				return;
			}
		
			//判断是否有数据，初始化 所属模型
			var data = null;
			if(dialog != null){
				data = dialog.data;
				if(data.id){
					$("#pageId").val(data.id);
				}
			}	
		
			//查询数据
			$.ajax({
		   		type: "POST",
		   		url: basePath + "common/user/get.do",
		   		data: {
		   			id:$("#pageId").val()
		   		},
		   		success: function(data){
					if(data != null){	
			     		$("#PC").val(data.PAGEID_PC);
			     		$("#PC_LIST").val(data.PAGEID_PC_LIST);
				 		$("#APP").val(data.PAGEID_APP);
				 		$("#APP_LIST").val(data.PAGEID_APP_LIST);
				 		$("#PC_Name").val(data.PAGEID_PC_Name);
			     		$("#PC_LIST_Name").val(data.PAGEID_PC_LIST_Name);
				 		$("#APP_Name").val(data.PAGEID_APP_Name);
				 		$("#APP_LIST_Name").val(data.PAGEID_APP_LIST_Name);
		    		}
		  		}
			});	
		
			$("#groupForm").find("input[type='text']").on("click",function(){
				var $text = $(this);
				top.dialog({
					id: 'add-dialog' + Comm.uuid(),
					title: '页面绑定选中页面',
					url: basePath + "fd/listSelect.do",
					skin:"col-md-8",
					onclose: function () {
						var ret = this.returnValue;
						if (ret) {
							$text.val(ret.name);
							var id = $text.attr("id");
							id = id.substring(0,id.length-5);
							$("#" + id).val(ret.id);
						}
					}
				}).showModal();
			});
			
			$("#save").click(function(){
				var pc = $("#PC").val();
				var pc_list = $("#PC_LIST").val();
				var app = $("#APP").val();
				var app_list = $("#APP_LIST").val();
				var id = $("#pageId").val();
				$.ajax({
				   	type: "POST",
				   	url: basePath + "common/user/update.do",
				   	data:{
				   		id:id,
				   		pc:pc,
				   		pc_list:pc_list,
				   		app:app,
				   		app_list:app_list
				   	},
				   	success: function(data){
						if(data.status == 0){	
							if(dialog != null){
								dialog.close(data);
								dialog.remove();
							}
						}else{
							Comm.alert(data.message);
						}
				   	}
				})	
			});

			$("#closeBtn").on("click",function(){
				if(dialog != null){
					dialog.close();
					dialog.remove();
				}
			})
		});
	</script>
</body>
</html>
