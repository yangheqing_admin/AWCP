<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="renderer" content="webkit">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>新增按钮编辑页面</title>
	<base href="<%=basePath%>">
	<%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body id="main">
	<section class="content">
	    <div class="container-fluid">
			<div class="row">
				<div class="opeBtnGrop">
					<button type="button" class="btn btn-success" id="saveBtn"><i class="fa fa-save"></i> 保存</button>
					<button type="button" class="btn btn-default" id="backBtn" style="display: none;">
						<i class="fa fa-reply"></i> 返回
					</button>
					<button type="button" class="btn btn-default" id="closeBtn" style="display: none;">
						<i class="fa fa-close"></i> 关闭
					</button>
				</div>
				<div class="row">
					<div class="col-md-12">
					    <div class="box box-info">
					        <div class="box-body">
								<form class="form-horizontal" id="groupForm" action="<%=basePath%>fd/act/save.do" method="post">
									<div class="form-group ">
										<div class='col-md-4'>
											<label class="control-label">目标页面</label>
											<select class=" form-control" tabindex="2" name="extbute[target]" id="target">
												<c:forEach items="${pages}" var="page">
													<c:choose>
														<c:when test="${act.extbute['target'] ==  page.id}">
															<option value="${page.id}" selected="selected">${page.name}</option>
														</c:when>
														<c:otherwise>
															<option value="${page.id}">${page.name}</option>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</select>
										</div>
									</div>
									<%@ include file="comms/basicInfo.jsp"%>						
									<%@ include file="comms/scripts.jsp"%>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script src="<%=basePath%>resources/scripts/map.js"></script>
	<script src="<%=basePath%>resources/scripts/jquery.serializejson.min.js"></script>
	<script src="<%=basePath%>formdesigner/page/script/act.js"></script>
	<script type="text/javascript">	
		$(document).ready(function() {
			$('#target').select2();
			initPage();
		});
	</script>
</body>
</html>