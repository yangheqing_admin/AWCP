<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()	+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="renderer" content="webkit">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>动作列表</title>
	<base href="<%=basePath%>">
	<%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body id="main">
	<section class="content">
	    <div class="container-fluid">
	        <div class="opeBtnGrop">
				<div class="btn-group">
					<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-plus"></i> 新增 <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="javascript:void(0)" onclick="addAct('2000');">普通</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('2001');">保存</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('2002');">返回</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('2003');">删除</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('2004');">导入</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('2009');">新增</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('2010');">更新</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('3000');">流程办结</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('3001');">流程撤销</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('3002');">流程拒绝</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('3003');">流程回撤</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('3004');">流程退回</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('3005');">流程发送</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('3006');">流程跳转</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('3007');">流程传阅</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('3008');">加签</a></li>
						<li><a href="javascript:void(0)" onclick="addAct('3009');">流程保存</a></li>
					</ul>
				</div>
				<button type="button" class="btn btn-success" id="syncBtn"><i class="fa fa-refresh"></i> 同步到资源库</button>
				<button type="button" class="btn btn-danger" id="deleteBtn"><i class="fa fa-trash"></i> 删除</button>
	            <button type="button" class="btn btn-default" id="closeBtn"><i class="fa fa-close"></i> 关闭</button>
	        </div>	
	        <div class="row">
	            <div class="col-md-12">
	                <div class="box box-info">
	                    <div class="box-body">
	                        <form method="post" id="dataList">
	                            <input type="hidden" id="currentPage" name="currentPage" 
	                            	value="${vos.getPaginator().getPage()}" />
	                            <input type="hidden" id="pageSize" name="pageSize" 
	                            	value="${vos.getPaginator().getLimit()}" />
	                            <input type="hidden" id="totalCount" name="totalCount" 
	                            	value="${vos.getPaginator().getTotalCount()}" />
	                            <div class="row form-group" id="searchDiv">
				                   	<div class="col-md-3">
										<div class="input-group">
											<span class="input-group-addon">动作名称</span>
											<input name="name" class="form-control" id="name" type="text" value="${name }"/>
										</div>
									</div>
									<div class="col-md-3">
										<div class="input-group">
											<span class="input-group-addon">动作描述</span>
											<input name="actDes" class="form-control" id="actDes" type="text" value="${actDes }"/>
										</div>
									</div>
									<div class="col-md-3 btn-group">
										<button type="button" class="btn btn-info" id="searchBtn">
											<i class="fa fa-search"></i> 搜索
										</button>
									</div>
	                            </div>
	                            <table class="table table-hover">
	                                <thead>
		                                <tr>
		                                    <th class="hidden"></th>
		                                    <th data-width="" data-field="" data-checkbox="true"></th>
											<th>名称</th>
											<th>类型</th>
											<th>描述</th>
											<th>所属动态页面</th>
		                                </tr>
	                                </thead>
	                                <tbody>
		                                <c:forEach items="${vos}" var="vo">
		                                    <tr>
		                                        <td class="hidden formData"><input type="hidden" value="${vo.pageId}"></td>
		                                        <td></td>
		                                        <td><a href="<%=basePath%>/fd/act/edit.do?_selects=${vo.pageId}">${vo.name}</a></td>
												<td><span class="text-ellipsis">${actTypes[vo.actType] }</span></td>
												<td><span class="text-ellipsis">${vo.description }</span></td>
												<td><span class="text-ellipsis">${vo.dynamicPageName }</span></td>
		                                    </tr>
		                                </c:forEach>
	                                </tbody>
	                            </table>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>			
	<%@ include file="/resources/include/common_lte_js.jsp"%>
    <script type="text/javascript">
        $(function () {
            var count = 0;//默认选择行数为0
            $(".table").bootstrapTable({
                pageSize:parseInt($("#pageSize").val()),
                pageNumber:parseInt($("#currentPage").val()),
                totalRows:parseInt($("#totalCount").val()),
                sidePagination:"server",
                pagination:true,
                onPageChange:function(number, size){
                    $("#pageSize").val(size);
                    $("#currentPage").val(number);
                    $("#dataList").submit();
                },
                onClickRow:function(row,$element,field){
                    var $checkbox=$element.find(":checkbox").eq(0);
                    if($checkbox.get(0).checked){
                        $checkbox.get(0).checked=false;
                        $element.find("input[type='hidden']").removeAttr("name","id");
                    }else{
                        $checkbox.get(0).checked=true;
                        $element.find("input[type='hidden']").attr("name","id");
                    }
                    count = $("input[name='id']").length;
                },
                onCheck: function(row,$element){
                    $element.closest("tr").find("input[type='hidden']").attr("name","id");
                    count = $("input[name='id']").length;
                },
                onUncheck:function(row,$element){
                    $element.closest("tr").find("input[type='hidden']").removeAttr("name");
                    count = $("input[name='id']").length;
                },
                onCheckAll: function (rows) {
                    $.each(rows,function(i,e){
                        $("input[value='"+$($.trim(e["0"])).attr("value")+"']").attr("name","id");
                    });
                    count = $("input[name='id']").length;
                },
                onUncheckAll: function (rows) {
                    $.each(rows,function(i,e){
                        $("input[value='"+$($.trim(e["0"])).attr("value")+"']").removeAttr("name");
                    });
                    count = $("input[name='id']").length;
                }
            });
    	
            //搜索
            $("#searchBtn").click(function(){
                $("#dataList").submit();
            });
            	
            //关闭
	    	$("#closeBtn").on("click",function(){
	    		closePage();
	    	})
	    	
	    	//删除
	    	$("#deleteBtn").click(function(){
	    		if(count<1){
	    			Comm.alert("请至少选择一项进行操作");
	    			return false;
	    		}
	    		Comm.confirm("确定删除？",function(){
	    			$("#dataList").attr("action","<%=basePath%>fd/act/delete.do").submit();
	    		})
				return false;
	    	});
	    	
	    	//同步资源库
	    	$("#syncBtn").click(function(){
	    		$.get(basePath + "fd/act/syncResource.do",function(data){
	                if(data.status==0){
	                    Comm.alert("同步成功");
					}else{
	                    Comm.alert(data.message);
					}
				})
	    	});
    	});
        
        function addAct(type){
    		location.href = basePath + "fd/act/edit.do?type=" + type;
    		return false;
    	}
	</script>
</body>
</html>


