<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-12 col-md-12">
			<label class="control-label">隐藏脚本(服务器脚本)</label>	
			<textarea name='hiddenScript' id='hiddenScript' rows='4' class='form-control' style="resize: vertical;"></textarea>
		</div>
	</div>
</div>
<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-12 col-md-12">
			<label class="control-label">禁用脚本(服务器脚本)</label>	
			<textarea name='disabledScript' id='disabledScript' rows='4' class='form-control' style="resize: vertical;"></textarea>
		</div>
	</div>
</div>
<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-12 col-md-12">
			<label class="control-label">只读脚本(服务器脚本)</label>	
			<textarea name='readonlyScript' id='readonlyScript' rows='4' class='form-control' style="resize: vertical;"></textarea>
		</div>
	</div>
</div>
<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-12 col-md-12">
			<label class="control-label">option选项脚本(服务器脚本)</label>	
			<textarea name='optionScript' id='optionScript' rows='4' class='form-control' style="resize: vertical;"></textarea>
		</div>
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-12 col-md-12">
			<label class="control-label">onchange事件(客户端脚本)</label>	
			<textarea name='onchangeScript' id='onchangeScript' rows='4' class='form-control' style="resize: vertical;"></textarea>
		</div>
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-12 col-md-12">
			<label class="control-label">默认值脚本(服务器脚本)</label>	
			<textarea name='defaultValueScript' id='defaultValueScript' rows='4' class='form-control' style="resize: vertical;"></textarea>
		</div>
	</div>
</div>