<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ page import="cn.org.awcp.venson.util.PlatfromProp" %>
<%@ page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>图片上传框组件编辑页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body id="main">
	<div class="content" style="background-color: white;">
		<div class="opeBtnGrop">
			<button type="button" class="btn btn-primary" id="saveComponent"><i class="fa fa-save"></i> 保存</button>
			<button type="button" class="btn btn-default" id="cancelComponent"><i class="fa fa-remove"></i> 关闭</button>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div id="searchTab" class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab1" data-toggle="tab">基本</a></li>
							<li class=""><a href="#tab2" data-toggle="tab">校验</a></li>
							<li class=""><a href="#tab3" data-toggle="tab">状态</a></li>
						</ul>
					</div>
					<form id="componentForm" class="form-horizontal">
						<div class="tab-content" style="margin:0px 15px 100px 15px;padding-bottom:15px;min-height:500px;">
							<div class="tab-pane active" id="tab1">
								<%@ include file="/formdesigner/page/component/image/basicInfo.jsp" %>
							</div>
							<div class="tab-pane" id="tab2">
								<%@ include file="/formdesigner/page/component/common/validators.jsp" %>
							</div>
							<div class="tab-pane" id="tab3">
								<%@ include file="/formdesigner/page/component/common/hidden-disabled-readonly-defaultvalue.jsp" %>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>	
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script src="<%=basePath%>resources/scripts/jquery.serializejson.min.js"></script>
	<script src="<%=basePath%>/formdesigner/scripts/form.cpcommons.js"></script>
	<script type="text/javascript">	
		var basePath = "<%=basePath%>";
		$('document').ready(function(){
			initFormValidator();
			
			$("#componentForm").bootstrapValidator('addField', 'dataItemCode',{validators:{
				notEmpty: {
                    message: "数据源不能为空"
                }
			}});
			
			$("#uploadType").val('<%=PlatfromProp.getValue("default_upload_type")%>')
			$("#uploadType").on("change",function(){
				if(this.value==1){
					$("#filePath").attr("disabled",false);
				}else{
					$("#filePath").attr("disabled",true);
				}
			})
			initializeDocument("${componentType}","${_ComponentTypeName[componentType]}");
			if($("#uploadType").val()==1){
				$("#filePath").attr("disabled",false);
			}else{
				$("#filePath").attr("disabled",true);
			}
		});
	</script>
</body>
</html>