<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="renderer" content="webkit">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>批量修改数据源别名</title>
	<base href="<%=basePath%>">
	<%@ include file="/resources/include/common_lte_css.jsp" %>
	<link rel="stylesheet" type="text/css" href="<%=basePath%>resources/plugins/zTree_v3/css/zTreeStyle/zTreeStyle.css">
</head>
<body id="main">	
	<section class="content">
		<div class="opeBtnGrop">
	    	<button class="btn btn-primary" id="quickModifyAlias"><i class="fa fa-save"></i> 保存</button>
		    <button class="btn btn-default" id="cancelComponent"></i><i class="fa fa-reply"></i> 取消</button>
    	</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" id="groupForm">
							<input type="hidden" name="dynamicPageId" id="dynamicPageId"/>
							<div class="form-group"><!-- 显示1列数据 -->
								<div class="customGroup">
									<div class="col-md-4">
										<label class="control-label required">数据源别名</label>
										<input name="alias" class="form-control" id="alias" type="text" />
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script type="text/javascript">	
		$(function(){
			var data = null;
			var _selects = null;
			try {
				var dialog = top.dialog.get(window);
				dialog.title("批量修改数据源名称");
				dialog.width(300);
				dialog.height(400); 
				dialog.reset();
				data = dialog.data;
				if(data != null && data._selects){
					_selects = data._selects;
				}
				if(data.dynamicPageId && $("#dynamicPageId")){
					$("#dynamicPageId").val(data.dynamicPageId);
				}
			} catch (e) {
				return;
			}
			
			var $form = $("#groupForm");
        	$form.bootstrapValidator({
        		excluded: [":disabled"],
        	    fields:{
                    "alias": {
                        validators: {
                        	notEmpty: {
                                message: "数据源别名不能为空"
                            }
                        }
                    }
        	    }
        	});
	
			$("#cancelComponent").click(function(){
				if(dialog != null){
					dialog.close();
					dialog.remove();
				}
			});
			
			$("#quickModifyAlias").click(function(){
				if(!validateForm()){
	    			return false;
	    		}
				$.ajax({
					type: "POST",
					url: basePath + "component/batchModifyDataAlias.do",
					data: {
						alias:$("#alias").val(),
						_selects:_selects,
						dynamicPageId:$("#dynamicPageId").val()
					},
					async : false,
					success: function(data){
						if("1" == data){	
							if(dialog != null){
								dialog.close(data);
								dialog.remove();
							}
						}else{
							alert("修改失败");
						}
					}
				});			
				return false;
			});
		});
	</script>
</body>
</html>


