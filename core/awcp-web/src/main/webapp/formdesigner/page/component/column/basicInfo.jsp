<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<input type="hidden" name="dynamicPageId" id="dynamicPageId"/>
<input type="hidden" name="pageId" id="pageId"/>
<input type="hidden" name="componentType" id="componentType">	
<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label">名称</label>
			<input class="form-control" name="name" id="name" type="text" />
		</div>	
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label required">列头名称</label>
			<input type="text" class="form-control" name="columnName" id="columnName" />
		</div>	
	</div>
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label">列宽</label>
			<input type="text" class="form-control" name="width" id="width" value="">
		</div>	
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label required">数据源</label>
			<div class="input-group" id="selectInput">
	            <input type="text" class="form-control sI_input" name="dataItemCode" id="dataItemCode">
	            <div class="input-group-btn">
	              	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
	                	<span class="caret"></span>
	              	</button>
	              	<ul class="dropdown-menu pull-right sI_select"></ul>
	            </div>
	         </div>
		</div>	
	</div>
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label required">序号</label>
			<input type="text" class="form-control" name="order" id="order" value="">
		</div>	
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-3 col-md-3">
			<label class="control-label">允许排序</label>
			<select name="alloworderby" class="form-control" id="alloworderby">
				<option value="1">是</option>
				<option value="-1" selected="selected">否</option>
			</select>
		</div>	
	</div>
	<div class="customGroup">
		<div class="col-sm-3 col-md-3">
			<label class="control-label">排序类型</label>
			<select name="orderby" class="form-control" id="orderby">
				<option value="asc" selected="selected">升序</option>
				<option value="desc">降序</option>
			</select>
		</div>	
	</div>
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label">排序字段名称</label>
			<input type="text" class="form-control" name="sortName" id="sortName" />
		</div>	
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label">格式化类型</label>
			<select name="columnFormat" class="form-control" id="columnFormat">
				<option selected="selected" value="">无</option>
				<option value="1">日期</option>
				<option value="2">数字</option>
				<option value="3">字符串</option>
				<option value="4">图片</option>
			</select>
		</div>	
	</div>
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label">格式</label>
			<select name="columnPatten" class="form-control" id="columnPatten">
				<option selected="selected" value="yyyy-MM-dd">yyyy-MM-dd</option>
				<option value="yyyy-MM-dd HH:mm">yyyy-MM-dd HH:mm</option>
				<option value="yyyy-MM-dd HH:mm:ss">yyyy-MM-dd HH:mm:ss</option>
				<option value="yyyy年MM月dd日">yyyy年MM月dd日</option>
				<option value="0.00">保留小数两位</option>
				<option value="0.0">保留小数1位</option>
			</select>
		</div>	
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-md-12 col-sm-12">
			<label class="control-label">描述(title)</label>	
			<textarea class="form-control" name='description' id='description' rows='2' style="resize: vertical;"></textarea>
		</div>
	</div>
</div>

