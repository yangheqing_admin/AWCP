<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>标签组件编辑页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body>
	<div class="content" style="background-color: white;">
		<div class="opeBtnGrop">
			<button type="button" class="btn btn-primary" id="saveComponent"><i class="fa fa-save"></i> 保存</button>
			<button type="button" class="btn btn-default" id="cancelComponent"><i class="fa fa-remove"></i> 关闭</button>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div id="searchTab" class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab1" data-toggle="tab">基本</a></li>
							<li class=""><a href="#tab3" data-toggle="tab">状态</a></li>
						</ul>
					</div>
					<form id="componentForm" class="form-horizontal">
						<div class="tab-content" style="margin:0px 15px 100px 15px;padding-bottom:15px;">
							<div class="tab-pane active" id="tab1">
								<%@ include file="/formdesigner/page/component/label/basicInfo.jsp" %>
							</div>
							<div class="tab-pane " id="tab3">
								<%@ include file="/formdesigner/page/component/common/hidden-disabled-readonly-defaultvalue.jsp" %>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script src="<%=basePath%>resources/scripts/jquery.serializejson.min.js"></script>
	<script src="<%=basePath%>/formdesigner/scripts/form.cpcommons.js"></script>
	<script type="text/javascript">	
		var basePath = "<%=basePath%>";		
		$('document').ready(function(){
			initFormValidator();
			
			$("#componentForm").bootstrapValidator('addField', 'title',{validators:{
				notEmpty: {
                    message: "显示文本不能为空"
                }
			}});
			
			initializeDocument("${componentType}","${_ComponentTypeName[componentType]}");
			
			$("#disabledScript,#readonlyScript,#defaultValueScript").parent().parent().hide();
		});
	</script>
</body>
</html>
