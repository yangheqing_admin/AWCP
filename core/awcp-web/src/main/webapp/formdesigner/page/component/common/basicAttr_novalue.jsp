<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="form-horizontal">
	<input type="hidden" name="dynamicPageId" id="dynamicPageId"/>
	<input type="hidden" name="pageId" id="pageId"/>
	<input type="hidden" name="componentType" id="componentType">
	
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-12">
				<label class="control-label required">名称</label>
				<div class="input-group">					
					<input class="form-control" name="name" id="name" type="text" />
					<div class="input-group-btn">
		             	<button class="btn btn-default" type="button" id="editName">编辑名称</button>
				 	</div>
				</div>
			</div>	
		</div>
	</div>
	
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label">文本(label-zh)</label>
				<input name="title" class="form-control" id="title" type="text" value="">
			</div>
		</div>
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label">文本(label-en)</label>		
				<input name="enTitle" class="form-control" id="enTitle" type="text" value="">
			</div>
		</div>	
	</div>
	
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label required">布局</label>
				<div class="input-group">
					<input type="text" readonly="readonly" class="form-control" name="layoutName" id="layoutName">
					<div class="input-group-btn"> <button class="btn btn-default" type="button" id="layoutSelect">选择</button></div>
					<input type="hidden" readonly="readonly" class="form-control" name="layoutId" id="layoutId">
				</div>
			</div>
		</div>
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label required">序号</label>
				<input name="order" class="form-control" id="order" type="text" value="">
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-12 col-sm-12">
				<label class="control-label">自定义样式(style)</label>
				<input class="form-control" name='style' id='style' />
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-12 col-sm-12">
				<label class="control-label">描述(title)</label>	
				<textarea class="form-control" name='description' id='description' rows='2' style="resize: vertical;"></textarea>
			</div>
		</div>
	</div>
</div>