<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="text-right" style="margin-bottom:6px;">
	<button type="button" class="btn btn-info btn-sm" id="addColumn"><i class="fa fa-plus"></i> 新增</button>
</div>
<div class="columnsTable" contenteditable="false" >
	<table class="table table-bordered" id="vt" align="right">
		<thead>
			<tr>
				<th width="12%" class="required" >名称</th>
				<th width="22%" class="required">数据源</th>
				<th width="10%">宽度</th>
				<th width="10%" class="required">序号</th>
				<th width="13%">类型</th>
				<th width="18%">类型值</th>
				<th width="10%">删除</th>
			</tr>
		</thead>
		<tbody id="columnsBody"></tbody>
	</table>
</div>