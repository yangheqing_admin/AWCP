<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>多行输入框组件编辑页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp" %>
	<style>
		td{
			padding:0px!important;
		}
		tbody input{
			padding:2px!important;
		}
	</style>
</head>
<body id="main">
	<div class="content" style="background-color: white;">
		<div class="opeBtnGrop">
			<button type="button" class="btn btn-primary" id="saveComponent"><i class="fa fa-save"></i> 保存</button>
			<button type="button" class="btn btn-default" id="cancelComponent"><i class="fa fa-remove"></i> 关闭</button>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div id="searchTab" class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab1" data-toggle="tab">基本</a></li>
							<li class=""><a href="#tab2" data-toggle="tab">列</a></li>
							<li class=""><a href="#tab3" data-toggle="tab">状态</a></li>
						</ul>
					</div>
					<form id="componentForm" class="form-horizontal">
						<div class="tab-content" style="margin:0px 15px 100px 15px;padding-bottom:15px;min-height:500px;">
							<div class="tab-pane active" id="tab1">
								<%@ include file="/formdesigner/page/component/suggestion/basicInfo.jsp" %>
							</div>
							<div class="tab-pane" id="tab2">
								<%@ include file="/formdesigner/page/component/suggestion/columns.jsp" %>
							</div>
							<div class="tab-pane" id="tab3">
								<%@ include file="/formdesigner/page/component/common/hidden-disabled-readonly.jsp" %>													
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script src="<%=basePath%>resources/scripts/jquery.serializejson.min.js"></script>
	<script src="<%=basePath%>/formdesigner/scripts/form.cpcommons.js"></script>
	<script type="text/javascript">	
		var basePath = "<%=basePath%>";
		$('document').ready(function(){
			initFormValidator();
			
			$("#componentForm").bootstrapValidator('addField', 'dataAlias',{validators:{
				notEmpty: {
                    message: "数据源别名不能为空"
                }
			}});
			
			initializeDocument("${componentType}","${_ComponentTypeName[componentType]}");
			
			var index = 0;
			$("#addColumn").click(function(){
				var str="<tr id=dataTr'"+index+"'>";			
				str+="<td><input class='form-control'  name='columns[][columnTitle]' id='columnTitle-"+index+"'" + "type=text" + "/>"+"</td>";
				str+="<td><input class='form-control'  name='columns[][columnField]' id='columnField-"+index+"'" + "type=text" + "/>"+"</td>";
				str+="<td><input class='form-control'  name='columns[][columnWidth]' id='columnWidth-"+index+"'" + "type=text" + "/>"+"</td>";
				str+="<td><input class='form-control'  name='columns[][order]' id='columnOrder-"+index+"'/>"+"</td>";
				str+="<td style='text-align:center;'><select class='editType form-control' name='columns[][editType]' id='editType-'"+ index +"'>";
				str+="<option value='0'>无</option><option value='text'>文本</option><option value='numberbox'>数字</option>"+
					 "<option value='datebox'>日期框</option><option value='combobox'>下拉框</option><option value='checkbox'>选择框</option></select></td>";
				str+="<td><input class='form-control' class='editValue' disabled='disabled' style='width:100%' name='columns[][editValue]' id='editValue-"+index+"'/>"+"</td>";
				str+="<td style='text-align:center;'><a href='javascript:void(0)' class='removeTr'>删除</a>"+"</td>";
				str+="</tr>";
				index++;
				$("#columnsBody").append(str);
			});
			
			$(".editType").off("change").on("change","",function(){
				var val=$(this).val();
				if(val=="combobox"||val=="checkbox"){
					$(this).parent("td").next("td").children(".editValue").attr("disabled",false);
				}else{
					$(this).parent("td").next("td").children(".editValue").attr("disabled",true);
				}
			});	
			
			$("#columnsBody").on("click",".removeTr",function(){
				var _this = $(this);
				_this.parents("tr").remove();
			});
		});
	</script>
</body>
</html>

