<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="form-group">
	<div class="customGroup">
		<div class="col-md-12">
			<label class="control-label">操作按钮</label>
			<div>
				<label class="checkbox-inline"><input name="operateAdd" type="checkbox" value="1">新增</label> 
				<label class="checkbox-inline"><input name="operateEdit" type="checkbox" value="1">编辑</label> 
				<label class="checkbox-inline"><input name="operateSave" type="checkbox" value="1">保存</label>
				<label class="checkbox-inline"><input name="operateUndo" type="checkbox" value="1">还原</label>
				<label class="checkbox-inline"><input name="operateDelete" type="checkbox" value="1">删除</label>
			</div>
		</div>
	</div>
</div>	

<div class="form-group">
	<div class="customGroup">
		<div class="col-md-12">
			<label class="control-label required">新增弹出页面</label>
			<select name="alertPage" class="form-control" id="alertPage" style="width:100%;"></select>
		</div>
	</div>
</div>	

<div class="form-group">
	<div class="customGroup">
		<div class="col-md-6 col-sm-6">
			<label class="control-label">弹出页面高度</label>
			<input name="viewHeight" class="form-control" id="viewHeight" type="text" value="" />		
		</div>
	</div>
	<div class="customGroup">
		<div class="col-md-6 col-sm-6">
			<label class="control-label">弹出页面宽度</label>
			<input name="viewWidth" class="form-control" id="viewWidth" type="text" value="" />
		</div>
	</div>
</div>

<div class="text-right" style="margin-bottom:10px;">
	<button type="button" class="btn btn-info btn-sm" id="addParament"><i class="fa fa-plus"></i> 新增传参</button>	
</div>

<div class="columnsTable" contenteditable="false" >
	<table class="table table-bordered" id="vt" align="right">
		<thead>
			<tr>
				<th width="40%">传入参数</th>
				<th width="10%" style="text-align: center;">——》</th>
				<th width="40%">存储组件</th>
				<th width="10%" style="text-align: center;">删除</th>
			</tr>
		</thead>
		<tbody id="paramentBody"></tbody>
	</table>
</div>