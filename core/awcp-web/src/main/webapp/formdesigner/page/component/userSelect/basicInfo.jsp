<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="form-horizontal">
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-6 col-sm-6">
				<label class="control-label">单用户</label>
				<select id="isSingle" class="chosen-select form-control" tabindex="2" name="isSingle">
					<option value="N">否</option>
					<option value="Y">是</option>
				</select>
			</div>
		</div>
	</div>
</div>
<%@ include file="../common/basicAttr.jsp" %>