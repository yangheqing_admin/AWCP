<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="form-horizontal">
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-12 col-sm-12">
				<label class="control-label">api接口名称：</label>
				<input type="text" name='select_select2_sql' id='select_select2_sql' value='province;city;area' class='form-control'/>
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-12 col-sm-12">
				<label class="control-label">select标签name值：</label>
				<input type="text" name='select_select2_name' id='select_select2_name'  class='form-control' value="provinceId;cityId;id"/>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<div class="customGroup">
			<div class="col-md-12 col-sm-12">
				<label class="control-label">select标签值：</label>
				<input type="text" name='select_select2_label' id='select_select2_label'  class='form-control' value="省：;市：;县："/>
			</div>
		</div>
	</div>
</div>
<%@ include file="../common/basicAttr.jsp" %>