<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<input type="hidden" name="dynamicPageId" id="dynamicPageId" />
<input type="hidden" name="pageId" id="pageId"/>
<input type="hidden" name="componentType" id="componentType">
<div class="form-group">
	<div class="customGroup">
		<div class="col-md-12">
			<label class="control-label required">名称</label>
			<input class="form-control" name="name" id="name" type="text" />
		</div>	
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label required">显示文本</label>
			<input class="form-control" name="title" id="title" type="text" />
		</div>	
	</div>
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label">跨行数</label>
			<input type="text" class="form-control" name="rowSpan" id="rowSpan" />
		</div>	
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label required">布局</label>
			<div class="input-group">
				<input type="text" readonly="readonly" class="form-control" name="layoutName" id="layoutName" />
				<div class="input-group-btn"><button class="btn btn-default" type="button" id="layoutSelect">选择</button></div>
				<input type="hidden" readonly="readonly" class="form-control" name="layoutId" id="layoutId" />
			</div>
		</div>	
	</div>
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label required">序号</label>
			<input class="form-control" name="order" id="order" type="text" />	
		</div>	
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label">是否必须</label>
			<select class="form-control" name="isRequired" id="isRequired">	
				<option value="2">否</option>		
				<option value="1">是</option>			
			</select>
		</div>	
	</div>
	<div class="customGroup">
		<div class="col-sm-6 col-md-6">
			<label class="control-label">对齐属性</label>
			<select class="form-control" name="labelAlign" id="labelAlign">			
				<option value="center">居中</option>
				<option value="right">居右</option>
				<option value="left">居左</option>
			</select>
		</div>	
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-12 col-md-12">
			<label class="control-label">样式库样式</label>
			<select class="form-control" name="css" id="css" style="width:100%;">
				<c:forEach items="${styles}" var="style">
					<option value="">请选择</option>
					<option value="${style.id }">${style.name }</option>
				</c:forEach>
			</select>
		</div>
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-12 col-md-12">
			<label class="control-label">自定义样式</label>
			<input class="form-control" name="style" id="style" type="text" />
		</div>
	</div>
</div>

<div class="form-group">
	<div class="customGroup">
		<div class="col-sm-12 col-md-12">
			<label class="control-label">描述：</label>
			<textarea class="form-control" name="description" id="description" rows="4">${vo.description}</textarea>
		</div>
	</div>
</div>