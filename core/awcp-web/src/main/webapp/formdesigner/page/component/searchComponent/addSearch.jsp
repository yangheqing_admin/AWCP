<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ page isELIgnored="false"%> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="renderer" content="webkit">
	<title>搜索条件组件编辑页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp" %>
</head>
<body id="main">
	<div class="content" style="background-color: white;">
		<div class="opeBtnGrop">
			<button type="button" class="btn btn-primary" id="saveComponent"><i class="fa fa-save"></i> 保存</button>
			<button type="button" class="btn btn-default" id="cancelComponent"><i class="fa fa-remove"></i> 关闭</button>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div id="searchTab" class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab1" data-toggle="tab">基本</a></li>
							<li class=""><a href="#tab3" data-toggle="tab">状态</a></li>
						</ul>
					</div>
					<form id="componentForm" class="form-horizontal">
						<div class="tab-content" style="margin:0px 15px 100px 15px;padding-bottom:15px;min-height:500px;">
							<div class="tab-pane active" id="tab1">
								<input type="hidden" name="dynamicPageId" id="dynamicPageId"/>
								<input type="hidden" name="pageId" id="pageId"/>
								<input type="hidden" name="componentType" id="componentType">				
								
								<div class="form-group">
									<div class="customGroup">
										<div class="col-md-12">
											<label class="control-label required">名称</label>
											<div class="input-group">					
												<input class="form-control" name="name" id="name" type="text" />
												<div class="input-group-btn">
									             	<button class="btn btn-default" type="button" id="editName">编辑名称</button>
											 	</div>
											</div>
										</div>	
									</div>
								</div>
																			
								<div class="form-group">
									<!-- <div class="customGroup">
										<div class="col-md-6 col-sm-6">
											<label class="control-label required">布局</label>
											<div class="input-group">
												<input type="text" readonly="readonly" class="form-control" name="layoutName" id="layoutName">
												<div class="input-group-btn"> <button class="btn btn-default" type="button" id="layoutSelect">选择</button></div>
												<input type="hidden" readonly="readonly" class="form-control" name="layoutId" id="layoutId">
											</div>
										</div>
									</div> -->
									<div class="customGroup">
										<div class="col-md-6 col-sm-6">
											<label class="control-label required">序号</label>
											<input name="order" class="form-control" id="order" type="text" value="">
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<div class="customGroup">
										<div class="col-md-6 col-sm-6">
											<label class="control-label">文本标签值：</label>	
											<input type="text" name='textLabel' id='textLabel'  class='form-control' 
												placeholder="角色@组织"/>
										</div>
									</div>
									<div class="customGroup">
										<div class="col-md-6 col-sm-6">
											<label class="control-label">文本name值：</label>	
											<input type="text" name='textName' id='textName'  class='form-control' 
												placeholder="role@group"/>
											<input type="hidden" name='searchLocation' id='searchLocation' value="1"/>
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<div class="customGroup">
										<div class="col-md-6 col-sm-6">
											<label class="control-label">日期选择标签值：</label>	
											<input type="text" name='dateSelectLabel' id='dateSelectLabel' class='form-control' 
												placeholder="开始日期@结束日期"/>
										</div>
									</div>
									<div class="customGroup">
										<div class="col-md-6 col-sm-6">
											<label class="control-label">日期选择name值：</label>	
											<input type="text" name='dateSelectName' id='dateSelectName' class='form-control' 
												placeholder="beginDate@endDate"/>
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<div class="customGroup">
										<div class="col-md-12 col-sm-12">
											<label class="control-label">下拉框选项：</label>	
											<textarea name='selectOption' id='selectOption' rows='4' class='form-control' 
												style="resize: vertical;" placeholder="province@1=深圳市;2=坪山区"></textarea>
										</div>
									</div>
								</div>								
								<div class="form-group">
									<div class="customGroup">
										<div class="col-md-6 col-sm-6">
											<label class="control-label">下拉框标签值：</label>	
											<input type="text" name='selectLabel' id='selectLabel'  class='form-control' 
												placeholder="名称@ID"/>
										</div>
									</div>
									<div class="customGroup">
										<div class="col-md-6 col-sm-6">
											<label class="control-label">下拉框name值：</label>	
											<input type="text" name='selectName' id='selectName'  class='form-control' 
												placeholder="name@id"/>
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<div class="customGroup">
										<div class="col-md-12 col-sm-12">
											<label class="control-label">单选框选项：</label>	
											<textarea name='radioOption' id='radioOption' rows='4' class='form-control'
												  placeholder="province@1=深圳市;2=坪山区"></textarea>
										</div>
									</div>
								</div>						
								<div class="form-group">
									<div class="customGroup">
										<div class="col-md-6 col-sm-6">
											<label class="control-label">单选框标签值：</label>	
											<input type="text" name='radioLabel' id='radioLabel' class='form-control' 
												placeholder="名称@ID"/>
										</div>
									</div>
									<div class="customGroup">
										<div class="col-md-6 col-sm-6">
											<label class="control-label">单选框name值：</label>	
											<input type="text" name='radioName' id='radioName' class='form-control'
												placeholder="name@id"/>
										</div>
									</div>
								</div>
							
								<div class="form-group">
									<div class="customGroup">
										<div class="col-md-12 col-sm-12">
											<label class="control-label">多选框选项：</label>	
											<textarea name='checkboxOption' id='checkboxOption' rows='4' class='form-control'
												  placeholder="province@1=深圳市;2=坪山区"></textarea>
										</div>
									</div>
								</div>							
								<div class="form-group">
									<div class="customGroup">
										<div class="col-md-6 col-sm-6">
											<label class="control-label">多选框标签值：</label>	
											<input type="text" name='checkboxLabel' id='checkboxLabel' class='form-control' 
												placeholder="名称@ID"/>
										</div>
									</div>
									<div class="customGroup">
										<div class="col-md-6 col-sm-6">
											<label class="control-label">多选框name值：</label>	
											<input type="text" name='checkboxName' id='checkboxName' class='form-control'
												placeholder="name@id"/>
										</div>
									</div>
								</div>

							</div>
							<div class="tab-pane" id="tab3">
								<%@ include file="/formdesigner/page/component/common/hidden-disabled-readonly-defaultvalue.jsp" %>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script src="<%=basePath%>resources/scripts/jquery.serializejson.min.js"></script>
	<script src="<%=basePath%>/formdesigner/scripts/form.cpcommons.js"></script>
	<script type="text/javascript">	
		var basePath = "<%=basePath%>";
		$('document').ready(function(){
			initFormValidator();
			
			initializeDocument("${componentType}","${_ComponentTypeName[componentType]}");
		});
	</script>
</body>
</html>