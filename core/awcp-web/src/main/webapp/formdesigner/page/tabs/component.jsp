<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<div>
	<c:forEach items="${_COMPOENT_TYPE_NAME }" var="cs" varStatus="status">
		<div class="compoents">
			<a href="javascript:void(0)" onclick="editComponent('${cs.key}','${vo.id }',null);">${cs.value}</a>
		</div>
	</c:forEach>
</div>
<div class="btn-group">
	<div class="btn-group">
		<button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
			<i class="fa fa-plus"></i> 快捷新增 <span class="caret"></span>
		</button>
		<ul class="dropdown-menu" role="menu">
			<li><a href="javascript:void(0)" onclick="quickComByDataSource();">以数据源新增</a></li>
		</ul>
	</div>
	<div class="btn-group">
		<button type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown">
			<i class="fa fa-edit"></i> 批量修改 <span class="caret"></span>
		</button>
		<ul class="dropdown-menu" role="menu">
			<li><a href="javascript:void(0)" onclick="batchModifiAllowNull(0);">允许为空</a></li>
			<li><a href="javascript:void(0)" onclick="batchModifiAllowNull(1);">不允许为空</a></li>
			<li><a href="javascript:void(0)" onclick="modifiDataAlias();">批量修改数据源别名</a></li>
		</ul>
	</div>	
	<button class="btn btn-sm btn-danger" id="deleteComponent">
		<i class="fa fa-remove"></i> 删除
	</button>
	<button class="btn btn-sm btn-success" id="copyComponent">
		<i class="fa fa-copy"></i> 复制
	</button>
	<button class="btn btn-sm btn-info" id="refreshComponentOrder">
		<i class="fa fa-toggle-down"></i> 重置序号
	</button>
</div>
<div id="collapseButton" class="in">
	<input type="hidden" name="currentPage" value="1">
	<div class="form-group">					
		<div class="col-md-2">
			<label class="control-label">布局行条件</label>
			<input name="rows1" class="form-control" id="rowValue" type="text" value="">
		</div>
		<div class="col-md-2">
			<label class="control-label">布局列条件</label>
			<input name="columns1" class="form-control" id="colValue" type="text" value="">
		</div>			
		<div class="col-md-2">
			<label class="control-label">组件类型</label>
			<select id="typeId" class="form-control" >
				<option value="">-请选择-</option>
				<c:forEach items="${_COMPOENT_TYPE_NAME }" var="cs">
					<option value="${cs.key}">${cs.value}</option>
				</c:forEach>
			</select>
		</div>	
		<div class="col-md-2">
			<label class="control-label">组件或文本</label>
			<input name="cname" class="form-control" id="cname" type="text" value="">
		</div>				 
		<div class="col-md-2">
			<label class="control-label">数据源</label>	
			<input name="dataCode" class="form-control" id="dataCode" type="text" value="">
		</div>
		<div class="col-md-2 btn-group" style="margin-top: 25px;">
			<button class="btn btn-info" type="button" onclick="loadComByCondition()">
				<i class="fa fa-search"></i> 提交
			</button>
		</div>	
	</div>
</div>

<div class="componentTable" contenteditable="false">
	<table class="table table-bordered" id="componentTable" align="left">
		<thead>
			<tr>
				<th width="5%"><input type="checkbox" id="checkAllComponent" /></th>
				<th width="25%">name</th>
				<th width="10%">类型</th>
				<th width="30%">数据源</th>
				<th width="15%">名称</th>
				<th width="10%">布局</th>
				<th width="5%">序号</th>
			</tr>
		</thead>
		<tbody id="componentt">
		</tbody>
	</table>
</div>
