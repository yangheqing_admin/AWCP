<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="btn-group">
	<div class="btn-group">
		<button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
			<i class="fa fa-plus"></i> 新增 <span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li><a href="javascript:void(0)" onclick="addAct('2000');">普通</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('2001');">保存</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('2002');">返回</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('2003');">删除</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('2004');">导入</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('2009');">新增</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('2010');">更新</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('3000');">流程办结</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('3001');">流程撤销</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('3002');">流程拒绝</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('3003');">流程回撤</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('3004');">流程退回</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('3005');">流程发送</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('3006');">流程跳转</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('3007');">流程传阅</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('3008');">加签</a></li>
			<li><a href="javascript:void(0)" onclick="addAct('3009');">流程保存</a></li>				
		</ul>
	</div>
	<button class="btn btn-sm btn-danger" id="deleteAct">
		<i class="fa fa-remove"></i> 删除
	</button>
</div>
<div class="actTable" contenteditable="false" style="margin-top:10px;">
	<table class="table table-bordered" id="acttable" align="left">
		<thead>
			<tr>
				<th width="30px"><input type="checkbox" id="checkAllAct" /></th>
				<th>名称</th>
				<th>类型</th>
				<th>顺序</th>
			</tr>
		</thead>
		<tbody id="actdatabody"></tbody>
	</table>
</div>