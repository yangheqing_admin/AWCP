<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="btn-group">
	<button type="button" class="btn btn-sm btn-primary" onclick="editDatasource()" >
		<i class="fa fa-plus"></i> 添加
	</button>
	<button class="btn btn-sm btn-danger" id="deleteModel">
		<i class="fa fa-remove"></i> 删除
	</button>
	<button class="btn btn-sm btn-success" id="copyModel">
		<i class="fa fa-copy"></i> 复制
	</button>
	<button class="btn btn-sm btn-info" id="freshModel">
		<i class="fa fa-refresh"></i> 刷新
	</button>
</div>
<div class="modelTable" contenteditable="false" style="margin-top:10px;">
	<table class="table table-bordered" id="modelTable">
		<thead>
			<tr>
				<th width="30px"><input type="checkbox" id="checkAllDatasource" /></th>
				<th>名称</th>
				<th>描述</th>
			</tr>
		</thead>
		<tbody id="datasourcebody">
		</tbody>
	</table>
</div>
