<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<div class="btn-group" style="margin-bottom:10px;">
	<button type="button" class="btn btn-sm btn-primary" id="addWorkflowBtn"><i class="fa fa-plus"></i> 新增</button>
	<button type="button" class="btn btn-sm btn-danger" id="deleteWorkflowBtn"><i class="fa fa-remove"></i> 删除</button>
	<button type="button" class="btn btn-sm btn-success" id="configWorkflowBtn"><i class="fa fa-edit"></i> 配置节点参数</button>
	<button type="button" class="btn btn-sm btn-info" id="authorityWorkflowBtn"><i class="fa fa-edit"></i> 配置权限组</button>
</div>
<table class="table table-hover" id="nodetable">
	 <thead>
        <tr>
			<th data-width="30" data-checkbox="true"></th>
			<th>结点名称</th>
			<th>流程名称</th>
        </tr>
    </thead>
	<tbody id="noteList"></tbody>
</table>





