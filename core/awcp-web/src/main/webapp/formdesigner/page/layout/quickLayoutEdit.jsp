<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>布局快捷新增页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body id="main">
	<section class="content">
		<div class="opeBtnGrop">
	    	<button class="btn btn-primary" id="saveLayout"><i class="fa fa-save"></i> 保存</button>
		    <button class="btn btn-default" id="closeBtn"></i><i class="fa fa-close"></i> 关闭</button>
    	</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" id="groupForm">	
						 	<div class="form-group">
						 		<div class="customGroup">						      	
							      	<div class="col-md-4">
							      		<input type="hidden"  name="pageId" id="pageId">
										<input type="hidden" name="dynamicPageId" id="dynamicPageId"/>
										<input type="hidden" name="others[parentId]" id="parentId"/>						      	 
										<label class="control-label">名称</label>
										<input id="name" type="text" class="form-control" name="name" value="" />
							      	</div>	
							    </div>
							</div>
							<div class="form-group">
							    <div class="customGroup">			  
									<div class="col-md-4">
										<label class="control-label required">行数</label>
										<input id="rows" type="text" class="form-control" name="rows" value=""/>
									</div>
						    	</div>
						    </div>
						    <div class="form-group">
								<div class="customGroup">			  
									<div class="col-md-4">
										<label class="control-label required">列数</label>
										<input id="cods" type="text" class="form-control" name="cods" value=""/>
									</div>
						    	</div>
						    </div>										
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>		
	<%@ include file="/resources/include/common_lte_js.jsp" %>				
	<script type="text/javascript">
		$(function(){
			$("#closeBtn").on("click",function(){
				top.dialog({id:window.name}).close();
			});
			
			var $form = $("#groupForm");
        	$form.bootstrapValidator({
        		excluded: [":disabled"],
        	    fields:{
                    "rows": {
                        validators: {
                        	notEmpty: {
                                message: "行数不能为空"
                            },
                            regexp: {
                                regexp: /^[1-9]\d*$/,
                                message: '请输入正整数'
                            }
                        }
                    },
                    "cods": {
                        validators: {
                        	notEmpty: {
                                message: "列数不能为空"
                            },
                            regexp: {
                                regexp: /^[1-9]\d*$/,
                                message: '请输入正整数'
                            }
                        }
                    }
        	    }
        	});
		
        	var data = null;
        	try {
				var dialog = top.dialog.get(window);
				dialog.title("布局快捷新增");
				dialog.width(300);
				dialog.height(500);
				dialog.reset(); 
				data = dialog.data;
				if(data.dynamicPageId){
					$("#dynamicPageId").val(data.dynamicPageId);
				}
				if(data.parentId){
					$("#parentId").val(data.parentId);
				}
			} catch (e) {
				return;
			}
			
			$("#saveLayout").click(function(){
				if(!validateForm()){
	    			return false;
	    		}
				$.ajax({
				   	type: "POST",
				   	url: basePath + "layout/quickSave.do",
				  	data: {
					   	rows: $("#rows").val(),
					   	parentId: $("#parentId").val(),
					   	dynamicPageId: $("#dynamicPageId").val(),
					   	cods: $("#cods").val(),
					   	name:$("#name").val()
				   	},
				   	success: function(data){
						if(data != null){	
							if(dialog != null){
								dialog.close(data);
								dialog.remove();
							}
						}else{
							alert("保存失敗");
						}
				   	}
				})	
			});
		})	
	</script>
</body>
</html>
