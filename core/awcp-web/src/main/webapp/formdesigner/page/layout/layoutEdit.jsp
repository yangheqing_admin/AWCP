<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>布局编辑页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body id="main">
	<section class="content">
		<div class="opeBtnGrop">
	    	<button class="btn btn-primary" id="saveLayout"><i class="fa fa-save"></i> 保存</button>
		    <button class="btn btn-default" id="closeBtn"></i><i class="fa fa-close"></i> 关闭</button>
    	</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" id="groupForm">	
						 	<div class="form-group">
						 		<div class="customGroup">						      	
							      	<div class="col-md-4">
							      		<input type="hidden"  name="pageId" id="pageId" />
										<input type="hidden" name="dynamicPageId" id="dynamicPageId" />
										<input type="hidden" name="parentId" id="parentId" />						      	 
										<label class="control-label required">名称</label>
										<input id="name" type="text" class="form-control" name="name" />
							      	</div>	
							    </div>
							</div>
							<div class="form-group">
							    <div class="customGroup">			  
									<div class="col-md-4">
										<label class="control-label">布局类型</label>
										<input type="hidden" id="layoutType" name="layoutType" />
										<input type="text" class="form-control" readonly="readonly" id="layoutTypeName" />
									</div>
						    	</div>
						    </div>
						    <div class="form-group">
								<div class="customGroup">			  
									<div class="col-md-4">
										<label class="control-label required">占比</label>
										<input id="proportion" type="text" class="form-control" name="proportion" />
									</div>
						    	</div>
						    </div>
						    <div class="form-group">
						    	<div class="customGroup">			  
									<div class="col-md-4">
										<label class="control-label required">顺序</label>
										<input id="order" type="text" class="chosen-select form-control" name="order" />
									</div>
						    	</div>
						    </div>										
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>		
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script src="<%=basePath%>resources/scripts/json2.js"></script>
	<script src="<%=basePath%>resources/scripts/jquery.serializejson.min.js"></script>
	<script type="text/javascript">		
		//在更新时加载组件值;
		function loadLayoutData(storeObject){
			var item = JSON.parse(storeObject.content);
			if(item.pageId != null){
				if(item.pageId) {
					$("#pageId").val(item.pageId);
				}
				if(item.name) {
					$("#name").val(item.name);
				}
				if(item.proportion) {
					$("#proportion").val(item.proportion);
				}
				if(item.order) {
					$("#order").val(item.order);
				}
				if(item.parentId) {
					$("#parentId").val(item.parentId);
				}
				if(item.systemId) {
					$("#systemId").val(item.systemId);			
				}
				if(item.layoutType)	{
					var type = item.layoutType=="2"?"row":"col";
					setLoyoutType(type);
				}
			}
		}
		
		function setLoyoutType(type){
			if("row" == type){
				$("#layoutType").val("2");
				$("#layoutTypeName").val("水平布局（行）");
			} else if("col" == type){
				$("#layoutType").val("1");
				$("#layoutTypeName").val("垂直布局（列）");
			}
		}
		
		$(function(){
			$("#closeBtn").on("click",function(){
				top.dialog({id:window.name}).close();
			});
			
			var $form = $("#groupForm");
        	$form.bootstrapValidator({
        		excluded: [":disabled"],
        	    fields:{
        	    	"name": {
                        validators: {
                            notEmpty: {
                                message: "名称不能为空"
                            }
                        }
                    },
                    "proportion": {
                        validators: {
                        	notEmpty: {
                                message: "占比不能为空"
                            },
                            regexp: {
                                regexp: /^[1-9]\d*$/,
                                message: '请输入正整数'
                            }
                        }
                    },
                    "order": {
                        validators: {
                        	notEmpty: {
                                message: "顺序不能为空"
                            },
                            regexp: {
                                regexp: /^[1-9]\d*$/,
                                message: '请输入正整数'
                            }
                        }
                    }
        	    }
        	});
			
			try {
				var dialog = top.dialog.get(window);
				dialog.title("布局配置");
				dialog.width(300);
				dialog.height(500);
				dialog.reset();
			} catch (e) {
				return;
			}
			
			//判断是否有数据，初始化 所属模型
			var data = null;//dialog.data;
			
			var layoutId = "";
			var type = "";
			if(dialog != null){
				data = dialog.data;
				layoutId = data.pageId;
				if(data.type){
					setLoyoutType(data.type);
				}
				if(data.dynamicPageId){
					$("#dynamicPageId").val(data.dynamicPageId);
				}
				if(data.parentId){
					$("#parentId").val(data.parentId);
				}
			}	
			
			//如果是编辑，则加载组件内容
			if(layoutId){
				$.ajax({
					type: "POST",
					url: basePath + "layout/getLayoutById.do",
					data: "storeId="+layoutId,
					success: function(data){
						if(data != null && data.id != null){	
							loadLayoutData(data);
						}else{
							alert("保存失敗");
						}
					}
				});	
			}
			
			function validateCheckOut(dynamicPageId){
				var flag;
				$.ajax({
					type:"POST",
					url:basePath + "fd/validateCheckOut.do?id=" + dynamicPageId,
					async:false,
					dataType:'json',
					//回调函数
					success:function(result){
						if(result.value == 1){
							flag = true;
						}else{
							alert(result);
							flag = false;
						}	   	
					},
 		 	 	 	error: function (XMLHttpRequest, textStatus, errorThrown) { 
 		  	       		alert(errorThrown); 
  		 	       		flag = false;
  		 	 	 	}
				});
				return flag;
			}

			$("#saveLayout").click(function(){	
				if(!validateForm()){
	    			return false;
	    		}
				if(!validateCheckOut($("#dynamicPageId").val())){	//校验是否已签出			
					return false;
				}				
				var formJson = $("#groupForm").serializeJSON();
				var sjContent = JSON.stringify(formJson);
				$.ajax({
					type: "POST",
				   	url: basePath + "layout/save.do",
				   	data: {
				   		id: $("#pageId").val(),
				   		name: $("#name").val(),
				   		description: $("#parentId").val(),
				   		content: sjContent,
				   		dynamicPageId: $("#dynamicPageId").val(),
				   		order: $("#order").val()
				   	},
				   	success: function(data){
						if(data != null){	
							$("#layoutId").val(data.id);
							if(dialog != null){
								dialog.close(data);
								dialog.remove();
							}
						}else{
							alert("保存失敗");
						}
				   	}
				})				
			});
		})
	</script>
</body>
</html>
