<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>批量修改布局页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body id="main">
	<section class="content">
		<div class="opeBtnGrop">
	    	<button class="btn btn-primary" id="batchModifyProportion"><i class="fa fa-save"></i> 保存</button>
		    <button class="btn btn-default" id="closeBtn"></i><i class="fa fa-close"></i> 关闭</button>
    	</div>
    	<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" id="groupForm">	
						 	<div class="form-group">
						 		<div class="customGroup">						      	
							      	<div class="col-md-4">
							      		<input type="hidden" name="dynamicPageId" id="dynamicPageId"/>					      	 
										<label class="control-label ">占比</label>
										<input name="proportion" class="form-control" id="proportion" type="text" />
							      	</div>	
							    </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>		
	<%@ include file="/resources/include/common_lte_js.jsp" %>		
	<script type="text/javascript">	
		$(function(){
			$("#closeBtn").on("click",function(){
				top.dialog({id:window.name}).close();
			});
			
			var $form = $("#groupForm");
        	$form.bootstrapValidator({
        		excluded: [":disabled"],
        	    fields:{
                    "proportion": {
                        validators: {
                        	notEmpty: {
                                message: "占比不能为空"
                            },
                            regexp: {
                                regexp: /^[1-9]\d*$/,
                                message: '请输入正整数'
                            }
                        }
                    }                
        	    }
        	});
			
			var data = null;
			var _selects = null;
			try {
				var dialog = top.dialog.get(window);
				data = dialog.data;
				if(data != null && data._selects){
					_selects = data._selects;
				}
				if(data.dynamicPageId && $("#dynamicPageId")){
					$("#dynamicPageId").val(data.dynamicPageId);
				}
			} catch (e) {
				return;
			}
			
			$("#batchModifyProportion").click(function(){
				if(!validateForm()){
	    			return false;
	    		}
				$.ajax({
					type: "POST",
					url: basePath + "layout/batchModifyProportion.do",
					data: {
						proportion: $("#proportion").val(),
						_selects:_selects,
						dynamicPageId: $("#dynamicPageId").val()
					},
					async : false,
					success: function(data){
						if("1" == data){	
							if(dialog != null){
								dialog.close(data);
								dialog.remove();
							}
						}else{
							alert("修改失败");
						}
					}
				});	
				return false;
			});
			
		});
	</script>
</body>
</html>



