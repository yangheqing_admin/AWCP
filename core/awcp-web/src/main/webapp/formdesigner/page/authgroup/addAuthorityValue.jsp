<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>权限组权限配置页面</title>
	<%@ include file="/resources/include/common_lte_css.jsp"%>
</head>
<body>
	<section class="content">
		<div class="opeBtnGrop">
		    <button class="btn btn-default" id="closeBtn"></i><i class="fa fa-close"></i> 关闭</button>
    	</div>
    	<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-body">
						<form class="form-horizontal" id="groupForm">	
							<input type="hidden" name="dynamicPageId" id="dynamicPageId"/>
							<input type="hidden" name="componentId" id="componentId"/>
							<table id="treeTable1" class="table table-bordered" width="100%">
			                	<thead>
			                		<tr>
			                    		<th style="width:200px;">组件名称</th>
			                    		<th><input type="checkbox" id="t1" onClick="allReady()">所有</th>
						  				<th><input type="checkbox" id="t2" onClick="allModiy()">所有</th>
						  				<th><input type="checkbox" id="t3" onClick="allHidden()">所有</th>
			                		</tr>
			                	</thead>
			                	<tbody id="component">
			                
			                	</tbody>
			            	</table>
			            </form>
			    	</div>
			    </div>
			</div>
		</div>
	</section>
	<%@ include file="/resources/include/common_lte_js.jsp" %>
	<script src="<%=basePath%>resources/scripts/map.js"></script>
	<script src="<%=basePath%>formdesigner/scripts/dynamicpage.constant.js"></script>
	<script src="<%=basePath%>resources/scripts/json2.js"></script>
	<script src="<%=basePath%>resources/scripts/jquery.serializejson.min.js"></script>
	<script src="<%=basePath%>resources/plugins/treeTable/jquery.treeTable.js"></script>
	<script type="text/javascript">				
		$(function(){
			var data = null;
			try {
				var dialog = top.dialog.get(window);
				dialog.title("权限配置");
				dialog.width(800);
				dialog.height(500);
				dialog.reset(); // 重置对话框位置
				data = dialog.data;
				if(data.dynamicPageId){
					$("#dynamicPageId").val(data.dynamicPageId);
				}
				if(data.componentId){
					$("#componentId").val(data.componentId);
				}
			} catch (e) {
				return;
			}
			
			$("#closeBtn").on("click",function(){
				top.dialog({id:window.name}).close();
			});
			
			//查询组件数据
			$.ajax({
				type: "POST",
			   	async: false,
			   	url: basePath + "authority/getComponentListByPageId.do",
			   	data:{
			   		dynamicPageId: $("#dynamicPageId").val(),
			   		pageSize: 9999,
			   		authorityGroup: $("#componentId").val()
			   	},
			   	success: function(data){
					if(data != null){
					 	$("#component").empty();
					 	$.each(data,function(index,item){
					 		var vo = eval("(" + item.content + ")");
					 		//回显权限值
					   		var s=item.scope;
					   		var rd=new RegExp('10');
					   		var up=new RegExp('11');
					   		var he=new RegExp('12');
					   		var tab="";
					   		//过滤一些组件
					   		if(vo.componentType!='1009' && vo.componentType!='1010'){
						   		if(rd.test(s)){
						   			tab="<td><input type='checkbox' name='ready' value='10' id='"+vo.name+"10' onclick='saveAuthority(\""+vo.name+"\",10)' checked='checked'>只读</td>";
						   		}else{
						   			tab="<td><input type='checkbox' name='ready' value='10' id='"+vo.name+"10' onclick='saveAuthority(\""+vo.name+"\",10)'>只读</td>";
						   		}
						   		if(up.test(s)){
						   			tab+="<td><input type='checkbox' name='modiy' value='11' id='"+vo.name+"11' onclick='saveAuthority(\""+vo.name+"\",11)' checked='checked'>修改</td>";
						   		}else{
						   			tab+="<td><input type='checkbox' name='modiy' value='11' id='"+vo.name+"11' onclick='saveAuthority(\""+vo.name+"\",11)'>修改</td>";
						   		}
						   		if(he.test(s)){
						   			tab+="<td><input type='checkbox' name='hidden' value='12' id='"+vo.name+"12' onclick='saveAuthority(\""+vo.name+"\",12)' checked='checked'>隐藏</td>";
						   		}else{
						   			tab+="<td><input type='checkbox' name='hidden' value='12' id='"+vo.name+"12' onclick='saveAuthority(\""+vo.name+"\",12)'>隐藏</td>";
						   		}
						   
						   		if(!vo.configures){
						   			var trHtml = "<tr id='"+vo.name+"'>"+
					   				"<td>"+pageConstant.getComponentTypes().get(vo.componentType)+"("+vo.name+")</td>"+
					   				tab+
					   				"</tr>";
						    		$("#component").append(trHtml);
						   		}
						   	}
					   
						    //包含组件框
						    if(vo.configures){
						    	var data=vo.configures;
						    	$("#component").append(""+
						   			"<tr id='"+vo.name+"'>"+
						   			"<td><span controller='true'>"+pageConstant.getComponentTypes().get(vo.componentType)+"("+vo.name+")</span></td>"+
						  	 		tab+
						   			"</tr>"+
						   		"");
						   
						  	 	$.each(data,function(index,con){
							   		var componentId=vo.name+"_"+con.name;
							   		var authorityGroup=$("#componentId").val();
							   		var strVal=getValue(componentId,authorityGroup);
							   		var tab2="";
						   			//过滤一些组件
			              			if(con.componentType!='1009'&& con.componentType!='1010'){
								   		if(rd.test(strVal)){
								   			tab2="<td><input type='checkbox' name='ready' value='10' id='"+con.name+"10' onclick='saveAuthority_include(\""+con.name+"\",10,\""+vo.name+"_"+con.name+"\")' checked='checked'>只读</td>";
								   		}else{
								   			tab2="<td><input type='checkbox' name='ready' value='10' id='"+con.name+"10' onclick='saveAuthority_include(\""+con.name+"\",10,\""+vo.name+"_"+con.name+"\")'>只读</td>";
								   		}
								   		if(up.test(strVal)){
								   			tab2+="<td><input type='checkbox' name='modiy' value='11' id='"+con.name+"11' onclick='saveAuthority_include(\""+con.name+"\",11,\""+vo.name+"_"+con.name+"\")' checked='checked'>修改</td>";
								   		}else{
								   			tab2+="<td><input type='checkbox' name='modiy' value='11' id='"+con.name+"11' onclick='saveAuthority_include(\""+con.name+"\",11,\""+vo.name+"_"+con.name+"\")'>修改</td>";
								   		}
								   		if(he.test(strVal)){
								   		
								   			tab2+="<td><input type='checkbox' name='hidden' value='12' id='"+con.name+"12' onclick='saveAuthority_include(\""+con.name+"\",12,\""+vo.name+"_"+con.name+"\")' checked='checked'>隐藏</td>";
								   		}else{
								   			tab2+="<td><input type='checkbox' name='hidden' value='12' id='"+con.name+"12' onclick='saveAuthority_include(\""+con.name+"\",12,\""+vo.name+"_"+con.name+"\")'>隐藏</td>";
								   		}
								   		var str="<tr id='"+con.name+"' pId='"+vo.name+"'>";
								   		str+="<td><span>"+pageConstant.getComponentTypes().get(con.componentType)+"("+con.name+")</span></td>";
								   		str+=tab2;
								   		str+="</tr>";
								   		$("#component").append(str);
							   		
							   		}
						   		});
						   	}
					 	});
						refreshTree();	
			    	}
				}
			});	
		});			
			
		function saveAuthority(componentId,value){
	  		var status=1;
	  		if($("#"+componentId+value).is(':checked')==false){
	  			status=0;
	  		}
	  	 	$.ajax({
				type: "POST",
			    async: false,
				url: "<%=basePath%>authority/addAuthorityValue.do",
				data: "componentId="+componentId+ 
				"&value=" + value+
				"&status=" + status+
				"&authorityGroupId="+$("#componentId").val(),
				success: function(data){
					
				}
			});	
		}
	
		//配置包含组件
		function saveAuthority_include(componentId,value,includeId){
	  		var status=1;
	  		if($("#"+componentId+value).is(':checked')==false){
	  			status=0;
	  		}
	   		$.ajax({
			    type: "POST",
			    async: false,
				url: "<%=basePath%>authority/addAuthorityValue.do",
				data: "componentId="+includeId + 
				"&value=" + value+
				"&status=" + status+
				"&includeComponent=" + includeId+
				"&authorityGroupId="+$("#componentId").val(),
				success: function(data){
				
				}	
			});	
		}
	
		function allReady(){
		 	if($('#t1').is(':checked')==true) {
    			$("input[name='ready']").not(":checked").click();
	 		} else{
			 	$("input[name='ready']:checked").click();
	 		}
		}
	
		function allModiy(){
	 		if($('#t2').is(':checked')==true) {
			 	$("input[name='modiy']").not(":checked").click();
	 		} else{
			 	$("input[name='modiy']:checked").click();
	 		}
		}
	
		function allHidden(){
	 		if($('#t3').is(':checked')==true) {
			 	$("input[name='hidden']").not(":checked").click();
	 		} else{
			 	$("input[name='hidden']:checked").click();
			}
		}
	
		//初始化tree
		function refreshTree(){
			var option = {
		        theme:'vsStyle',
		        expandLevel :1,
		        beforeExpand : function($treeTable, id) {
		            //判断id是否已经有了孩子节点，如果有了就不再加载，这样就可以起到缓存的作用
		            if ($('.' + id, $treeTable).length) { return; }
		            //这里的html可以是ajax请求
		            var html = '<tr id="8" pId="6"><td>5.1</td><td>可以是ajax请求来的内容</td></tr>'
		                     + '<tr id="9" pId="6"><td>5.2</td><td>动态的内容</td></tr>';
		            $treeTable.addChilds(html);
		        },
		        onSelect : function($treeTable, id) {
		            window.console && console.log('onSelect:' + id);
		        }
		    }
    		$('#treeTable1').treeTable(option);
		}
	
		function getValue(componentId,authorityGroup){
			var strVal="";
			$.ajax({
			  	type: "POST",
			  	async: false,
		      	url: "<%=basePath%>authority/getComponentValueByInclude.do",
			  	data:"componentId="+componentId+"&authorityGroup="+authorityGroup,
			  	success: function(data){
					strVal=data;
			  	}	
			});
			return strVal;
		}
	
		$("#close").click(function(){
			top.dialog({id : window.name}).close();
		});
	</script>
</body>
</html>
