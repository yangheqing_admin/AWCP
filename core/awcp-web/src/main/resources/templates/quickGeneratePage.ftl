<#--	生成列表页面数据源Sql		-->
<#macro createListPageSql>
var where = "where 1=1";
<#list searchList as searchItem>
var ${searchItem.itemCode} = request.getParameter("${searchItem.itemCode}");
if(${searchItem.itemCode}){
<#if "text"==searchItem.searchType>
	where += " and ${searchItem.itemCode} like '%" + ${searchItem.itemCode} + "%'";
<#else>	
	where += " and ${searchItem.itemCode}='" + ${searchItem.itemCode} + "'";
</#if>
}	
</#list>
"select ${modelItemCodes} from ${tableName} " + where;
</#macro>

<#--	生成表单页面数据源Sql		-->
<#macro createFormPageSql>
var id = request.getParameter("id");
"select ${modelItemCodes} from ${tableName} where ID='" + id + "'";
</#macro>

<#--	生成保存按钮客户端脚本		-->
<#macro createSaveBtnClientScript>
$("#buttons").find("button").attr("disabled",true);
$("#actId").val($(this).attr("id"));
$.ajax({
	type: "POST",
	url: basePath + "document/excuteOnly.do",
	data: $("#groupForm").serialize(),
	async: false,
	success: function(data){
		if(data==1){
			Comm.alert("保存成功",function(){
				toListPage(basePath + "document/view.do?dynamicPageId=${dynamicPageId}");
			});
		} else{
			if(data.message){
				Comm.alert(data.message,function(){
					$("#buttons").find("button").attr("disabled",false);
				});
			} else{
				Comm.alert("保存失败",function(){
					$("#buttons").find("button").attr("disabled",false);
				});
			}               
		}
	}
});
</#macro>

<#--	生成保存按钮服务端脚本		-->
<#macro createSaveBtnServerScript>
var id = DocumentUtils.getDataItem("${tableName}","ID");
var retVal = "1";
if(id == null || id == "") {
	id = UUID.randomUUID().toString();
	DocumentUtils.setDataItem("${tableName}","ID",id);
	if(DocumentUtils.saveData("${tableName}") == null){
		DocumentUtils.setResultMessage("保存失败");
	}
} else {
	if(DocumentUtils.updateData("${tableName}") == null){
		DocumentUtils.setResultMessage("更新失败");
	}
}
retVal;
</#macro>

<#--	生成返回按钮客户端脚本,新增按钮客户端脚本		-->
<#macro createBtnClientScript>
toListPage(basePath + "document/view.do?dynamicPageId=${dynamicPageId}");
</#macro>

<#--	生成编辑按钮客户端脚本		-->
<#macro createEditBtnClientScript>
location.href = basePath + "document/view.do?dynamicPageId=${dynamicPageId}&id=" + id;
</#macro>

<#--	生成删除按钮客户端脚本		-->
<#macro createDelBtnClientScript>
$("#actId").val($(this).attr("id"));
dialogConfirm("是否确认删除?",function(){
    $.ajax({
		type: "POST",
		url: basePath + "document/excuteOnly.do",
		data: $("#groupForm").serialize()+"&id=" + id,
		async: false,
		success: function(data){
			if(data==1){
		        Comm.alert("删除成功",function(){
		            $("#groupForm").submit();
		        });
		    } else{
		        Comm.alert("删除失败");
		    }
		}
	});
});
</#macro>

<#--	生成删除按钮服务端脚本		-->
<#macro createDelBtnServerScript>
var id = request.getParameter("id");
DocumentUtils.excuteUpdate("delete from ${tableName} where ID=?",id);
</#macro>