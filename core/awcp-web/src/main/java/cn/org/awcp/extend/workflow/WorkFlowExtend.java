package cn.org.awcp.extend.workflow;

import cn.org.awcp.formdesigner.application.vo.DocumentVO;
import cn.org.awcp.venson.api.APIService;
import cn.org.awcp.venson.api.PFMAPI;
import cn.org.awcp.workflow.service.IWorkFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 流程扩展类，流程执行前，执行后可以增加新功能
 *
 * @author venson
 * @version 20180307
 */
@Component("workFlowExtend")
public class WorkFlowExtend {

    @Autowired
    private IWorkFlowService iWorkFlowServiceImpl;
   
    @Resource(name = "apiService")
    private APIService apiService;
   
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 流程执行前执行的方法
     *
     * @param request http请求
     * @param docVo   文档类
     * @throws ScriptException 脚本引擎异常
     */
    public void beforeExecuteFlow(HttpServletRequest request, DocumentVO docVo) throws ScriptException {
        //执行流程前,查看是否有存在流程执行前事件
        PFMAPI before = PFMAPI.get("workFlowExecuteBefore");
        if (before != null) {
            ScriptEngine engine = apiService.getEngine();
            engine.eval(before.getAPISQL());
        }
    }

    /**
     * 流程执行成功后执行的方法
     *
     * @param docVo     文档类
     * @param resultMap 返回的结果集
     * @throws ScriptException 脚本引擎异常
     */
    public void afterExecuteFlow(DocumentVO docVo, Map<String, Object> resultMap) throws ScriptException {
        Boolean result = (Boolean) resultMap.get("success");
        if (result) {
            PFMAPI after = PFMAPI.get("workFlowExecuteAfter");
            //执行流程后,查看是否有存在流程执行后事件
            if (after != null) {
                ScriptEngine engine = apiService.getEngine();
                engine.eval(after.getAPISQL());
            }
            // 执行消息推送
            iWorkFlowServiceImpl.addPush(docVo, resultMap);
            // 流程日志
            iWorkFlowServiceImpl.addCommonLogs(docVo, resultMap);
            iWorkFlowServiceImpl.saveComment(docVo,resultMap);
        }
    }

}
