package cn.org.awcp.formdesigner.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import cn.org.awcp.common.util.CommonUtil;
import cn.org.awcp.core.domain.BaseExample;
import cn.org.awcp.core.domain.SzcloudJdbcTemplate;
import cn.org.awcp.core.utils.BeanUtils;
import cn.org.awcp.core.utils.SessionUtils;
import cn.org.awcp.core.utils.constants.ResourceTypeEnum;
import cn.org.awcp.core.utils.constants.SessionContants;
import cn.org.awcp.formdesigner.application.service.FormdesignerService;
import cn.org.awcp.formdesigner.application.service.StoreService;
import cn.org.awcp.formdesigner.application.vo.DynamicPageVO;
import cn.org.awcp.formdesigner.application.vo.PageActVO;
import cn.org.awcp.formdesigner.application.vo.StoreVO;
import cn.org.awcp.formdesigner.core.constants.FormDesignGlobal;
import cn.org.awcp.formdesigner.core.domain.design.context.component.Layout;
import cn.org.awcp.formdesigner.core.engine.FreeMarkers;
import cn.org.awcp.metadesigner.application.MetaModelOperateService;
import cn.org.awcp.unit.service.PunResourceService;
import cn.org.awcp.unit.vo.PunResourceVO;
import cn.org.awcp.unit.vo.PunSystemVO;
import cn.org.awcp.venson.controller.base.ReturnResult;
import cn.org.awcp.venson.controller.base.StatusCode;

@RequestMapping(value="/auto")
@Controller
public class AutoPageDesignController {

	@Autowired
	private FormdesignerService formdesignerServiceImpl;
	
	@Autowired
	@Qualifier("storeServiceImpl")
	private StoreService storeService;
	
	@Resource(name = "metaModelOperateServiceImpl")
	private MetaModelOperateService meta;
	
	@Autowired
	@Qualifier("punResourceServiceImpl")
	private PunResourceService punResourceService;
	
	@Autowired
	private SzcloudJdbcTemplate jdbcTemplate;
	
	/**
	 * 获取数据源
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getDataSourceArray")
	public JSONArray getDataSourceArray(String dynamicPageId){
		String sql = "select model_xml from p_fm_dynamicpage where id=?";
		String model_xml = StringEscapeUtils.unescapeHtml4(jdbcTemplate.queryForObject(sql, String.class,dynamicPageId));
		return JSON.parseArray(model_xml);
	}
	
	/**
	 * 删除组件
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/deleteStore")
	public ReturnResult deleteStore(String id){
		ReturnResult ret = new ReturnResult();
		boolean bool = storeService.delete(Arrays.asList(id));
		if(!bool) {
			ret.setStatus(StatusCode.FAIL);
		}
		return ret;
	}
	
	/**
	 * 修改列表页面组件的顺序
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/updateListCompOrder")
	public ReturnResult updateListCompOrder(String ids){
		ReturnResult ret = new ReturnResult();
		if(StringUtils.isNotBlank(ids)) {
			List<StoreVO> stores = storeService.findByIds(ids.split(","));
			int index = 1;
			for(StoreVO store : stores) {
				store.setOrder(index);
				JSONObject jsonObj = JSON.parseObject(store.getContent());
				jsonObj.put("order", index);
				store.setContent(JSON.toJSONString(jsonObj));
				storeService.save(store);
				index++;
			}
		}
		return ret;
	}
	
	/**
	 * 可视化表单设计
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView editPage(@RequestParam(value = "_selects", required = false) String id) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("formdesigner/page/dynamicPage-autoEdit");
		if (id != null) {
			DynamicPageVO vo = formdesignerServiceImpl.findById(id);
			if (vo != null) {
				BaseExample actExample = new BaseExample();
				actExample.createCriteria().andEqualTo("DYNAMICPAGE_ID", id).andLike("CODE",
						StoreService.PAGEACT_CODE + "%");
				PageList<StoreVO> actStore = storeService.selectPagedByExample(actExample, 1, Integer.MAX_VALUE,
						"BTN_GROUP ASC,T_ORDER ASC");
				List<PageActVO> acts = new ArrayList<PageActVO>();
				for (StoreVO store : actStore) {
					PageActVO act = JSON.parseObject(store.getContent(), PageActVO.class);
					acts.add(act);
				}
				actStore.clear();
				mv.addObject("acts", acts);
			}
			mv.addObject("vo", vo);
		}
		mv.addObject("modulars", meta.search("select ID,modularName from p_fm_modular"));
		mv.addObject("templates", meta.search("select id,file_name from p_fm_template"));
		mv.addObject("styles", meta.search("select ID,NAME from p_fm_store where code like '0.1.6.%'"));
		mv.addObject("_COMPOENT_TYPE_NAME", new TreeMap<>(FormDesignGlobal.COMPOENT_TYPE_NAME));
		return mv;
	}
	
	/**
	 * 获取组件和布局
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getLayoutAndComponent")
	public Map<String,Object> getLayoutAndComponent(HttpServletRequest request){
		Map<String,Object> ret = new HashMap<String,Object>();
		String pageId = request.getParameter("pageId");
		String sql = "select ID,CONTENT from p_fm_store where DYNAMICPAGE_ID=? and code like '0.1.7.%' order by T_ORDER";
		List<Map<String,Object>> comps = jdbcTemplate.queryForList(sql, pageId);
		for(Map<String,Object> map : comps) {
			String content = (String) map.get("CONTENT");
			map.put("CONTENT", JSON.parseObject(content));
		}
		sql = "select ID,CONTENT from p_fm_store where DYNAMICPAGE_ID=? and code like '0.1.8.%' order by T_ORDER";
		List<Map<String,Object>> layouts = jdbcTemplate.queryForList(sql, pageId);
		List<Map<String,Object>> rows = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> cols = new ArrayList<Map<String,Object>>();
		for(Map<String,Object> map : layouts) {
			String content = (String) map.get("CONTENT");
			JSONObject jsonObj = JSON.parseObject(content);
			map.put("CONTENT", jsonObj);
			if(jsonObj.getInteger("layoutType")==1) {
				cols.add(map);
			} else {
				rows.add(map);
			}
		}
		ret.put("comps", comps);
		ret.put("rows", rows);
		ret.put("cols", cols);
		return ret;
	}
	
	/**
	 * 保存布局
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ResponseBody
	@RequestMapping(value="/saveAndUpdateLayout")
	public List<Map> saveAndUpdateLayout(HttpServletRequest request){
		List<String> ids = new ArrayList<String>();
		String json = request.getParameter("json");
		String dynamicPageId = request.getParameter("dynamicPageId");
		if(StringUtils.isBlank(json)) {
			String sql = "delete from p_fm_store where CODE like '0.1.8.%' and DYNAMICPAGE_ID=?";
			jdbcTemplate.update(sql,dynamicPageId);
			return new ArrayList<Map>();
		} else {
			Object o = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
			Long sysId = 0L;
			if (o instanceof PunSystemVO) {
				PunSystemVO system = (PunSystemVO) o;
				sysId = system.getSysId();
			}
			List<Map> rows = JSON.parseArray(json, Map.class);
			if(rows.size() == 0) {
				String sql = "delete from p_fm_store where CODE like '0.1.8.%' and DYNAMICPAGE_ID=?";
				jdbcTemplate.update(sql,dynamicPageId);
				return new ArrayList<Map>();
			}
			for(Map row : rows) {
				String rowId = (String) row.get("rowId");
				Integer rowIndex = (Integer) row.get("rowIndex");
				// 保存行布局
				StoreVO rowSV = new StoreVO();
				rowSV.setSystemId(sysId);
				rowSV.setDescription("");
				rowSV.setCode(StoreService.LAYOUT_CODE + System.currentTimeMillis());
				rowSV.setDynamicPageId(dynamicPageId);
				rowSV.setName("_row" + rowIndex);
				rowSV.setOrder(rowIndex);
				rowSV.setId(rowId);
				Layout s = new Layout();
				s.setName(rowSV.getName());
				s.setOrder(rowSV.getOrder());
				s.setPageId(rowId);
				s.setDynamicPageId(dynamicPageId);
				s.setTop(0);
				s.setLeft(0);
				s.setLayoutType(2);
				s.setProportion(12L);
				s.setParentId("");
				s.setDynamicPageId(dynamicPageId);
				String jContent = JSONObject.toJSONString(s);
				rowSV.setContent(jContent);
				rowId = storeService.save(rowSV);
				row.put("rowId", rowId);
				ids.add(rowId);
				List<Map> cols = (List<Map>) row.get("cols");
				// 遍历存列布局
				for (Map col : cols) {
					Integer colIndex = (Integer) col.get("colIndex");
					String colId = (String) col.get("colId");
					long colWidth = (Integer) col.get("colWidth");
					StoreVO codSV = new StoreVO();
					codSV.setSystemId(sysId);
					codSV.setDescription(rowId);
					codSV.setCode(StoreService.LAYOUT_CODE + System.currentTimeMillis());
					codSV.setDynamicPageId(dynamicPageId);
					codSV.setName("_row" + rowIndex + "_cod" + colIndex);
					codSV.setOrder(colIndex);
					codSV.setId(colId);
					Layout s1 = new Layout();
					s1.setName(codSV.getName());
					s1.setOrder(codSV.getOrder());
					s1.setTop(0);
					s1.setLeft(0);
					s1.setLayoutType(1); 
					s1.setProportion(colWidth); 
					s1.setParentId(rowId); 
					s1.setDynamicPageId(dynamicPageId);
					s1.setPageId(colId);
					String jContent1 = JSONObject.toJSONString(s1);
					codSV.setContent(jContent1);
					colId = storeService.save(codSV);
					col.put("colId", colId);
					col.put("colName", codSV.getName());
					ids.add(colId);
				}
			}
			String wrapQuotIds = CommonUtil.wrapQuot(ids);
			if(StringUtils.isNotBlank(wrapQuotIds)) {
				String sql = "delete from p_fm_store where CODE like '0.1.8.%' and "
						+ "ID not in (" + wrapQuotIds + ") and DYNAMICPAGE_ID=?";
				jdbcTemplate.update(sql, dynamicPageId);
			}		
			return rows;
		}		
	}

	/**
	 * 修改组件的布局
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/updateCompLayout")
	public ReturnResult updateCompLayout(HttpServletRequest request) {
		ReturnResult ret = new ReturnResult();
		String compId = request.getParameter("compId");
		String layoutId = request.getParameter("layoutId");
		String layoutName = request.getParameter("layoutName");
		if(StringUtils.isNotBlank(compId) && StringUtils.isNotBlank(layoutId)) {
			StoreVO store = storeService.findById(compId);
			JSONObject jsonObj = JSON.parseObject(store.getContent());
			jsonObj.put("layoutId", layoutId);
			jsonObj.put("layoutName", layoutName);
			jsonObj.put("order", Integer.parseInt(layoutName.replaceAll("\\D", "")));
			store.setContent(JSON.toJSONString(jsonObj));
			storeService.save(store);
		}
		return ret;
	}

	/**
	 * 根据配置快速生成页面
	 * @param request
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@ResponseBody
	@RequestMapping(value="/quickGeneratePage")
	public ReturnResult quickGeneratePage(HttpServletRequest request) {
		ReturnResult ret = ReturnResult.get();
		try {
			jdbcTemplate.beginTranstaion();
			String tableName = request.getParameter("tableName");
			String pageName = request.getParameter("pageName");
			String formLayout = request.getParameter("formLayout");
			String listArr = request.getParameter("listArr");
			String formArr = request.getParameter("formArr");
			List<Map> listPageList = JSON.parseArray(listArr, Map.class);
			List<Map> formPageList = JSON.parseArray(formArr, Map.class);
			String listPageId = saveListPage(pageName, tableName, listPageList);
			generateListComp(listPageId, tableName, listPageList);
			String formPageId = saveFormPage(pageName, tableName, formPageList);
			generateFormComp(formPageId, tableName, formPageList, formLayout);
			generatePageAct(listPageId, formPageId, tableName, pageName);
			formdesignerServiceImpl.publish(formPageId);
			formdesignerServiceImpl.publish(listPageId);
			jdbcTemplate.commit();
		} catch(Exception e) {
			jdbcTemplate.rollback();
			ret.setStatus(StatusCode.FAIL);
			e.printStackTrace();
		}
		return ret;
	}
	
	//生成列表动态页面
	@SuppressWarnings("rawtypes")
	private String saveListPage(String pageName,String tableName,List<Map> list) {		
		pageName += "列表页面";
		String dataJson = "";
		String modelItemCodes = getModelItemCodes(list);	
		if(!modelItemCodes.contains("ID")) {
			modelItemCodes = "ID," + modelItemCodes;
		}
		if(StringUtils.isNotBlank(modelItemCodes)) {
			String sql = getListPageSql(list, tableName, modelItemCodes);
			dataJson = getPageDataJson(tableName, modelItemCodes, sql,1003);
			dataJson = StringEscapeUtils.escapeHtml4(dataJson);
		}		
		DynamicPageVO vo = generateDynamicPageVO(pageName,1003,dataJson,"1",1);
		formdesignerServiceImpl.saveOrUpdate(vo);
		return vo.getId();
	}
	
	//生成表单动态页面
	@SuppressWarnings("rawtypes")
	private String saveFormPage(String pageName,String tableName,List<Map> list) {		
		pageName += "表单页面";
		String dataJson = "";
		String modelItemCodes = getModelItemCodes(list);	
		if(StringUtils.isNotBlank(modelItemCodes)) {
			String sql = getFormPageSql(tableName,modelItemCodes);
			dataJson = getPageDataJson(tableName, modelItemCodes, sql,1002);
			dataJson = StringEscapeUtils.escapeHtml4(dataJson);
		}		
		DynamicPageVO vo = generateDynamicPageVO(pageName,1002,dataJson,"2",0);
		formdesignerServiceImpl.saveOrUpdate(vo);
		return vo.getId();
	}
	
	//生成表单页面的sql
	private String getFormPageSql(String tableName, String modelItemCodes) {
		String templateStr = "<#include \"templates/quickGeneratePage.ftl\"><@createFormPageSql />";
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("modelItemCodes", modelItemCodes);
		root.put("tableName", tableName);	
		String sql = FreeMarkers.renderString(templateStr, root);
		return sql;
	}
	
	//生成表单页面的布局和组件
	@SuppressWarnings("rawtypes")
	private void generateFormComp(String pageId,String tableName,List<Map> list,String formLayout) {
		int cods = Integer.parseInt(formLayout);
		List<Map> hiddens = new ArrayList<Map>();
		List<Map> formComps = new ArrayList<Map>();
		for(Map map : list) {
			String compType = (String) map.get("compType");
			if("1010".equals(compType)) {
				hiddens.add(map);
			} else {
				formComps.add(map);
			}
		}
		int rows = formComps.size() / cods;
		if(formComps.size() % cods != 0) {
			rows++;
		}
		List<Map<String,String>> layouts = quickSaveLayout(rows, cods, "", pageId);
		generateHiddenComp(hiddens, pageId, tableName);
		generateOtherComp(layouts, formComps, pageId, tableName);
	}
	
	//快速生成布局
	private List<Map<String,String>> quickSaveLayout(long rows, long cods, String parentId, String dynamicPageId) {
		List<Map<String,String>> layouts = new ArrayList<Map<String,String>>();
		for (int i = 0; i < rows; i++) {
			// 保存行布局
			StoreVO rowSV = new StoreVO();
			Object o = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
			if (o instanceof PunSystemVO) {
				PunSystemVO system = (PunSystemVO) o;
				rowSV.setSystemId(system.getSysId());
			}
			rowSV.setDescription(parentId);
			rowSV.setCode(StoreService.LAYOUT_CODE + System.currentTimeMillis());
			rowSV.setDynamicPageId(dynamicPageId);
			rowSV.setName("_row" + (i + 1));
			rowSV.setOrder(i + 1);
			Layout s = new Layout();
			s.setName(rowSV.getName());
			s.setOrder(rowSV.getOrder());
			s.setPageId("");
			s.setDynamicPageId(dynamicPageId);
			s.setTop(0);
			s.setLeft(0);
			s.setLayoutType(2);
			s.setProportion(12L);
			s.setParentId(parentId);
			s.setDynamicPageId(dynamicPageId);
			String jContent = JSONObject.toJSONString(s);
			rowSV.setContent(jContent);
			String id = storeService.save(rowSV);

			// 遍历存列布局
			for (int j = 0; j < cods; j++) {
				StoreVO codSV = new StoreVO();
				Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
				if (obj instanceof PunSystemVO) {
					PunSystemVO system = (PunSystemVO) obj;
					codSV.setSystemId(system.getSysId());
				}
				codSV.setDescription(id);
				codSV.setCode(StoreService.LAYOUT_CODE + System.currentTimeMillis());
				codSV.setDynamicPageId(dynamicPageId);
				codSV.setName("_row" + (i + 1) + "_cod" + (j + 1));
				codSV.setOrder((i + 1) * (j + 1));
				Layout s1 = new Layout();
				s1.setName(codSV.getName());
				s1.setOrder(codSV.getOrder());
				s1.setPageId("");
				s1.setTop(0);
				s1.setLeft(0);
				s1.setLayoutType(1); // 垂直布局
				s1.setProportion(12 / cods); // 占比为12除以列数
				s1.setParentId(id); // parentId为行布局的Id
				s1.setDynamicPageId(dynamicPageId);
				String jContent1 = JSONObject.toJSONString(s1);
				codSV.setContent(jContent1);
				storeService.save(codSV);
				Map<String,String> temp = new HashMap<String,String>();
				temp.put("layoutId", codSV.getId());
				temp.put("layoutName", codSV.getName());
				temp.put("order", (i+1) + "" + (j+1));
				layouts.add(temp);
			}
		}
		return layouts;
	}
	
	//生成隐藏框组件
	@SuppressWarnings("rawtypes")
	private void generateHiddenComp(List<Map> hiddens,String pageId,String tableName) {
		int order = 1;
		for(Map map : hiddens) {
			String itemCode = (String) map.get("itemCode");
			String dataItemCode = tableName + "." + itemCode;
			saveStoreVO(pageId, dataItemCode, getHiddenContent(), order);
			order++;
		}
	}
	
	//生成表单组件
	@SuppressWarnings("rawtypes")
	private void generateOtherComp(List<Map<String,String>> layouts,List<Map> formComps,String pageId,String tableName) {
		for(int i=0;i<formComps.size();i++) {
			Map formComp = formComps.get(i);
			Map<String,String> layout = layouts.get(i);
			String itemCode = (String) formComp.get("itemCode");
			String dataItemCode = tableName + "." + itemCode;
			String title = (String) formComp.get("title");
			String compType = (String) formComp.get("compType");
			String required = formComp.get("isNeed") + "";
			int order = Integer.parseInt(layout.get("order"));
			String layoutId = layout.get("layoutId");
			String layoutName = layout.get("layoutName");
			saveStoreVO(pageId, dataItemCode, getFormContent(compType, title, layoutId, required, layoutName), order);
		}
	}
	
	@SuppressWarnings("rawtypes")
	private String getModelItemCodes(List<Map> list) {
		StringBuffer sb = new StringBuffer();
		for(Map map : list) {
			sb.append(map.get("itemCode")).append(",");
		}
		if(sb.length() > 0) {
			sb.deleteCharAt(sb.length()-1);
		}
		return sb.toString();
	}
	
	//生成列表页面sql
	@SuppressWarnings("rawtypes")
	private String getListPageSql(List<Map> list, String tableName, String modelItemCodes) {
		String templateStr = "<#include \"templates/quickGeneratePage.ftl\"><@createListPageSql />";
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("modelItemCodes", modelItemCodes);
		root.put("tableName", tableName);
		List<Map> searchList = new ArrayList<Map>();
		for(Map map : list) {
			int isSearch = (Integer) map.get("isSearch");
			if(isSearch == 1) {
				searchList.add(map);
			}
		}
		root.put("searchList", searchList);
		String sql = FreeMarkers.renderString(templateStr, root);
		return sql;
	}
	
	/*
	 * 生成数据源
	 * @param tableName			表名
	 * @param modelItemCodes	数据源Item
	 * @param sql				数据源sql	
	 * @param pageType			页面类型
	 * @return
	 */
	private String getPageDataJson(String tableName,String modelItemCodes,String sql,int pageType) {
		List<Map<String,String>> modelList = new ArrayList<Map<String,String>>();
		Map<String,String> model = new HashMap<String,String>();
		model.put("id", UUID.randomUUID().toString());
		model.put("name", tableName);
		if(pageType == 1003) {
			model.put("isSingle", "1");
			model.put("isPage", "1");
		} else {
			model.put("isSingle", "0");
			model.put("isPage", "0");
		}	
		model.put("limitCount", "0");
		model.put("deleteSql", "");
		model.put("modelCode", tableName);
		model.put("description", "");
		model.put("modelItemCodes", modelItemCodes);
		model.put("sqlScript", sql);
		modelList.add(model);
		return JSON.toJSONString(modelList);
	}
	
	/*
	 * 生成动态页面
	 * @param pageName		动态页面名
	 * @param pageType		动态页面类型
	 * @param dataJson		数据源
	 * @param templateId	模板Id	
	 * @param isLimitPage	是否分页
	 * @return
	 */
	private DynamicPageVO generateDynamicPageVO(String pageName,int pageType,String dataJson,String templateId,int isLimitPage) {
		DynamicPageVO vo = new DynamicPageVO();
		vo.setName(pageName);
		vo.setPageType(pageType);
		vo.setDataJson(dataJson);
		vo.setTemplateId(templateId);
		vo.setIsLimitPage(isLimitPage);
		Object o = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		if (o instanceof PunSystemVO) {
			PunSystemVO system = (PunSystemVO) o;
			vo.setSystemId(system.getSysId());
		}
		vo.setIsCheckOut(1); 	
		vo.setCreatedUser("9999");
		vo.setUpdatedUser("9999");
		vo.setCheckOutUser("9999");
		vo.setCreated(new Date());
		vo.setUpdated(new Date());
		vo.setIsLog("0");
		vo.setShowReverseNum(0);
		return vo;
	}

	//生成列表页面的组件
	@SuppressWarnings("rawtypes")
	private void generateListComp(String pageId,String tableName,List<Map> list) {
		int order = 2;
		Map<String,String> searchs = new HashMap<String,String>();
		String[] keys = {"selectName","selectLabel","selectOption","dateSelectName","dateSelectLabel","textName","textLabel",
				"checkboxName","checkboxLabel","checkboxOption","radioName","radioOption","radioLabel"};
		for(String key : keys) {
			searchs.put(key, "");
		}
		for(Map map : list) {
			String itemCode = (String) map.get("itemCode");
			String dataItemCode = tableName + "." + itemCode;
			String columnName = (String) map.get("colName");
			String colType = (String) map.get("colType");
			JSONObject content = getColumnContent(colType, columnName);
			saveStoreVO(pageId, dataItemCode, content, order);
			order++;
			int isSearch = (Integer)map.get("isSearch");
			if(isSearch==1) {
				String searchType = (String) map.get("searchType");
				if("text".equals(searchType)) {
					updateSearchs(searchs, "textName", "textLabel", itemCode, columnName);
				} else if("date".equals(searchType)) {
					updateSearchs(searchs, "dateSelectName", "dateSelectLabel", itemCode, columnName);
				} else if("select".equals(searchType)) {
					updateSearchs(searchs, "selectName", "selectLabel", itemCode, columnName);
				} else if("radio".equals(searchType)) {
					updateSearchs(searchs, "radioName", "radioLabel", itemCode, columnName);
				} else if("checkbox".equals(searchType)) {
					updateSearchs(searchs, "checkboxName", "checkboxLabel", itemCode, columnName);
				}
			}
		}
		saveStoreVO(pageId, "", getSearchContent(searchs), 1);	
	}
	
	private void updateSearchs(Map<String,String> searchs,String keyName,String keyLable,String name,String label) {
		String oldName = searchs.get(keyName);
		oldName = StringUtils.isBlank(oldName) ? oldName : oldName+"@";
		String oldLable = searchs.get(keyLable);
		oldLable = StringUtils.isBlank(oldLable) ? oldLable : oldLable+"@";
		searchs.put(keyName, oldName + name);
		searchs.put(keyLable, oldLable + label);
	}
	
	private void saveStoreVO(String pageId,String dataItemCode,JSONObject content,int order) {
		StoreVO vo = new StoreVO();
		String name = UUID.randomUUID().toString().replaceAll("-", "");
		content.put("dynamicPageId", pageId);	
		content.put("name", name);
		content.put("dataItemCode", dataItemCode);
		content.put("order", order);
		vo.setCode(StoreService.COMPONENT_CODE + System.currentTimeMillis());
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		if (obj instanceof PunSystemVO) {
			PunSystemVO system = (PunSystemVO) obj;
			vo.setSystemId(system.getSysId());
		}
		vo.setContent(content.toJSONString());
		vo.setDynamicPageId(pageId);
		vo.setName(name);
		vo.setOrder(order);
		storeService.save(vo);
	}
	
	//列框
	private JSONObject getColumnContent(String colType, String columnName) {
		JSONObject content = new JSONObject();
		content.put("columnName", columnName);
		content.put("componentType", "1008");
		content.put("pageId", "");	
		content.put("width", "");
		content.put("columnFormat", "");
		content.put("columnPatten", "");
		 if("date".equals(colType)) {
			content.put("columnFormat", "1");
			content.put("columnPatten", "yyyy-MM-dd");	
		} else if("number".equals(colType)) {
			content.put("columnFormat", "2");
			content.put("columnPatten", "0.00");	
		} else if("img".equals(colType)) {
			content.put("columnFormat", "4");
			content.put("width", "100");	
		} 	
		content.put("alloworderby", "-1");
		content.put("orderby", "asc");
		content.put("sortName", "");
		content.put("description", "");
		setScript(content);
		content.put("showScript", "");
		return content;
	}
	
	//搜索组件
	private JSONObject getSearchContent(Map<String,String> searchs) {
		JSONObject content = new JSONObject();
		content.put("searchLocation", "1");
		content.put("componentType", "1036");
		content.put("pageId", "");	
		setScript(content);
		content.putAll(searchs);
		return content;
	}

	//隐藏框组件
	private JSONObject getHiddenContent() {
		JSONObject content = new JSONObject();
		content.put("componentType", "1010");
		content.put("pageId", "");	
		setScript(content);
		return content;
	}
	
	//表单组件
	private JSONObject getFormContent(String componentType,String title,String layoutId,String required,String layoutName) {
		JSONObject content = new JSONObject();
		content.put("componentType", componentType);
		content.put("title", title);
		content.put("layoutId", layoutId);
		content.put("required", required);
		content.put("layoutName", layoutName);
		content.put("pageId", "");	
		content.put("css", "");
		content.put("validatJson", "");
		content.put("description", "");
		content.put("enTitle", "");
		content.put("placeholder", "");
		content.put("style", "");
		setScript(content);
		setOtherAttr(content, componentType);
		return content;
	}
	
	private void setScript(JSONObject content) {
		content.put("hiddenScript", "");
		content.put("disabledScript", "");
		content.put("readonlyScript", "");
		content.put("defaultValueScript", "");
	}

	private void setOtherAttr(JSONObject content, String componentType) {
		switch(componentType) {
		case "1001":
			content.put("textType", "text");
			break;
		case "1002":
			content.put("dateType", "yyyy-mm-dd");
			break;
		case "1005":
			content.put("rowCount", "5");
		case "1020":
			content.put("isSingle", "N");
		}		
	}

	//生成按钮
	private void generatePageAct(String listPageId,String formPageId,String tableName,String pageName) {
		String formPageName = pageName + "表单页面";
		String listPageName = pageName + "列表页面";
		List<PageActVO> buttons = new ArrayList<PageActVO>();		
		String clientScript = getSaveBtnClientScript(listPageId);
		String serverScript = getSaveBtnServerScript(tableName);	
		buttons.add(getBasePageAct(true, clientScript, "primary", formPageId, formPageName, 
									"保存", 1, serverScript, "0", "fa fa-save"));//保存按钮
		clientScript = getBtnClientScript(listPageId);
		buttons.add(getBasePageAct(false, clientScript, "default", formPageId, formPageName, 
									"返回", 2, "", "0", "fa fa-reply"));			//返回按钮
		clientScript = getBtnClientScript(formPageId);
		buttons.add(getBasePageAct(false, clientScript, "primary", listPageId, listPageName, 
									"新增", 1, "", "0", "fa fa-plus"));			//新增按钮	
		clientScript = "closePage();";
		buttons.add(getBasePageAct(false, clientScript, "default", listPageId, listPageName, 
									"关闭", 2, "", "0", "fa fa-remove"));			//关闭按钮
		clientScript = getEditBtnClientScript(formPageId);
		buttons.add(getBasePageAct(false, clientScript, "success", listPageId, listPageName, 
									"编辑", 3, "", "1", "fa fa-edit"));			//编辑按钮
		clientScript = getDelBtnClientScript();
		serverScript = getDelBtnServerScript(tableName);
		buttons.add(getBasePageAct(false, clientScript, "danger", listPageId, listPageName, 
									"删除", 4, serverScript, "1", "fa fa-trash"));//删除按钮
		for(PageActVO vo : buttons) {
			savePageAct(vo);
		}
	}

	//保存按钮客户端脚本
	private String getSaveBtnClientScript(String dynamicPageId) {
		String templateStr = "<#include \"templates/quickGeneratePage.ftl\"><@createSaveBtnClientScript />";
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("dynamicPageId", dynamicPageId);
		String sql = FreeMarkers.renderString(templateStr, root);
		return sql;
	}
	
	//保存按钮服务端脚本
	private String getSaveBtnServerScript(String tableName) {
		String templateStr = "<#include \"templates/quickGeneratePage.ftl\"><@createSaveBtnServerScript />";
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("tableName", tableName);
		String sql = FreeMarkers.renderString(templateStr, root);
		return sql;
	}
	
	//返回按钮,新增按钮客户端脚本
	private String getBtnClientScript(String dynamicPageId) {
		String templateStr = "<#include \"templates/quickGeneratePage.ftl\"><@createBtnClientScript />";
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("dynamicPageId", dynamicPageId);
		String sql = FreeMarkers.renderString(templateStr, root);
		return sql;
	}
	
	//编辑按钮客户端脚本
	private String getEditBtnClientScript(String dynamicPageId) {
		String templateStr = "<#include \"templates/quickGeneratePage.ftl\"><@createEditBtnClientScript />";
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("dynamicPageId", dynamicPageId);
		String sql = FreeMarkers.renderString(templateStr, root);
		return sql;
	}
	
	//删除按钮客户端脚本
	private String getDelBtnClientScript() {
		String templateStr = "<#include \"templates/quickGeneratePage.ftl\"><@createDelBtnClientScript />";
		Map<String,Object> root = new HashMap<String,Object>();
		String sql = FreeMarkers.renderString(templateStr, root);
		return sql;
	}
	
	//删除按钮服务端脚本
	private String getDelBtnServerScript(String tableName) {
		String templateStr = "<#include \"templates/quickGeneratePage.ftl\"><@createDelBtnServerScript />";
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("tableName", tableName);
		String sql = FreeMarkers.renderString(templateStr, root);
		return sql;
	}
	
	private PageActVO getBasePageAct(boolean chooseValidate,String clientScript,String color,
			String dynamicPageId,String dynamicPageName,String name,int order,String serverScript,
			String place,String icon) {
		if(StringUtils.isNotBlank(clientScript)) {
			clientScript = StringEscapeUtils.escapeHtml4(clientScript);
		}
		if(StringUtils.isNotBlank(serverScript)) {
			serverScript = StringEscapeUtils.escapeHtml4(serverScript);
		}
		PageActVO vo = new PageActVO();
		vo.setActType(2000);
		vo.setButtonGroup(1);
		vo.setChooseValidate(chooseValidate);
		vo.setClientScript(clientScript);
		vo.setColor(color);
		vo.setDynamicPageId(dynamicPageId);
		vo.setDynamicPageName(dynamicPageName);
		vo.setName(name);
		vo.setOrder(order);
		vo.setServerScript(serverScript);
		vo.setPlace(place);
		vo.setIcon(icon);
		vo.setPageId("");
		vo.setDescription("");
		vo.setHiddenScript("");
		vo.setEnName("");
		return vo;
	}
		
	public void savePageAct(PageActVO vo) {
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		if (obj instanceof PunSystemVO) {
			PunSystemVO system = (PunSystemVO) obj;
			vo.setSystemId(system.getSysId());
		}
		StoreVO store = convert(vo);
		String id = storeService.save(store);
		if (StringUtils.isBlank(vo.getPageId())) { // 新增,则往resource表中写上数据
			PunResourceVO resource = new PunResourceVO();
			resource.setSysId(vo.getSystemId());
			resource.setResouType(ResourceTypeEnum.RESO_BUTTON.getkey());// 菜单;
			resource.setResourceName(vo.getName());
			resource.setRelateResoId(id);
			punResourceService.addOrUpdate(resource);
		} else {
			PunResourceVO resource = punResourceService.getResourceByRelateId(vo.getPageId(),
					ResourceTypeEnum.RESO_BUTTON.getkey());
			if (resource == null) {
				resource = new PunResourceVO();
				resource.setSysId(vo.getSystemId());
				resource.setRelateResoId(vo.getPageId());
				resource.setResouType(ResourceTypeEnum.RESO_BUTTON.getkey());
			}
			resource.setResourceName(vo.getName());
			punResourceService.addOrUpdate(resource);
		}
	}
	
	private StoreVO convert(PageActVO vo) {
		StoreVO store = new StoreVO();
		if (StringUtils.isBlank(vo.getCode())) {
			vo.setCode(StoreService.PAGEACT_CODE + System.currentTimeMillis());
		}
		store = BeanUtils.getNewInstance(vo, StoreVO.class);
		store.setContent(JSON.toJSONString(vo));
		store.setId(vo.getPageId());
		return store;
	}
}
