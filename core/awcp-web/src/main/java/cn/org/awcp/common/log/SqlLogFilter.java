package cn.org.awcp.common.log;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

/**
 * Logback日志过滤器
 * 生成Sql日志
 * @author yqtao
 *
 */
public class SqlLogFilter extends Filter<ILoggingEvent>{

	@Override
	public FilterReply decide(ILoggingEvent event) {
		String msg = event.getMessage();
		if(msg.contains("<#assign path = request.getContextPath()>")) {
			return FilterReply.DENY;
		}
		if(msg.contains("datasource[")) {
			return FilterReply.ACCEPT;
		}
		if(event.getMarker() != null) {
			String marker = event.getMarker().getName();
			if("MYBATIS".equals(marker)) {
				return FilterReply.ACCEPT;
			}
		}
		String loggerName = event.getLoggerName();
		if("cn.org.awcp.core.domain.SzcloudJdbcTemplate".equals(loggerName) ||
			"org.springframework.jdbc.datasource.DataSourceUtils".equals(loggerName)) {		
			return FilterReply.ACCEPT;
		}
		return FilterReply.DENY;
	}

}
