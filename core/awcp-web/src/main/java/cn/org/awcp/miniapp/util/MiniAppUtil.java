package cn.org.awcp.miniapp.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import cn.org.awcp.venson.controller.base.ControllerHelper;
import cn.org.awcp.venson.exception.PlatformException;
import cn.org.awcp.wechat.util.RequestUtil;

public class MiniAppUtil {
	private static String PATH;

	//二维码存放路径
	private static String getPath(){
		if(StringUtils.isBlank(PATH)){
			PATH = ControllerHelper.getUploadPath("/upload/code");
		}	
		return PATH;
	}
	
	//生成二维码分享图片
	public static void createAqrCodeImg(String imgName, String path, String getAccessTokenUrl, String createWxAqrCode){
		try {
			String jsonStr = RequestUtil.doGet(getAccessTokenUrl);
			JSONObject jsonObj = JSON.parseObject(jsonStr);
			String accessToken = jsonObj.getString("access_token");
			doPost(createWxAqrCode + accessToken, imgName, path);
		} catch (Exception e) {
			e.printStackTrace();
			throw new PlatformException("生成用户分享的二维码失败");
		}
	}
	
	private static void doPost(String requestUrl, String imgName, String path) throws Exception{
		Map<String,String> map = new HashMap<String,String>();
		map.put("path", path);	
		URL url = new URL(requestUrl);
		URI uri = new URI(url.getProtocol(), url.getHost() + ":" + url.getPort(),url.getPath(), url.getQuery(), null);
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(uri);
		httpPost.addHeader("Content-type","application/json; charset=utf-8");  
		httpPost.setHeader("Accept", "application/json"); 
		httpPost.setEntity(new StringEntity(JSON.toJSONString(map), Charset.forName("UTF-8")));
		CloseableHttpResponse response = httpclient.execute(httpPost);
		HttpEntity entity = response.getEntity();
		InputStream is = entity.getContent();
		saveToImgByInputStream(is, imgName);
	}
	
	private static void saveToImgByInputStream(InputStream instreams, String imgName) throws IOException {
        if (instreams != null) {
            try {
                File file = new File(getPath() + "/" + imgName);// 可以是任何图片格式.jpg,.png等
                FileOutputStream fos = new FileOutputStream(file);
                byte[] b = new byte[1024];
                int nRead = 0;
                while ((nRead = instreams.read(b)) != -1) {
                    fos.write(b, 0, nRead);
                }
                fos.flush();
                fos.close();
            } finally {
                try {
                    instreams.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
	
}
