package cn.org.awcp.miniapp.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.org.awcp.core.domain.SzcloudJdbcTemplate;
import cn.org.awcp.core.utils.Springfactory;
import cn.org.awcp.venson.exception.PlatformException;

/**
 * 微信小程序常量
 * @author yqtao
 *
 */
public class MiniAppConstant {

	/**
	 * 通过code获取openid的接口url
	 */
	public static final String GET_OPEN_ID_URL = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID" + 
			 "&secret=APP_SECRET&grant_type=authorization_code&js_code=";
	
	/**
	 * 获取微信AccessToken的接口url
	 */
	public static final String GET_ACCESS_TOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential" + 
			 "&appid=APPID&secret=APP_SECRET";
	
	/**
	 * 生成二维码的接口url
	 */
	public static final String CREATE_WX_AQR_CODE = "https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode?access_token=";
	
	/**
	 * 微信小程序的参数数据
	 */
	public static Map<String,Map<String,Object>> allParams = new HashMap<String,Map<String,Object>>();
	
	static {
		init();
	}
	
	//初始化微信小程序的参数数据
	private static void init() {
		SzcloudJdbcTemplate jdbcTemplate = Springfactory.getBean("jdbcTemplate");
		String sql = "select ID,appid,appsecret,mch_id,mch_key from p_fm_wechat_miniapp_param";
		List<Map<String,Object>> list = jdbcTemplate.queryForList(sql);
		allParams.clear();
		for(Map<String,Object> map : list) {
			String appid = (String) map.get("appid");
			allParams.put(appid, map);
		}
	}
	
	/**
	 * 根据appid获取该小程序的参数
	 * @param appid
	 * @return
	 */
	public static Map<String,Object> getMiniAppParams(String appid){
		if(allParams.containsKey(appid)) {
			return allParams.get(appid);
		} else {
			init();
			if(allParams.containsKey(appid)) {
				return allParams.get(appid);
			} else {
				throw new PlatformException("数据库没有该微信小程序的参数记录");
			}
		}
	}
	
}
