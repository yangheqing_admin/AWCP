package cn.org.awcp.common.log;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

/**
 * Logback日志过滤器
 * 生成脚本引擎日志
 * @author yqtao
 *
 */
public class ScriptLogFilter extends Filter<ILoggingEvent>{

	@Override
	public FilterReply decide(ILoggingEvent event) {		
		String loggerName = event.getLoggerName();
		if("cn.org.awcp.formdesigner.utils.BaseUtils".equals(loggerName) ||
			"cn.org.awcp.formdesigner.engine.util.MyScriptEngine".equals(loggerName)) {
			return FilterReply.ACCEPT;
		}
		return FilterReply.DENY;
	}

}