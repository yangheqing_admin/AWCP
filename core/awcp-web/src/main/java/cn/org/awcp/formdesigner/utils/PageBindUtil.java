package cn.org.awcp.formdesigner.utils;

import org.springframework.jdbc.core.JdbcTemplate;

import cn.org.awcp.core.utils.Springfactory;

/**
 * 页面绑定Util
 */
public class PageBindUtil {
	// 注入SpringJdbc
	private JdbcTemplate jdbcTemplate = Springfactory.getBean("jdbcTemplate");

	private final static PageBindUtil util = new PageBindUtil();

	public static PageBindUtil getInstance() {
		return util;
	}

	private PageBindUtil() {
		
	}

	/**
	 * 根据手机列表ID返回手机表单ID
	 * 
	 * @param mListPageId
	 * @return
	 */
	public String getMPageIdByMListPageId(String mListPageId) {
		String sql = "select PAGEID_APP from p_un_page_binding where PAGEID_APP_LIST=?";
		return jdbcTemplate.queryForObject(sql, String.class, mListPageId);
	}

	/**
	 * 根据PC列表ID返回PC表单ID
	 * 
	 * @param pcListPageId
	 * @return
	 */
	public String getPCPageIdByPCListPageId(String pcListPageId) {
		String sql = "select PAGEID_PC from p_un_page_binding where PAGEID_PC_LIST=?";
		return jdbcTemplate.queryForObject(sql, String.class, pcListPageId);
	}

	/**
	 * 根据任意PageID返回手机表单ID
	 * 
	 * @param defaultId
	 * @return
	 */
	public String getMPageIDByDefaultId(String defaultId) {
		String sql = "select PAGEID_APP from p_un_page_binding "
				+ "where PAGEID_PC=? or PAGEID_PC_LIST=? or PAGEID_APP=? or PAGEID_APP_LIST=?";
		return jdbcTemplate.queryForObject(sql, String.class, defaultId, defaultId, defaultId, defaultId);
	}

	/**
	 * 根据任意PageId返回PC表单ID
	 * 
	 * @param defaultId
	 * @return
	 */
	public String getPCPageIDByDefaultId(String defaultId) {
		String sql = "select PAGEID_PC from p_un_page_binding "
				+ "where PAGEID_PC=? or PAGEID_PC_LIST=? or PAGEID_APP=? or PAGEID_APP_LIST=?";
		return jdbcTemplate.queryForObject(sql, String.class, defaultId, defaultId, defaultId, defaultId);
	}
}
