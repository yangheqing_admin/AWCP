package cn.org.awcp.common.rabbitmq;

import java.util.HashMap;
import java.util.Map;

import org.springframework.amqp.core.AmqpTemplate;

/**
 * 发送Email消息队列生产者
 * @author yqtao
 *
 */
public class EmailMqProducer {
	
	private AmqpTemplate amqpTemplate;
	private String routekey;
	
	public void sendMessage(String email, String subject, String content) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("email", email);
		map.put("subject", subject);
		map.put("content", content);
		amqpTemplate.convertAndSend(routekey, map);
	}
	
	public AmqpTemplate getAmqpTemplate() {
		return amqpTemplate;
	}
	
	public void setAmqpTemplate(AmqpTemplate amqpTemplate) {
		this.amqpTemplate = amqpTemplate;
	}
	
	public String getRoutekey() {
		return routekey;
	}
	
	public void setRoutekey(String routekey) {
		this.routekey = routekey;
	}
}
