package cn.org.awcp.formdesigner.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import cn.org.awcp.base.BaseController;
import cn.org.awcp.core.domain.BaseExample;
import cn.org.awcp.core.utils.SessionUtils;
import cn.org.awcp.core.utils.constants.SessionContants;
import cn.org.awcp.formdesigner.application.service.StoreService;
import cn.org.awcp.formdesigner.application.vo.StoreVO;
import cn.org.awcp.formdesigner.application.vo.StyleVO;
import cn.org.awcp.unit.vo.PunSystemVO;
import cn.org.awcp.venson.controller.base.ReturnResult;

/**
 * 样式库管理
 *
 */
@Controller
@RequestMapping("/fd/style")
public class StyleController extends BaseController {
	
	@Autowired
	@Qualifier("storeServiceImpl")
	private StoreService storeService;

	/**
	 * 根据code、name、description字段进行查询分页显示列表
	 * 
	 * @param styleCode
	 * @param name
	 * @param description
	 * @param currentPage
	 * @return
	 */
	@RequestMapping(value = "/list")
	public ModelAndView listPageStyles(@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "styleDesc", required = false) String description,
			@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {
		BaseExample baseExample = new BaseExample();
		if (pageSize < 10) {
			pageSize = 10;
		}
		if (currentPage <= 0) {
			currentPage = 1;
		}
		baseExample.createCriteria().andLike("code", "%" + StoreService.STYLE_CODE + "%");
		if (StringUtils.isNoneBlank(name)) {
			baseExample.getOredCriteria().get(0).andLike("name", "%" + name + "%");
		}
		if (StringUtils.isNoneBlank(description)) {
			baseExample.getOredCriteria().get(0).andLike("description", "%" + description + "%");
		}
		PageList<StoreVO> list = storeService.selectPagedByExample(baseExample, currentPage, pageSize, "code desc");
		for (StoreVO vo : list) {
			String content = vo.getContent();
			vo.setContent(JSON.parseObject(content, StyleVO.class).getScript());
		}
		ModelAndView mv = new ModelAndView();
		mv.addObject("vos", list);
		mv.addObject("name", name);
		mv.addObject("styleDesc", description);
		mv.setViewName("formdesigner/page/style/style-list");
		return mv;
	}

	/**
	 * 跳转到编辑页面，编辑样式信息 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView editPageStyle(String id) {
		ModelAndView mv = new ModelAndView();
		if (StringUtils.isNoneBlank(id)) {
			StoreVO store = storeService.findById(id);
			StyleVO vo = JSON.parseObject(store.getContent(), StyleVO.class);
			mv.addObject("vo", vo);
		}
		mv.setViewName("formdesigner/page/style/style-edit");
		return mv;
	}
	
	/**
	 * 保存样式
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveByAjax")
	public ReturnResult saveByAjax(StyleVO vo) {
		ReturnResult ret = ReturnResult.get();
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		if (obj instanceof PunSystemVO) {
			PunSystemVO system = (PunSystemVO) obj;
			vo.setSystemId(system.getSysId());
		}
		StoreVO store = convert(vo);
		String id = storeService.save(store);
		vo.setPageId(id);
		return ret;
	}
	
	/**
	 * 删除选中的页面动作数据,更改数据状态
	 * @param selects
	 * @return
	 */
	@RequestMapping(value = "delete")
	public ModelAndView deletePageStyle(String id) {
		ModelAndView mv = new ModelAndView();
		String[] ids = id.split(",");
		storeService.delete(ids);
		mv.setViewName("redirect:/fd/style/list.do");
		return mv;
	}
	
	//把StyleVO 转换成StoreVO
	private StoreVO convert(StyleVO vo) {
		StoreVO store = new StoreVO();
		if (StringUtils.isBlank(vo.getCode())) {
			vo.setCode(StoreService.STYLE_CODE + System.currentTimeMillis());
		}
		store.setCode(vo.getCode());
		store.setName(vo.getName());
		store.setContent(JSON.toJSONString(vo));
		store.setId(vo.getPageId());
		store.setDescription(vo.getDescription());
		store.setSystemId(vo.getSystemId());
		return store;
	}

}
