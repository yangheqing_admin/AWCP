package cn.org.awcp.formdesigner.controller;

import static cn.org.awcp.venson.common.SC.DATA_SOURCE_COOKIE_KEY;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import cn.org.awcp.base.BaseController;
import cn.org.awcp.common.db.DynamicDataSource;
import cn.org.awcp.core.domain.BaseExample;
import cn.org.awcp.core.domain.Criteria;
import cn.org.awcp.core.domain.SzcloudJdbcTemplate;
import cn.org.awcp.core.utils.SessionUtils;
import cn.org.awcp.core.utils.Springfactory;
import cn.org.awcp.core.utils.constants.SessionContants;
import cn.org.awcp.formdesigner.application.service.FormdesignerService;
import cn.org.awcp.formdesigner.application.service.StoreService;
import cn.org.awcp.formdesigner.application.vo.DynamicPageVO;
import cn.org.awcp.formdesigner.application.vo.PageActVO;
import cn.org.awcp.formdesigner.application.vo.StoreVO;
import cn.org.awcp.formdesigner.core.constants.FormDesignGlobal;
import cn.org.awcp.metadesigner.application.MetaModelOperateService;
import cn.org.awcp.unit.service.PunResourceService;
import cn.org.awcp.unit.vo.PunSystemVO;
import cn.org.awcp.venson.controller.base.ControllerHelper;
import cn.org.awcp.venson.controller.base.ReturnResult;
import cn.org.awcp.venson.controller.base.StatusCode;
import cn.org.awcp.venson.exception.PlatformException;
import cn.org.awcp.venson.util.CookieUtil;

/**
 * 表单设计Controller
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/fd")
public class FormdesignerController extends BaseController {

	@Autowired
	private FormdesignerService formdesignerServiceImpl;

	@Autowired
	@Qualifier("storeServiceImpl")
	private StoreService storeService;

	@Autowired
	@Qualifier("punResourceServiceImpl")
	private PunResourceService punResourceService;

	@Resource(name = "metaModelOperateServiceImpl")
	private MetaModelOperateService meta;

	/**
	 * 根据name查询动态页面，分页显示
	 * 
	 * @param name
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(String name, String modular, String pageType, String templateId, String menuId,
			String tips, @RequestParam(required = false, defaultValue = "1") int currentPage,
			@RequestParam(required = false, defaultValue = "10") int pageSize) {
		ModelAndView mv = new ModelAndView();
		if (pageSize < 10) {
			pageSize = 10;
		}
		if (currentPage <= 0) {
			currentPage = 1;
		}
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		BaseExample example = new BaseExample();
		Criteria criteria = example.createCriteria();
		if (obj instanceof PunSystemVO) {
			if (StringUtils.isNotBlank(name)) {
				criteria.andLike("name", "%" + name + "%");
			}
			if (StringUtils.isNotBlank(modular)) {
				criteria.andEqualTo("modular", modular);
			}
			if (StringUtils.isNotBlank(pageType)) {
				criteria.andEqualTo("page_type", pageType);
			}
			if (StringUtils.isNotBlank(templateId)) {
				criteria.andEqualTo("template_id", templateId);
			}
			if (StringUtils.isNotBlank(menuId)) {
				criteria.andEqualTo("id", menuId);
			}
			// 只能查看当前用户创建的表单
			criteria.andEqualTo("CREATED_USER", ControllerHelper.getUser().getName());
			PageList<DynamicPageVO> vos = formdesignerServiceImpl.selectPagedByExample(example, currentPage, pageSize,
					"created desc");
			mv.addObject("vos", vos);
		}
		mv.addObject("pageType", pageType);
		mv.addObject("tips", tips);
		mv.addObject("name", name);
		mv.addObject("currentPage", currentPage);
		mv.addObject("templateId", templateId);
		mv.addObject("menuId", menuId);
		mv.addObject("modular", modular);
		mv.addObject("templates", meta.search(
				"SELECT DISTINCT b.id,b.file_name as text FROM p_fm_dynamicpage a LEFT JOIN p_fm_template b ON a.template_id=b.id"));
		mv.addObject("menus", meta
				.search("SELECT dynamicpage_id id,menu_name text FROM p_un_menu a WHERE LENGTH(a.dynamicpage_id)>0"));
		mv.addObject("modulars", meta.search("select ID,modularName from p_fm_modular"));
		mv.setViewName("formdesigner/page/dynamicPage-list");
		return mv;
	}

	private static Map<String, String> baseDynamicPage = new HashMap<>();
	static {
		baseDynamicPage.put("API管理列表", "api_list");
		baseDynamicPage.put("API管理表单", "api_form");
		baseDynamicPage.put("API参数表单", "api_param_form");
		baseDynamicPage.put("页面模块列表", "page_modular_list");
		baseDynamicPage.put("页面模块新增表单", "page_modular_form");
		baseDynamicPage.put("模块添加页面", "modular_add_page");
		baseDynamicPage.put("模块列表页面", "modular_list_page");
		baseDynamicPage.put("钉钉微应用类型列表", "dd_micro_app_type_list");
		baseDynamicPage.put("钉钉微应用类型表单", "dd_micro_app_type_form");
		baseDynamicPage.put("钉钉微应用列表", "dd_micro_app_list");
		baseDynamicPage.put("钉钉微应用表单", "dd_micro_app_form");
		baseDynamicPage.put("意见组件（手机）", "suggest_component_mobile");
		baseDynamicPage.put("意见组件（电脑）", "suggest_componen_computer");
		baseDynamicPage.put("意见组件（PC）", "suggest_componen_PC");
		baseDynamicPage.put("意见列表片段（手机）", "suggest_list_fragment_mobile");
	}

	@ResponseBody
	@RequestMapping(value = "/updatePageID")
	public Map<String, Object> updatePageID(String oldID, String newID) {
		SzcloudJdbcTemplate jdbcTemplate = Springfactory.getBean("jdbcTemplate");
		jdbcTemplate.beginTranstaion();
		try {
			String sql = "update p_fm_dynamicpage set id=? where id=?";
			String updateMenuSQL = "update p_un_menu set DYNAMICPAGE_ID=? where DYNAMICPAGE_ID=?";
			jdbcTemplate.update(sql, newID, oldID);
			jdbcTemplate.update(updateMenuSQL, newID, oldID);
			PageList<StoreVO> stores = storeService.findByDyanamicPageId(oldID);
			for (StoreVO store : stores) {
				String content = store.getContent();
				JSONObject json = JSONObject.parseObject(content);
				if (StringUtils.isNotBlank(json.getString("dynamicPageId"))) {
					json.put("dynamicPageId", newID);
				}
				store.setContent(json.toJSONString());
				store.setDynamicPageId(newID);
				storeService.save(store);
			}
			jdbcTemplate.commit();
		} catch (Exception e) {
			e.printStackTrace();
			jdbcTemplate.rollback();
		}
		return null;
	}

	/**
	 * 查询动态页面，并跳转到编辑页面
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView editPage(@RequestParam(value = "errorMsg", required = false) String errorMsg,
			@RequestParam(value = "_selects", required = false) String id,
			@RequestParam(required = false, defaultValue = "1") int currentPage) {
		// 根据id来查询，如果id为空，则为新增
		ModelAndView mv = new ModelAndView();
		mv.addObject("currentPage", currentPage);
		mv.setViewName("formdesigner/page/dynamicPage-edit");
		if (errorMsg!=null && !"".equals(errorMsg)) {
			mv.addObject("tips", errorMsg);
		}
		if (!validatorPage(mv, id)) {
			mv.setViewName("redirect:/fd/list.do");
			return mv;
		}
		if (id != null) {
			DynamicPageVO vo = formdesignerServiceImpl.findById(id);
			if (vo != null) {
				BaseExample actExample = new BaseExample();
				actExample.createCriteria().andEqualTo("DYNAMICPAGE_ID", id).andLike("CODE",
						StoreService.PAGEACT_CODE + "%");
				PageList<StoreVO> actStore = storeService.selectPagedByExample(actExample, 1, Integer.MAX_VALUE,
						"BTN_GROUP ASC,T_ORDER ASC");
				List<PageActVO> acts = new ArrayList<PageActVO>();
				for (StoreVO store : actStore) {
					PageActVO act = JSON.parseObject(store.getContent(), PageActVO.class);
					acts.add(act);
				}
				actStore.clear();
				mv.addObject("acts", acts);
			}
			mv.addObject("vo", vo);
		}
		mv.addObject("modulars", meta.search("select ID,modularName from p_fm_modular"));
		mv.addObject("templates", meta.search("select id,file_name from p_fm_template"));
		mv.addObject("styles", meta.search("select ID,NAME from p_fm_store where code like '0.1.6.%'"));
		mv.addObject("_COMPOENT_TYPE_NAME", new TreeMap<>(FormDesignGlobal.COMPOENT_TYPE_NAME));
		return mv;
	}

	/**
	 * 保存动态页面信息 校验不通过，返回到编辑页面
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/save")
	public ReturnResult savePage(DynamicPageVO vo,
			@RequestParam(required = false, defaultValue = "1") int currentPage) {
		ReturnResult ret = new ReturnResult();
		Object o = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		if (o instanceof PunSystemVO) {
			PunSystemVO system = (PunSystemVO) o;
			vo.setSystemId(system.getSysId());
		}
		// 校验
		if (validatorPage(vo.getId())) {
			if (StringUtils.isBlank(vo.getId())) {
				vo.setCheckOutUser(ControllerHelper.getUser().getName());
				vo.setIsCheckOut(1);
			} else {
				updatePageBackup(vo.getId(),vo.getName());
			}
			formdesignerServiceImpl.saveOrUpdate(vo);
			ret.setData(vo.getId());
		} 
		return ret;
	}

	private void updatePageBackup(String id,String name) {
		String sql = "select name from p_fm_dynamicpage where id=?";
		List<Map<String,Object>> list = meta.search(sql, id);
		if(list!=null && list.size()==1) {
			String oldName = (String) list.get(0).get("name");
			if(name.equals(oldName)) {
				return;
			} else {
				sql = "select fileName from p_fm_backup where dynamicpageId=?";
				List<Map<String,Object>> backList = meta.search(sql, id);
				if(!backList.isEmpty()) {
					try {
			  			sql = "select path from p_fm_backup_dir";
						String path = (String) meta.search(sql).get(0).get("path");
			  			String dataSourceName = CookieUtil.findCookie(DATA_SOURCE_COOKIE_KEY);
			  			if(StringUtils.isBlank(dataSourceName)) {
			  				dataSourceName = DynamicDataSource.MASTER_DATA_SOURCE;
			  			}
			  			String oldDir = path + "\\" + dataSourceName + "\\" + oldName;  
			  			String newDir = path + "\\" + dataSourceName + "\\" + name; 
			  			File oldDirFile = new File(oldDir);
			  			if(oldDirFile.exists()) {
			  				oldDirFile.renameTo(new File(newDir));
			  				for(Map<String,Object> map : backList) {
								String fileName = (String) map.get("fileName");
								String newFileName = fileName.replaceAll(oldName, name);
								File file = new File(newDir + "\\" + fileName);
								if(file.exists()) {
									file.renameTo(new File(newDir + "\\" + newFileName));
								}
								sql = "update p_fm_backup set fileName=? where dynamicpageId=?";
								meta.updateBySql(sql, newFileName,id);
							}
			  			}		  			
			  			sql = "update p_fm_backup set dynamicpageName=? where dynamicpageId=?";
						meta.updateBySql(sql, name, id);
			  		} catch(Exception e) {
			  			e.printStackTrace();
			  		}
				}
			}
		}
	}
	
	/**
	 * 复制页面
	 * 
	 * @param ids
	 * @param currentPage
	 * @return
	 */
	@RequestMapping(value = "/copy")
	public ModelAndView copyDyanmiacPages(@RequestParam(value = "_selects", required = false) String[] ids,
			@RequestParam(required = false, defaultValue = "1") int currentPage) {
		ModelAndView mv = new ModelAndView();
		mv.addObject("currentPage", currentPage);
		for (String id : ids) {
			formdesignerServiceImpl.copy(id);
		}
		mv.setViewName("redirect:/fd/list.do");
		return mv;
	}

	/**
	 * 复制到系统
	 * 
	 * @param ids
	 * @param currentPage
	 * @param systemId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/copyToSystem")
	public String copyDyanmiacPagesToSystem(@RequestParam(value = "_selects", required = false) String[] ids,
			@RequestParam(required = false, defaultValue = "1") int currentPage,
			@RequestParam(required = false) String systemId) {
		JSONObject o = new JSONObject();
		if (StringUtils.isNotBlank(systemId)) {
			for (String id : ids) {
				formdesignerServiceImpl.copyToSystem(id, systemId);
			}
			o.put("rtn", "1");
		} else {
			o.put("rtn", "-1");
			o.put("msg", "请选择目标系统！");
		}
		return o.toJSONString();
	}

	/**
	 * 删除选择的动态页面数据
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView deletePage(@RequestParam(value = "_selects") String ids,
			@RequestParam(required = false, defaultValue = "1") int currentPage) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:/fd/list.do?currentPage=" + currentPage);
		try {
			String[] array = ids.split(",");
			List<String> temp = new ArrayList<String>();
			for (String id : array) {
				if (!validatorPage(mv, id)) {
					return mv;
				}
				temp.add(id);
				BaseExample example = new BaseExample();
				example.createCriteria().andEqualTo("dynamicPage_id", id);
				List<StoreVO> stores = storeService.selectPagedByExample(example, 1, Integer.MAX_VALUE, null);
				for (StoreVO store : stores) {
					storeService.delete(store);
					// 删除资源表中的按钮
					if (store.getCode().contains(StoreService.PAGEACT_CODE)) {
						punResourceService.removeByRelateResoAndType(store.getId(), "3");
					}
				}
				stores.clear();
			}
			formdesignerServiceImpl.delete(temp);
			temp.clear();
		} catch (Exception e) {
			logger.info("ERROR", e);
		}
		return mv;
	}

	/**
	 * 批量发布页面
	 * 
	 * @param ids
	 * @param currentPage
	 * @return
	 */
	@RequestMapping(value = "/publish")
	public ModelAndView publishPage(@RequestParam(value = "_selects", required = false) String[] ids,
			@RequestParam(required = false, defaultValue = "1") int currentPage) {
		ModelAndView mv = new ModelAndView();
		mv.addObject("currentPage", currentPage);
		for (String id : ids) {
			formdesignerServiceImpl.publish(id);
		}
		mv.setViewName("redirect:/fd/list.do");
		return mv;
	}

	/**
	 * 发布当前页面
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/publishOnePage")
	public ReturnResult publishOnePage(String id) {
		ReturnResult ret = new ReturnResult();
		if (id == null) {
			ret.setStatus(StatusCode.FAIL).setMessage("动态页面ID为空");
		} else {
			try {
				formdesignerServiceImpl.publish(id);
			} catch (Exception e) {
				ret.setStatus(StatusCode.FAIL).setMessage("发布错误");
				logger.info("ERROR", e);
			}
		}
		return ret;
	}

	/**
	 * 签出页面
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/checkOutPage")
	public Map<String, Object> checkOutPage(String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		formdesignerServiceImpl.checkOut(id);
		map.put("msg", "签出成功");
		map.put("userName", ControllerHelper.getUser().getName());
		return map;
	}

	/**
	 * 签入页面
	 * 
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/checkInPage")
	public ReturnResult checkInPage(String id) {
		ReturnResult ret = new ReturnResult();
		DynamicPageVO vo = formdesignerServiceImpl.findById(id);
		if (vo.getIsCheckOut() != null && vo.getIsCheckOut() == 1) { // 已签出状态
			if (!ControllerHelper.getUser().getName().equals(vo.getCheckOutUser())) {
				String msg = vo.getName() + "页面已被  " + vo.getCheckOutUser() + "签出,你无法进行签入";
				ret.setStatus(StatusCode.FAIL).setMessage(msg);
			}
		}
		formdesignerServiceImpl.checkIn(id);
		return ret;
	}

	/**
	 * 查看发布后的内容
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/catTemplate")
	public ModelAndView catTemplate(@RequestParam(value = "_select") String id) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("formdesigner/page/catTemplate");
		if (StringUtils.isNotBlank(id)) {
			try {
				String content = formdesignerServiceImpl.getTemplateContext(id);
				mv.addObject("content", content);
			} catch (Exception e) {
				logger.info("ERROR", e);
				addMessage(mv, "啊哦~后台报错了，传入的id不能被转换为Long！");
			}
		} else {
			addMessage(mv, "请选择需要查看的动态页面！");
		}
		return mv;
	}

	/**
	 * 展示当前页面的直接父级和子级页面
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/relation")
	public ModelAndView refrenceRelation(@RequestParam(value = "_select") String id) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("formdesigner/page/refRelation");
		try {
			Map<String, DynamicPageVO> child = findChildComponentByDynamicPageId(id);
			// 找出包含该页面的包含组件（父亲）
			// 找出该包含组件所在页面
			Map<String, DynamicPageVO> parent = new HashMap<String, DynamicPageVO>();
			List<JSONObject> parentComponents = storeService.findParentComponentByDyanamicPageId(id);
			for (JSONObject component : parentComponents) {
				String relatePageId = component.getString("dynamicPageId");
				if (StringUtils.isNotBlank(relatePageId)) {
					DynamicPageVO vo = formdesignerServiceImpl.findById(relatePageId);
					if (vo != null) {
						parent.put(vo.getId(), vo);
					}
				}
			}
			mv.addObject("parent", parent);
			mv.addObject("child", child);
		} catch (Exception e) {
			logger.info("ERROR", e);
		}
		return mv;
	}

	/**
	 * 查询所有没有页面绑定的动态页面
	 * @return
	 */
	@RequestMapping(value = "/list-bind")
	public ModelAndView bind() {
		ModelAndView mv = new ModelAndView();
		mv.addObject("vos", getUnbindDynamicPage());
		mv.setViewName("formdesigner/page/dynamicPage-bind-add");
		return mv;
	}

	/**
	 * 查询所有没有页面绑定的动态页面
	 * @return
	 */
	@RequestMapping(value = "/listSelect")
	public ModelAndView listSelect() {
		ModelAndView mv = new ModelAndView();
		mv.addObject("vos", getUnbindDynamicPage());
		mv.setViewName("formdesigner/page/dynamicPage-bind-select");
		return mv;
	}
	
	private PageList<DynamicPageVO> getUnbindDynamicPage(){
		PageList<DynamicPageVO> vos = new PageList<DynamicPageVO>();
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		if (obj instanceof PunSystemVO) {
			BaseExample example = new BaseExample();
			String sql = "select distinct pageId from "
					+ "(select PAGEID_APP as pageId from p_un_page_binding union "
					+ "select PAGEID_APP_LIST as pageId from p_un_page_binding union "
					+ "select PAGEID_PC as pageId from p_un_page_binding union "
					+ "select PAGEID_PC_LIST as pageId from p_un_page_binding) temp where ifnull(pageId,'')<>''";
			SzcloudJdbcTemplate jdbcTemplate = Springfactory.getBean("jdbcTemplate");
			List<String> list = jdbcTemplate.queryForList(sql, String.class);
			if(!list.isEmpty()) {
				example.createCriteria().andNotInStr("id", list);
			}
			vos = formdesignerServiceImpl.selectPagedByExample(example, 1, 9999, "ID desc");
		} 
		return vos;
	}

	/**
	 * 根据name查询动态页面，分页显示
	 * 
	 * @param name
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/listJson")
	public String listJson(@RequestParam(required = false) String name) {
		Long systemId = -1L;
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		if (obj instanceof PunSystemVO) {
			PunSystemVO system = (PunSystemVO) obj;
			systemId = system.getSysId();
		}
		Map<String, Object> params = new HashMap<String, Object>();
		if (StringUtils.isNotBlank(name)) {
			params.put("name", "%" + name + "%");
		}
		List<DynamicPageVO> vos = formdesignerServiceImpl.listNameAndIdInSystem(systemId, params);
		JSONArray array = new JSONArray();
		for (DynamicPageVO vo : vos) {
			JSONObject o = new JSONObject();
			o.put("id", vo.getId());
			o.put("name", vo.getName());
			array.add(o);
		}
		return array.toJSONString();
	}

	private Map<String, DynamicPageVO> findChildComponentByDynamicPageId(String id) {
		List<JSONObject> components = storeService.findComponentByDyanamicPageId(id);
		Map<String, DynamicPageVO> child = new HashMap<String, DynamicPageVO>();
		for (JSONObject component : components) {
			String relatePageId = component.getString("relatePageId");
			if (StringUtils.isNotBlank(relatePageId)) {
				DynamicPageVO vo = formdesignerServiceImpl.findById(relatePageId);
				if (!vo.getId().equals(id)) {
					child.putAll(findChildComponentByDynamicPageId(vo.getId()));
				}
				if (vo != null) {
					child.put(vo.getId(), vo);
				}
			}
		}
		return child;
	}

	/**
	 * 更动态页面新数据源
	 * 
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateData")
	public String updateDataJson(DynamicPageVO vo) {
		ModelAndView mv = new ModelAndView();
		// 校验
		if (validatorPage(mv, vo.getId())) {
			formdesignerServiceImpl.updateModelInfo(vo);
			return "1";
		} else {
			return "0";
		}
	}

	private boolean validatorPage(ModelAndView mv, String id) {
		// 新增页面不需要判断
		if (StringUtils.isBlank(id)) {
			return true;
		}
		// 只有页面迁出者才可以修改
		DynamicPageVO vo = formdesignerServiceImpl.findById(id);
		if (vo.getIsCheckOut() == null || vo.getIsCheckOut() == 0) {
			mv.addObject("tips", vo.getName() + "页面还未签出，编辑修改前请先签出页面");
			return false;
		} else {
			if (vo.getIsCheckOut() != null && vo.getIsCheckOut() == 1) { // 已签出状态
				if (!ControllerHelper.getUser().getName().equals(vo.getCheckOutUser())) {
					String ret = vo.getName() + "页面已被  " + vo.getCheckOutUser() + "签出！";
					mv.addObject("tips", ret);
					return false;
				}
			}
		}
		return true;
	}

	@ResponseBody
	@RequestMapping(value = "/validateCheckOut")
	public Map<String, Object> validateCheckOut(String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (id == null) {
			map.put("value", 1);
			// 新增页面，无需校验是否签出；
			return map;
		}

		DynamicPageVO vo = formdesignerServiceImpl.findById(id);
		if (vo != null && vo.getIsCheckOut() != null && vo.getIsCheckOut() == 1) { // 已签出状态
			if (!ControllerHelper.getUser().getName().equals(vo.getCheckOutUser())) {
				String ret = vo.getName() + "页面已被  " + vo.getCheckOutUser() + "签出！";
				map.put("value", ret);
				return map;
			}
		}
		map.put("value", 1);
		return map;
	}

	/**
	 * 获取查询下拉框数据
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/getSearchData")
	public Map<String,Object> getSearchData(){
		Map<String,Object> ret = new HashMap<String,Object>();
		ret.put("templates", meta.search("SELECT DISTINCT b.id,b.file_name as text FROM p_fm_dynamicpage a "
				+ "LEFT JOIN p_fm_template b ON a.template_id=b.id"));
		ret.put("modulars", meta.search("select ID,modularName from p_fm_modular"));
		return ret;
	}
	
	/**
	 * 根据name查询动态页面，分页显示
	 * 
	 * @param name
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getDynamicPageList")
	public Map<String,Object> getDynamicPageList(String name, String modular, String pageType, String templateId,
			@RequestParam(required = false, defaultValue = "1") int currentPage,
			@RequestParam(required = false, defaultValue = "10") int pageSize) {
		Map<String,Object> ret = new HashMap<String,Object>();
		if (pageSize < 10) {
			pageSize = 10;
		}
		if (currentPage <= 0) {
			currentPage = 1;
		}
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		BaseExample example = new BaseExample();
		Criteria criteria = example.createCriteria();
		if (obj instanceof PunSystemVO) {
			if (StringUtils.isNotBlank(name)) {
				criteria.andLike("name", "%" + name + "%");
			}
			if (StringUtils.isNotBlank(modular)) {
				criteria.andEqualTo("modular", modular);
			}
			if (StringUtils.isNotBlank(pageType)) {
				criteria.andEqualTo("page_type", pageType);
			}
			if (StringUtils.isNotBlank(templateId)) {
				criteria.andEqualTo("template_id", templateId);
			}
			// 只能查看当前用户创建的表单
			criteria.andEqualTo("CREATED_USER", ControllerHelper.getUser().getName());
			PageList<DynamicPageVO> vos = formdesignerServiceImpl.selectPagedByExample(example, 
					currentPage, pageSize,"created desc");
			int total = vos.getPaginator().getTotalCount();
			ret.put("rows", vos);
			ret.put("total", total);
		}
		return ret;
	}

	/**
	 * 删除选择的动态页面数据
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deletePages")
	public ReturnResult deletePages(String ids) {
		ReturnResult ret = new ReturnResult();
		try {
			List<String> temp = new ArrayList<String>();
			for (String id : ids.split(",")) {
				validatorPage(id);
				temp.add(id);
				BaseExample example = new BaseExample();
				example.createCriteria().andEqualTo("dynamicPage_id", id);
				List<StoreVO> stores = storeService.selectPagedByExample(example, 1, Integer.MAX_VALUE, null);
				for (StoreVO store : stores) {
					storeService.delete(store);
					// 删除资源表中的按钮
					if (store.getCode().contains(StoreService.PAGEACT_CODE)) {
						punResourceService.removeByRelateResoAndType(store.getId(), "3");
					}
				}
				stores.clear();
			}
			formdesignerServiceImpl.delete(temp);
			temp.clear();
		} catch (Exception e) {
			ret.setStatus(StatusCode.FAIL).setMessage("删除失败");
			logger.info("ERROR", e);
		}
		return ret;
	}
	
	private boolean validatorPage(String id) {
		// 新增页面不需要判断
		if (StringUtils.isBlank(id)) {
			return true;
		}
		// 只有页面迁出者才可以修改
		DynamicPageVO vo = formdesignerServiceImpl.findById(id);
		if (vo.getIsCheckOut() == null || vo.getIsCheckOut() == 0) {
			throw new PlatformException(vo.getName() + "页面还未签出，编辑修改前请先签出页面");
		} else {
			if (vo.getIsCheckOut() != null && vo.getIsCheckOut() == 1) { // 已签出状态
				if (!ControllerHelper.getUser().getName().equals(vo.getCheckOutUser())) {
					throw new PlatformException(vo.getName() + "页面已被  " + vo.getCheckOutUser() + "签出！");
				}
			}
		}
		return true;
	}

	/**
	 * 复制选择的动态页面数据
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/copyPages")
	public ReturnResult copyPages(String ids) {
		ReturnResult ret = new ReturnResult();
		try {
			for (String id : ids.split(",")) {
				formdesignerServiceImpl.copy(id);
			}
		} catch (Exception e) {
			ret.setStatus(StatusCode.FAIL).setMessage("复制失败");
			logger.info("ERROR", e);
		}
		return ret;
	}

	/**
	 * 批量发布页面
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/publishPages")
	public ReturnResult publishPages(String ids) {
		ReturnResult ret = new ReturnResult();
		try {
			for (String id : ids.split(",")) {
				formdesignerServiceImpl.publish(id);
			}
		} catch (Exception e) {
			ret.setStatus(StatusCode.FAIL).setMessage("发布失败");
			logger.info("ERROR", e);
		}
		return ret;
	}
}
