package cn.org.awcp.formdesigner.controller;

import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import cn.org.awcp.base.BaseController;
import cn.org.awcp.core.domain.BaseExample;
import cn.org.awcp.core.utils.SessionUtils;
import cn.org.awcp.core.utils.constants.SessionContants;
import cn.org.awcp.formdesigner.application.service.StoreService;
import cn.org.awcp.formdesigner.application.vo.StoreVO;
import cn.org.awcp.formdesigner.application.vo.ValidatorVO;
import cn.org.awcp.unit.vo.PunSystemVO;
import cn.org.awcp.venson.controller.base.ReturnResult;

@Controller
@RequestMapping("/fd/validator")
public class ValidatorController extends BaseController {

	@Autowired
	@Qualifier("storeServiceImpl")
	private StoreService storeService;

	/**
	 * 根据name、description字段进行查询分页显示列表
	 * @param name
	 * @param description
	 * @param currentPage
	 * @return
	 */
	@RequestMapping(value = "/list")
	public ModelAndView listValidators(@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "validatorDes", required = false) String description,
			@RequestParam(value = "isSelect", required = false) boolean isSelect,
			@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {
		if (pageSize < 10) {
			pageSize = 10;
		}
		if (isSelect) {
			pageSize = 30;
		}
		if (currentPage <= 0) {
			currentPage = 1;
		}
		BaseExample baseExample = new BaseExample();
		baseExample.createCriteria().andLike("code", StoreService.VALIDATOR_CODE + "%");
		if (StringUtils.isNoneBlank(name)) {
			baseExample.getOredCriteria().get(0).andLike("name", "%" + name + "%");
		}
		if (StringUtils.isNoneBlank(description)) {
			baseExample.getOredCriteria().get(0).andLike("description", "%" + description + "%");
		}
		PageList<StoreVO> list = storeService.selectPagedByExample(baseExample, currentPage, pageSize, "code desc");
		ModelAndView mv = new ModelAndView();
		mv.addObject("vos", list);
		mv.addObject("currentPage", currentPage);
		mv.addObject("pageSize", pageSize);
		mv.addObject("name", name);
		mv.addObject("validatorDes", description);
		mv.addObject("isSelect", isSelect);
		mv.setViewName("formdesigner/page/validator/validator-list");
		return mv;
	}

	/**
	 * 返回单个validator对象json格式字符串
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getByAjax")
	public String getValidatorByAjax(String id) {
		StoreVO vo = storeService.findById(id);
		return vo.getContent();
	}

	/**
	 * 返回多个对象json格式字符串 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getResultsByAjax")
	public String getValidatorsByAjax(String ids) {
		StringBuilder rtn = new StringBuilder("[");
		if (StringUtils.isNotBlank(ids)) {
			ids = StringEscapeUtils.unescapeHtml4(ids);
			String[] idArray = ids.split("\\,");
			List<StoreVO> list = storeService.findByIds(idArray);
			for (int i = 0; i < idArray.length; i++) {
				String id = idArray[i];
				for (StoreVO vo : list) {
					if (id.equalsIgnoreCase(vo.getId())) {
						rtn.append(vo.getContent());
						rtn.append(",");
						list.remove(vo);
						break;
					}
				}
			}
			rtn.deleteCharAt(rtn.length() - 1);
		}
		rtn.append("]");
		return rtn.toString();
	}

	/**
	 * 跳转到编辑页面
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView editValidator(@RequestParam(value = "_selects", required = false) String id) {
		ModelAndView mv = new ModelAndView();
		StoreVO store = new StoreVO();
		if (StringUtils.isNotBlank(id)) {
			store = storeService.findById(id);
		}
		JSONObject vo = JSON.parseObject(store.getContent());
		mv.addObject("vo", vo);
		mv.setViewName("formdesigner/page/validator/validator-edit");
		return mv;
	}

	@ResponseBody
	@RequestMapping(value = "/getValidatorList")
	public List<StoreVO> getValidatorList(
			@RequestParam(required = false, defaultValue = "1") int currentPage,
			@RequestParam(required = false, defaultValue = "1000") int pageSize,
			@RequestParam(required = false, defaultValue = " T_ORDER ASC") String sortString) {
		BaseExample baseExample = new BaseExample();
		baseExample.createCriteria().andLike("CODE", StoreService.VALIDATOR_CODE + "%");
		return storeService.selectPagedByExample(baseExample, currentPage, pageSize, sortString);
	}

	/**
	 * ajax保存
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveValidator")
	public ReturnResult saveValidator(ValidatorVO vo) {
		ReturnResult ret = new ReturnResult();
		Object obj = SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
		if (obj instanceof PunSystemVO) {
			PunSystemVO system = (PunSystemVO) obj;
			vo.setSystemId(system.getSysId());
		}
		StoreVO store = convert(vo);
		storeService.save(store);				
		return ret;
	}

	private StoreVO convert(ValidatorVO vo) {
		StoreVO store = new StoreVO();
		if (StringUtils.isBlank(vo.getCode())) {
			vo.setCode(StoreService.VALIDATOR_CODE + System.currentTimeMillis());
		}
		store.setCode(vo.getCode());
		store.setName(vo.getName());
		store.setContent(JSON.toJSONString(vo));
		store.setId(vo.getPageId());
		store.setDescription(vo.getDescription());
		store.setSystemId(vo.getSystemId());
		return store;
	}
	
	/**
	 * 删除选中的校验数据,更改数据状态
	 * @param selects
	 * @return
	 */
	@RequestMapping(value = "delete")
	public ModelAndView deleteValidator(@RequestParam(value = "_selects") String selects) {
		ModelAndView mv = new ModelAndView();
		String[] ids = selects.split(",");
		storeService.delete(ids);
		mv.setViewName("redirect:/fd/validator/list.do");
		return mv;
	}	

}
