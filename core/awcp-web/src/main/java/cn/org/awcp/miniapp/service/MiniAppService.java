package cn.org.awcp.miniapp.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import cn.org.awcp.core.domain.SzcloudJdbcTemplate;
import cn.org.awcp.miniapp.util.MiniAppConstant;
import cn.org.awcp.miniapp.util.MiniAppUtil;
import cn.org.awcp.unit.service.PunUserBaseInfoService;
import cn.org.awcp.unit.utils.EncryptUtils;
import cn.org.awcp.unit.vo.PunUserBaseInfoVO;
import cn.org.awcp.venson.common.SC;
import cn.org.awcp.venson.exception.PlatformException;
import cn.org.awcp.venson.util.HttpUtils;
import cn.org.awcp.venson.util.RedisUtil;
import cn.org.awcp.wechat.util.RequestUtil;

/**
 * 小程序Service
 * @author yqtao
 */
@Service
public class MiniAppService {

	@Autowired
	private RedisUtil redisUtil;
	
	@Resource(name = "punUserBaseInfoServiceImpl")
	private PunUserBaseInfoService userService;
	
	@Autowired
	private SzcloudJdbcTemplate jdbcTemplate;
	
	/**
	 * 根据code获取openid
	 * @param code		获取openid的code
	 * @param appid		微信小程序appid
	 */
	public String getOpenid(String code, String appid) {
		if (StringUtils.isEmpty(code)) {
			throw new PlatformException("code值为空");
		} else {
			String urlPath = getOpenidUrl(appid) + code;
			String data = HttpUtils.sendGet(urlPath);
			JSONObject jsonObject = JSON.parseObject(data);
			if (jsonObject.get("errcode") != null) {
				throw new PlatformException(jsonObject.getString("errmsg"));
			} else {
				return jsonObject.getString("openid");
			}			
		}	
	}
	
	/*
	 * 获取openidUrl
	 * appid  微信小程序appid
	 */
	private String getOpenidUrl(String appid) {
		Map<String,Object> params = MiniAppConstant.getMiniAppParams(appid);
		String appsecret = (String) params.get("appsecret");
		return MiniAppConstant.GET_OPEN_ID_URL.replace("APPID", appid).replace("APP_SECRET", appsecret);
	}

	/**
	 * 生成小程序分享的二维码
	 * @param imgName	生成的二维码图片文件名
	 * @param path		扫描二维码后跳转的页面
	 * @param appid		小程序appid
	 */
	public void createAqrCodeImg(String imgName, String path, String appid) {
		MiniAppUtil.createAqrCodeImg(imgName, path, getAccessTokenUrl(appid), MiniAppConstant.CREATE_WX_AQR_CODE);
	}
	
	/*
	 * 获取getAccessTokenUrl
	 * appid : 小程序appid
	 */
	private String getAccessTokenUrl(String appid) {
		Map<String,Object> params = MiniAppConstant.getMiniAppParams(appid);	
		String appsecret = (String) params.get("appsecret");
		return MiniAppConstant.GET_ACCESS_TOKEN.replace("APPID", appid).replace("APP_SECRET", appsecret);	
	}
	
	/**
	 * 获取AccessToken
	 * @param appid		微信小程序appid
	 * @return
	 */
	public String getAccessToken(String appid) {
		String key = appid + "_accessToken";
		if(redisUtil.hasKey(key)) {
			return (String) redisUtil.get(key);
		} else {
			try {
				String jsonStr = RequestUtil.doGet(getAccessTokenUrl(appid));
				JSONObject jsonObj = JSON.parseObject(jsonStr);
				String accessToken = jsonObj.getString("access_token");
				redisUtil.set(key, accessToken, 3600);
				return accessToken;
			} catch (URISyntaxException | IOException e) {
				e.printStackTrace();
				throw new PlatformException("获取AccessToken失败");
			}			
		}
	}
	
	/**
	 * 生成用户
	 * @param openid		微信openid
	 * @param nickName		微信昵称
	 * @param imgs			微信头像
	 * @param args			生成二维码的参数
	 */
	@Transactional
	public void createUser(String openid, String nickName, String imgs, String ... args) {
		String sql = "select count(*) from p_un_user_app_base_info where openid=?";
		Integer count = jdbcTemplate.queryForObject(sql, Integer.class, openid);
		List<PunUserBaseInfoVO> users = userService.selectByIDCard(openid);		
		if(count == 0) {
			sql = "insert into p_un_user_app_base_info (openid, nick_name, imgs) VALUES (?, ?, ?)";
			jdbcTemplate.update(sql, openid, nickName, imgs);
			if(users.isEmpty()){
				PunUserBaseInfoVO vo = new PunUserBaseInfoVO();
				vo.setUserPwd(EncryptUtils.encrypt(SC.DEFAULT_PWD));
				vo.setUserIdCardNumber(openid);
				vo.setUserName(nickName);
				vo.setName(nickName);
				userService.addOrUpdateUser(vo);
			} 
			if(args.length == 3) {
				createAqrCodeImg(args[0], args[1], args[2]);
			}
		} else {
			sql = "update p_un_user_app_base_info set nick_name=?,imgs=? where openid=?";
			jdbcTemplate.update(sql, nickName, imgs, openid);
		}
	}

}
