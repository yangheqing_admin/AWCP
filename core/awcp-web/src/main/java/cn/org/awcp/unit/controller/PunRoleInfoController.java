package cn.org.awcp.unit.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import cn.org.awcp.core.domain.BaseExample;
import cn.org.awcp.core.domain.Criteria;
import cn.org.awcp.core.domain.SzcloudJdbcTemplate;
import cn.org.awcp.core.utils.SessionUtils;
import cn.org.awcp.core.utils.constants.ResourceTypeEnum;
import cn.org.awcp.core.utils.constants.SessionContants;
import cn.org.awcp.formdesigner.application.service.FormdesignerService;
import cn.org.awcp.formdesigner.application.vo.StoreVO;
import cn.org.awcp.unit.service.PunMenuService;
import cn.org.awcp.unit.service.PunResourceService;
import cn.org.awcp.unit.service.PunRoleInfoService;
import cn.org.awcp.unit.utils.JsonFactory;
import cn.org.awcp.unit.utils.ResourceTreeUtils;
import cn.org.awcp.unit.vo.PunMenuVO;
import cn.org.awcp.unit.vo.PunResourceTreeNode;
import cn.org.awcp.unit.vo.PunResourceVO;
import cn.org.awcp.unit.vo.PunRoleInfoVO;
import cn.org.awcp.unit.vo.PunSystemVO;
import cn.org.awcp.venson.controller.base.ReturnResult;
import cn.org.awcp.venson.exception.PlatformException;

/**
 * 角色管理Controller
 *
 */
@Controller
@RequestMapping("/unit")
public class PunRoleInfoController {
	/**
	 * 日志对象
	 */
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	@Qualifier("punRoleInfoServiceImpl")
	PunRoleInfoService roleService;

	@Autowired
	@Qualifier("punMenuServiceImpl")
	private PunMenuService menuService;

	@Resource(name = "punResourceServiceImpl")
	private PunResourceService resoService;

	@Autowired
	private FormdesignerService formdesignerServiceImpl;

	@Autowired
	private SzcloudJdbcTemplate jdbcTemplate;

	/**
	 * 获取角色列表
	 * @param roleName	角色名称
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("/listRolesInSys")
	public ModelAndView listRolesInSys(String roleName,
			@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {
		ModelAndView mv = new ModelAndView();
		if (currentPage <= 0) {
			currentPage = 1;
		}
		if (pageSize <= 5) {
			pageSize = 10;
		}
		BaseExample example = new BaseExample();
		Criteria criteria = example.createCriteria();
		if (StringUtils.isNotBlank(roleName)) {
			criteria.andLike("ROLE_NAME", "%" + roleName + "%");
		}
		PageList<PunRoleInfoVO> vos = roleService.selectPagedByExample(example, currentPage, pageSize, "ROLE_ID ASC");
		mv.addObject("roleName", roleName);
		mv.addObject("vos", vos);
		mv.setViewName("unit/punRoleInfo-sys-list");
		return mv;
	}
	
	/**
	 * 新增角色
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/intoPunRoleInfo", method = RequestMethod.GET)
	public ModelAndView intoPunRoleInfo(Model model) {
		ModelAndView mv = new ModelAndView("/unit/punRoleInfo-sys-edit");
		try {
			PunSystemVO sysVO = (PunSystemVO) SessionUtils.getObjectFromSession(SessionContants.TARGET_SYSTEM);
			if (null != sysVO) {
				PunRoleInfoVO vo = new PunRoleInfoVO();
				vo.setSysId(sysVO.getSysId());
				model.addAttribute("sysId", sysVO.getSysId());
				model.addAttribute("vo", vo);
			}
		} catch (Exception e) {
			logger.info("ERROR", e);
		}
		return mv;
	}
	
	/**
	 * 编辑角色
	 * @param id	角色ID
	 * @return
	 */
	@RequestMapping("editRoleInSys")
	public ModelAndView editRoleInSys(Long id) {
		ModelAndView mv = new ModelAndView();
		PunRoleInfoVO vo = new PunRoleInfoVO();
		if (id != null) {
			vo = roleService.findById(id);
		}
		mv.addObject("vo", vo);
		mv.setViewName("unit/punRoleInfo-sys-edit");
		return mv;
	}

	/**
	 * 保存角色
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping("saveRoleInSys")
	public ReturnResult saveRoleInSys(PunRoleInfoVO vo) {
		ReturnResult ret = ReturnResult.get();
		validate(vo);
		roleService.addOrUpdateRole(vo);		
		return ret;
	}
	
	/**
	 * 删除角色
	 * @param boxs
	 * @return
	 */
	@ResponseBody
	@RequestMapping("delRoleInSys")
	public ReturnResult delRoleInSys(Long[] boxs) {
		ReturnResult ret = ReturnResult.get();
		if (boxs != null) {
			for (int i = 0; i < boxs.length; i++) {
				roleService.delete(boxs[i]);
			}
		}
		return ret;
	}
	
	/**
	 * 角色授权
	 * @param boxs
	 * @param moduleId
	 * @return
	 */
	@RequestMapping(value = "punRoleMenuAccessEdit")
	public ModelAndView punRoleMenuAccessEdit(Long boxs, Integer moduleId) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("unit/dev/accessAuthor");
		try {
			List<PunMenuVO> resoVOs = menuService.findAll();
			List<PunMenuVO> accVOS = new ArrayList<>();
			if (boxs != null) {
				PunRoleInfoVO vo = new PunRoleInfoVO();
				vo.setRoleId(boxs);
				mv.addObject("roleName", jdbcTemplate
						.queryForObject("select ROLE_NAME from p_un_role_info where ROLE_ID=" + boxs, String.class));
				mv.addObject("vo", vo);
				accVOS = menuService.getByRoleAndSys(vo, null);
			}
			setAccessRight(resoVOs, accVOS);
			ResourceTreeUtils rtu = new ResourceTreeUtils();
			List<PunMenuVO[]> list = rtu.generateTreeView(resoVOs);
			Map<Integer, List<PunResourceTreeNode>> resultMap = new HashMap<>();
			Integer index = 1;
			for (PunMenuVO[] prvo : list) {
				// 以目录根节点的index为key，以manyNodeTree为value
				List<PunResourceTreeNode> manyNodeTree = ResourceTreeUtils.getPlainDevAccessZNodes(prvo, boxs);
				resultMap.put(index, manyNodeTree);
				index++;
			}
			JsonFactory factory = new JsonFactory();
			mv.addObject("menuJson", factory.encode2Json(resultMap));
			String sql = "select id from p_fm_dynamicpage where template_id in ('4','5','6')";
			if(moduleId!=null && moduleId!=0){
				sql += " and modular=" + moduleId;
			} 
			List<String> dynamicPageIds = jdbcTemplate.queryForList(sql,String.class);
			sql = "select ID,modularName from p_fm_modular order by ID ";
			mv.addObject("modules", jdbcTemplate.queryForList(sql));
			mv.addObject("moduleId", moduleId);

			// 1、查找当前系统的所有按钮，按动态表单名分类
			// 2、查找资源，格式为Map<relateResoId,Resource>
			// 3、查找已授权的资源
			Map<String, List<StoreVO>> buttonMap = new HashMap<String, List<StoreVO>>();
			if(!dynamicPageIds.isEmpty()) {
				buttonMap = formdesignerServiceImpl.getSystemActs(dynamicPageIds);
			}
			Map<String, Object> params = new HashMap<>();
			params.put("resouType", ResourceTypeEnum.RESO_BUTTON.getkey());
			Map<String, PunResourceVO> buttonResos = resoService.queryButtonMap("eqQueryList", params);
			Map<String, List<PunResourceVO>> resultsMap = new HashMap<>();
			Set<String> keys = buttonMap.keySet();
			// 将按钮按照动态表单名称划分
			for (Iterator<String> it = keys.iterator(); it.hasNext();) {
				String s =    it.next();
				List<StoreVO> storeVOs = buttonMap.get(s);
				for (StoreVO storeVO : storeVOs) {
					// 如果resouce中有按钮ID
					if (buttonResos.containsKey(storeVO.getId())) {
						if (resultsMap.containsKey(s)) {
							List<PunResourceVO> newVos = resultsMap.get(s);
							newVos.add(buttonResos.get(storeVO.getId()));
						} else {
							List<PunResourceVO> temVOs = new ArrayList<PunResourceVO>();
							temVOs.add(buttonResos.get(storeVO.getId()));
							resultsMap.put(s, temVOs);
						}
					}
				}
			}
			Map<String, Object> authorParams = new HashMap<>();
			authorParams.put("roleId", boxs);
			// 排除菜单地址和动态表单
			authorParams.put("resouType", ResourceTypeEnum.RESO_BUTTON.getkey());
			// 已授权的资源
			List<String> resoAuthor = resoService.queryResoIds("queryResoAuthor", authorParams);
			mv.addObject("resultsMap", resultsMap);
			mv.addObject("resoAuthor", resoAuthor);
		} catch (Exception e) {
			logger.info("ERROR", e);
			mv.addObject("result", "System error.");
		}
		return mv;
	}
	
	/**
	 * 获取所有角色
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/listRolesInSysByAjax")
	public List<PunRoleInfoVO> listRolesInSysByAjax() {
		ModelAndView mv = new ModelAndView();
		List<PunRoleInfoVO> vos = roleService.findAll();
		mv.addObject("PunRoleInfoVO", vos);
		return vos;
	}

	private void setAccessRight(List<PunMenuVO> mainList, List<PunMenuVO> accessList) {
		for (int i = 0; i < accessList.size(); i++) {
			PunMenuVO temp = accessList.get(i);
			for (int j = 0; j < mainList.size(); j++) {
				if (temp.getMenuId().longValue() == mainList.get(j).getMenuId().longValue()) {
					mainList.get(j).setChecked(true);
					break;
				}
			}
		}
	}

	//校验必填项
	private void validate(PunRoleInfoVO vo) {
		if (null == vo) {
			throw new PlatformException("没有数据");
		}
		if (null == vo.getRoleName()) {
			throw new PlatformException("角色名称不能为空");
		}
		Map<String, Object> params = new HashMap<String, Object>();
		List<PunRoleInfoVO> rVOs = null;
		// 新增时
		if (null == vo.getRoleId()) {
			params.put("roleName", vo.getRoleName());
			rVOs = roleService.queryResult("eqQueryList", params);
		// 修改时
		} else {
			PunRoleInfoVO roleInfoVO = roleService.findById(vo.getRoleId());
			// 角色名修改
			if (StringUtils.isNotEmpty(vo.getRoleName()) && !vo.getRoleName().equals(roleInfoVO.getRoleName())) {
				params.put("roleName", vo.getRoleName());
				rVOs = roleService.queryResult("eqQueryList", params);
			}
		}
		if (null!=rVOs && rVOs.size()>0) {
			throw new PlatformException("角色已经存在");
		}
	}

}
