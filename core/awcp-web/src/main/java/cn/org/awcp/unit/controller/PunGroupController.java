package cn.org.awcp.unit.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;

import cn.org.awcp.core.utils.SessionUtils;
import cn.org.awcp.core.utils.constants.SessionContants;
import cn.org.awcp.unit.service.PunGroupService;
import cn.org.awcp.unit.vo.PunAJAXStatusVO;
import cn.org.awcp.unit.vo.PunGroupVO;
import cn.org.awcp.venson.controller.base.ReturnResult;
import cn.org.awcp.venson.exception.PlatformException;

/**
 * 部门管理Controller
 *
 */
@Controller
@RequestMapping("/unit")
public class PunGroupController {
	/**
	 * 日志对象
	 */
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	@Qualifier("punGroupServiceImpl")
	private PunGroupService groupService;
	
	@Resource
	private JdbcTemplate jdbcTemplate;
	
	/**
	 * 获取当前登陆组织信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "selfGroupGet")
	public ModelAndView selfGroupGet() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/unit/punGroup-edit");
		try {
			PunGroupVO vo = (PunGroupVO) SessionUtils.getObjectFromSession(SessionContants.CURRENT_USER_GROUP);
			PunGroupVO vo1 = groupService.findById(vo.getGroupId());
			mv.addObject("vo", vo1);
		} catch (Exception e) {
			logger.info("ERROR", e);
			mv.addObject("result", "系统错误.");
		}
		return mv;
	}
	
	/**
	 * 保存部门
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/punGroupSave", method = RequestMethod.POST)
	public ReturnResult punGroupSave(PunGroupVO vo) {
		ReturnResult ret = ReturnResult.get();	
		validate(vo);		
		if (null == vo.getParentGroupId() || vo.getParentGroupId().longValue() == 0) {
			vo.setPid(null);
		} else {
			PunGroupVO parentGroupVO = (PunGroupVO) groupService.findById(vo.getParentGroupId());
			if (null == parentGroupVO) {
				throw new PlatformException("父组织不存在.");
			}
			vo.setPid(parentGroupVO.getPid() + vo.getParentGroupId().toString() + ",");
		}
		if (null == vo.getParentGroupId()) {
			vo.setParentGroupId(new Long(0));// 根节点的ParentId为0
		}
		groupService.addOrUpdate(vo);	
		ret.setData(vo);
		return ret;
	}
	
	//校验必填项
	private void validate(PunGroupVO vo) {
		if (StringUtils.isBlank(vo.getGroupChName())) {
			throw new PlatformException("请填写组织名称。");
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("groupChName", vo.getGroupChName());
		List<PunGroupVO> gVOs = groupService.queryResult("eqQueryList", params);
		int count = 0;
		if (vo.getGroupId() == null) {
			count = gVOs.size();
		} else {
			gVOs.stream().filter((item) -> {
				return !vo.getGroupId().equals(item.getGroupId());
			});
		}
		if (count > 0) {
			throw new PlatformException("该组织已经存在");
		}
	}
	
	/**
	 * 组织树编辑
	 * @param boxs
	 * @return
	 */
	@RequestMapping(value = "punGroupEdit")
	public ModelAndView punGroupEdit(Long[] boxs) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/unit/punGroupTree-edit");
		try {
			if (boxs == null || boxs.length == 0) {
				mv.addObject("vo", jdbcTemplate.queryForMap(
						"select group_id groupId,group_ch_name groupChName from p_un_group where PARENT_GROUP_ID=0"));
			} else {
				mv.addObject("vo", groupService.findById(boxs[0]));
			}
			List<Map<String, Object>> data = jdbcTemplate.queryForList("select group_id id,parent_group_id pId,"
					+ "group_ch_name name,number,group_type groupType from p_un_group order by number");
			mv.addObject("groupJson", JSON.toJSON(data));
		} catch (Exception e) {
			logger.info("ERROR", e);
			mv.addObject("result", "系统错误.");
		}
		return mv;
	}

	/**
	 * 删除组织
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "punGroupAJAXDelete")
	public PunAJAXStatusVO punGroupAJAXDelete(@ModelAttribute("vo") PunGroupVO vo) {
		PunAJAXStatusVO respStatus = new PunAJAXStatusVO();
		try {
			PunGroupVO groupVO = groupService.findById(vo.getGroupId());
			if (null != groupVO) {
				Long id = groupVO.getGroupId();
				groupService.delete(id);
				respStatus.setStatus(0);
			} else {
				respStatus.setStatus(1);
				respStatus.setMessage("该组织不存在.");
			}
		} catch (Exception e) {
			logger.info("ERROR", e);
			respStatus.setStatus(1);
			respStatus.setMessage("系统错误.");
		}
		return respStatus;
	}
	
	/**
	 * 获取所有部门
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "listGroup")
	public List<PunGroupVO> listGroup() {
		ModelAndView mv = new ModelAndView();
		List<PunGroupVO> vo = groupService.findAll();
		mv.addObject("vo", vo);
		return vo;
	}

}
