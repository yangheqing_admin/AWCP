package cn.org.awcp.formdesigner.controller;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import cn.org.awcp.base.BaseController;
import cn.org.awcp.core.domain.BaseExample;
import cn.org.awcp.core.domain.SzcloudJdbcTemplate;
import cn.org.awcp.core.utils.SessionUtils;
import cn.org.awcp.extend.formdesigner.DocumentUtils;
import cn.org.awcp.formdesigner.application.service.DocumentService;
import cn.org.awcp.formdesigner.application.service.FormdesignerService;
import cn.org.awcp.formdesigner.application.service.StoreService;
import cn.org.awcp.formdesigner.application.vo.DocumentVO;
import cn.org.awcp.formdesigner.application.vo.DynamicPageVO;
import cn.org.awcp.formdesigner.application.vo.PageActVO;
import cn.org.awcp.formdesigner.application.vo.StoreVO;
import cn.org.awcp.formdesigner.core.domain.design.context.data.DataDefine;
import cn.org.awcp.formdesigner.core.parse.bean.PageDataBeanWorker;
import cn.org.awcp.formdesigner.service.IDocumentService;
import cn.org.awcp.formdesigner.utils.ScriptEngineUtils;
import cn.org.awcp.metadesigner.application.MetaModelOperateService;
import cn.org.awcp.metadesigner.application.MetaModelService;
import cn.org.awcp.unit.vo.PunUserBaseInfoVO;
import cn.org.awcp.venson.controller.base.ControllerHelper;
import cn.org.awcp.venson.controller.base.ReturnResult;
import cn.org.awcp.venson.controller.base.StatusCode;
import cn.org.awcp.venson.exception.PlatformException;
import cn.org.awcp.venson.util.BeanUtil;

@Controller
@RequestMapping("/document")
public class DocumentController extends BaseController {
    /**
     * 日志对象
     */
    protected static final Log logger = LogFactory.getLog(DocumentController.class);

    @Autowired
    private FormdesignerService formdesignerServiceImpl;

    @Autowired
    private DocumentService documentServiceImpl;
    
    @Autowired
    private IDocumentService iDocumentServiceImpl;

    @Autowired
    private StoreService storeServiceImpl;
    
    @Autowired
    private MetaModelService metaModelServiceImpl;

    @Resource(name = "metaModelOperateServiceImpl")
    private MetaModelOperateService meta;

    @Autowired
    SzcloudJdbcTemplate jdbcTemplate;

    /**
     * 根据参数Map和脚本环境，解析出sql语句
     * 根据sql语句查询数据 根据数据初始化各组件的状态(各个脚本包括选项脚本) 读取模版 模版+数据+状态 =输出
     *
     * @param dynamicPageId
     * @param request
     * @return
     * @throws ScriptException
     * @throws IOException
     */
    @RequestMapping(value = "/view")
    public void view(String dynamicPageId, HttpServletRequest request, HttpServletResponse response)
            throws ScriptException, IOException {
        response.setContentType("text/html;");
        response.getWriter().write(iDocumentServiceImpl.view(dynamicPageId, request));
    }

    @RequestMapping(value = "/excute")
    public ModelAndView excute(HttpServletRequest request, HttpServletResponse response)
            throws ScriptException, IOException {
        ModelAndView mv = new ModelAndView();
        String actId = request.getParameter("actId");
        String pageId = request.getParameter("dynamicPageId");
        String docId = request.getParameter("docId");
        String update = request.getParameter("update");
        String[] _selects = request.getParameterValues("_selects");
        DocumentVO docVo;
        DocumentUtils utils = DocumentUtils.getIntance();
        DynamicPageVO pageVO;
        if (StringUtils.isBlank(pageId)) {
            throw new PlatformException("动态页面dynamicPageId为空");
        }
        if (StringUtils.isBlank(actId)) {
            throw new PlatformException("动作actId为空");
        }
        pageVO = formdesignerServiceImpl.findById(pageId);
        if (pageVO == null) {
            throw new PlatformException("动态页面未找到");
        }
        StoreVO store = storeServiceImpl.findById(actId);
        if (store == null) {
            throw new PlatformException("按钮动作ID未找到");
        }
        boolean isUpdate = false;
        if (StringUtils.isNotBlank(update)) {
            isUpdate = "true".equalsIgnoreCase(update);
        }
        // 初始化表单数据
        docVo = documentServiceImpl.findById(docId);
        docVo = BeanUtil.instance(docVo, DocumentVO.class);
        docVo.setUpdate(isUpdate);
        docVo.setDynamicPageId(pageId);
        docVo.setDynamicPageName(pageVO.getName());
        Map<String, String[]> enumeration = request.getParameterMap();
        Map<String, String> map = new HashMap<>(enumeration.size());
        for (Map.Entry<String, String[]> entry : enumeration.entrySet()) {
            map.put(entry.getKey(), StringUtils.join(entry.getValue(), ";"));
        }
        docVo.setRequestParams(map);
        docVo = documentServiceImpl.processParams(docVo);
        PageActVO act = JSON.parseObject(store.getContent(), PageActVO.class);
        // 初始化脚本解释执行器,加载全局工具类
        ScriptEngine engine = ScriptEngineUtils.getScriptEngine(docVo, pageVO);
        engine.put("request", request);
        engine.put("session", SessionUtils.getCurrentSession());

        // 如跳转、返回之类的
        if (StringUtils.isNotBlank(act.getServerScript())) {
            String ret;
            try {
                jdbcTemplate.beginTranstaion();
                String script = StringEscapeUtils.unescapeHtml4(act.getServerScript());
                ret = (String) engine.eval(script);
                jdbcTemplate.commit();
                if (ret != null && (ret.startsWith("redirect:") || ret.startsWith("forward:"))) {
                    mv.setViewName(ret);
                    return mv;
                }
            } catch (Exception e) {
                logger.info("ERROR", e);
                jdbcTemplate.rollback();
            }
        } else {
            // 校验文档
            switch (act.getActType()) {
                // 保存--不带流程
                case 2001:
                    PunUserBaseInfoVO user = ControllerHelper.getUser();
                    docVo.setLastmodifier(String.valueOf(user.getUserId()));
                    docVo.setAuditUser(String.valueOf(user.getUserId()));
                    // 设置doc 记录为草稿状态
                    docVo.setState("草稿");
                    if (docVo.isUpdate()) {
                        // 更新数据
                        for (Iterator<String> it = docVo.getListParams().keySet().iterator(); it.hasNext(); ) {
                            String o = it.next();
                            utils.updateData(o);
                        }
                        // 更新document 记录
                        docVo.setLastmodified(new Date());
                        utils.saveDocument();
                    } else {
                        docVo.setCreated(new Date());
                        docVo.setAuthorId(user.getUserId());
                        docVo.setLastmodified(docVo.getCreated());
                        // 保存数据 向document插入数据
                        for (Iterator<String> it = docVo.getListParams().keySet().iterator(); it.hasNext(); ) {
                            String o = it.next();
                            utils.saveData(o);
                        }
                        utils.saveDocument();
                    }
                    break;
                //退回
                case 2002:
                    Long backId = Long.parseLong(act.getExtbute().get("target"));
                    mv.setViewName("redirect:/document/view.do?dynamicPageId=" + backId);
                    return mv;
                //删除
                case 2003:
                    if (_selects != null && _selects.length >= 1) {
                        for (String select : _selects) {
                            utils.deleteData(pageVO, select);
                            BaseExample base = new BaseExample();
                            base.createCriteria().andEqualTo("RECORD_ID", select);
                            documentServiceImpl.deleteByExample(base);
                        }
                        String viewName = "redirect:/document/view.do?dynamicPageId=" + pageId;
                        mv.setViewName(viewName);
                        return mv;
                    }
                    break;
                // 新增
                case 2009:
                    mv.addObject("dynamicPageId", act.getExtbute().get("target"));
                    mv.setViewName("redirect:/document/view.do");
                    return mv;
                // 编辑
                case 2010:
                    if (_selects != null && _selects.length == 1) {
                        mv.setViewName("redirect:/document/view.do?id=" + _selects[0] + "&dynamicPageId="
                                + act.getExtbute().get("target"));
                        return mv;
                    }
                    break;
                default:
                    break;
            }

        }

        // 数据校验
        mv.addObject("dynamicPageId", pageId);
        mv.addObject("id", docVo.getRecordId());
        mv.setViewName("redirect:/document/view.do");
        return mv;
    }

    /***
     * 执行动作脚本
     *
     * @param actId
     *            动作ID
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "executeAct", method = RequestMethod.GET)
    public ReturnResult executeAct(String actId, HttpServletRequest request) throws ScriptException {
        ReturnResult result = ReturnResult.get();
        result.setStatus(StatusCode.SUCCESS).setData(iDocumentServiceImpl.eval(actId, null, null, request));
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/excuteOnly")
    public void excuteOnly(HttpServletRequest request, @RequestParam("dynamicPageId") String dynamicPageId,
                           @RequestParam(value = "docId", required = false) String docId,
                           @RequestParam(value = "actId", required = false) String actId) throws Exception {
        DocumentVO docVo = documentServiceImpl.findById(docId);
        docVo = BeanUtil.instance(docVo, DocumentVO.class);
        ControllerHelper.renderJSON(null, iDocumentServiceImpl.execute(actId, dynamicPageId, docVo, request));
    }

    @ResponseBody
    @RequestMapping(value = "/executeScript")
    public ReturnResult executeScript(HttpServletRequest request, @RequestParam("storeId") String storeId,
                                      @RequestParam("className") String className) {
        ReturnResult result = ReturnResult.get();
        //查找组件
        StoreVO store = storeServiceImpl.findById(storeId);
        String content = store.getContent();
        JSONObject json = JSON.parseObject(content);
        //查找对应按钮
        JSONArray buttons = json.getJSONArray("buttons");
        List<Object> list = buttons.stream().filter(b -> {
            JSONObject j = (JSONObject) b;
            return className.equals(j.get("className"));
        }).collect(Collectors.toList());
        if (list != null && list.size() == 1) {
            JSONObject script = (JSONObject) list.get(0);
            //服务端脚本不为空则执行
            String code = script.getString("severCodes");
            if (StringUtils.isNotBlank(code)) {
                ScriptEngine engine = ScriptEngineUtils.getScriptEngine();
                engine.put("request", request);
                engine.put("session", SessionUtils.getCurrentSession());
                try {
                    jdbcTemplate.beginTranstaion();
                    Object data=engine.eval(code);
                    jdbcTemplate.commit();
                    return result.setStatus(StatusCode.SUCCESS).setData(data);
                } catch (Exception e) {
                    jdbcTemplate.rollback();
                    logger.debug("ERROR",e);
                    if (e instanceof PlatformException) {
                        return result.setStatus(StatusCode.FAIL).setMessage(e.getMessage());
                    } else {
                        return result.setStatus(StatusCode.FAIL).setMessage("脚本执行出错，执行失败");
                    }
                }
            }
        }
        return result.setStatus(StatusCode.FAIL).setMessage("脚本未找到，执行失败");
    }

    /**
     * ajax刷新表格数据，如果传入的method为delete，则执行删除，返回查询列表数据
     *
     * @param request
     */
    @SuppressWarnings("unchecked")
	@ResponseBody
    @RequestMapping(value = "/refreshDataGrid")
    public ReturnResult refreshDataGrid(HttpServletRequest request,
                                        @RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
                                        @RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize) {
        ReturnResult result = ReturnResult.get();
        String componentId = request.getParameter("componentId");
        StoreVO s = storeServiceImpl.findById(componentId);
        if(s==null){
            return result.setStatus(StatusCode.FAIL).setMessage("组件未找到");
        }
        JSONObject oo = JSON.parseObject(s.getContent());
        if(oo==null){
            return result.setStatus(StatusCode.FAIL).setMessage("组件未找到");
        }
        String method = request.getParameter("method");
        String dataAlias;
        if (request.getParameter("dataFile") != null) {
            dataAlias = oo.getString(request.getParameter("dataFile"));
        } else {
            dataAlias = oo.getString("dataAlias");
        }
        String pageId = s.getDynamicPageId();
        DynamicPageVO pageVo = formdesignerServiceImpl.findById(pageId);
        Map<String, DataDefine> map = PageDataBeanWorker
                .convertConfToDataDefines(StringEscapeUtils.unescapeHtml4(pageVo.getDataJson()));
        DataDefine dd = map.get(dataAlias);
        if ("delete".equals(method)) {
            String selects = request.getParameter("_selects");
            String[] _selects = null;
            if (selects != null) {
                _selects = selects.split(",");
            }

            DocumentUtils utils = DocumentUtils.getIntance();
            if (_selects != null && _selects.length >= 1) {
                for (String select : _selects) {
                    utils.deleteData(pageVo, select);
                }
            }
        } else if ("save".equals(method)) {
            String json = request.getParameter("json");
            JSONArray arr = JSON.parseArray(json);
            if (arr != null && !arr.isEmpty()) {
                int size = arr.size();
                for (int i = 0; i < size; i++) {
                    JSONObject obj = arr.getJSONObject(i);
                    try {
                        meta.update(obj.toJavaObject(Map.class),
                                metaModelServiceImpl.queryByModelCode(dd.getModelCode()).getTableName());
                    } catch (Exception e) {
                        result.setData(Collections.EMPTY_LIST).setTotal(0);
                        return result;
                    }
                }
            }

        }
        getDataGridItems(request,dd,currentPage,pageSize, result);
        return result;
    }

    /**
     * 获取对应的表格数据
     *
     * @param request
     * @return
     */
    public void getDataGridItems(HttpServletRequest request,DataDefine dd, int currentPage, int pageSize, ReturnResult result) {
        ScriptEngine engine = ScriptEngineUtils.getScriptEngine();
        engine.put("request",request);
        PageList<Map<String, String>> pageList = documentServiceImpl.getDataListByDataDefine(dd, engine,currentPage, pageSize, null);
        if (pageList == null || pageList.isEmpty()) {
            result.setData(Collections.EMPTY_LIST).setTotal(0);
        }else{
            result.setData(pageList).setTotal(pageList.getPaginator().getTotalCount());
        }
    }

}
