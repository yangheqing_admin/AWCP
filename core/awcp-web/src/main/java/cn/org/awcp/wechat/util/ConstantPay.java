package cn.org.awcp.wechat.util;

/**
 * 微信公众号支付参数
 */
public class ConstantPay {

	/**
	 * 商户号ID
	 */
	//public static String MCH_ID = "1459258702";
	public static String MCH_ID = "1490313042";
	
	/**
	 * 商户号Key
	 */
	//public static String KEY = "730794d5c2e34541b32917dc2f119f47";
	public static String KEY = "70bcafecff934c55bf6d6d90d75bce6c";
	
	/**
	 * 微信支付服务商商户号
	 */
	public static String PARENT_MCH_ID = "1311397901";
	
	/**
	 * 微信支付服务商Key
	 */
	public static String PARENT_KEY = "SZHCzx13113979012016030114086688";	
	
	/**
	 * 微信支付服务商的公众号APPID
	 */
	public static String PARENT_APPID = "wxe82d24efd5c552a0";	
	
	/**
	 * 微信公众号在微信支付服务商中的子商户号
	 */
	public static String SUB_MCH_ID = "1358723402";	
	
	/**
	 * 微信支付方式:0：普通商户支付:1：服务商支付
	 */
	public static String PAYMENT_TYPE = "0";
	
}
